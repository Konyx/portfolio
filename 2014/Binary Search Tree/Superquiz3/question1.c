
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LINE_LENGTH 80      // The longest line this program will accept
#define MAX_NUM_STUDENTS 500    // The maximum number of students this program can handle
#define MAX_NAME_SIZE 50        // The maximum allowable name length

// The declaration of the student record (or struct). Note that
// the struct contains the name as an array of characters, rather than
// containing just a pointer to the name as before.

typedef struct student_s Student;

struct student_s {
    char name[MAX_NAME_SIZE];
    int age;
    Student* next;              // Pointer to next student in a list
};

// Create a pool of student records to be allocated on demand

Student studentPool[MAX_NUM_STUDENTS];  // The student pool
int firstFree = 0;

// Return a pointer to a new student record from the pool, after
// filling in the provided name and age fields. Returns NULL if
// the student pool is exhausted.
Student* newStudent(const char* name, int age) {
    Student* student = NULL;
    if (firstFree < MAX_NUM_STUDENTS) {
        student = &studentPool[firstFree];
        firstFree += 1;
        strncpy(student->name, name, MAX_NAME_SIZE);
        student->name[MAX_NAME_SIZE - 1] = '\0';  // Make sure it's terminated
        student->age = age;
        student->next = NULL;
    }
    return student;
}

// Read a single student from a csv input file with student name in first column,
// and student age in second.
// Returns: A pointer to a Student record, or NULL if EOF or an invalid
// student record is read. Blank lines, or lines in which the name is
// longer than the provided name buffer, or there is no comma in the line
// are considered invalid.
Student* readOneStudent(char name[600])
{
   // char buffer[MAX_LINE_LENGTH];  // Buffer into which we read a line from stdin
    Student* student = NULL;       // Pointer to a student record from the pool

    // Read a line, extract name and age
    

    char* cp = name;
    if (cp != NULL) {           // Proceed only if we read something
        char* commaPos = strchr(name, ',');
        if (commaPos != NULL && commaPos > name) {
            int age = atoi(commaPos + 1);
            *commaPos = '\0';  // null-terminate the name
            student = newStudent(name, age);
        }
    }
    return student;
}


Student* readStudents(char name[100])
{
    Student* first = NULL;     // Pointer to the first student in the list
    Student* last = NULL;      // Pointer to the last student in the list
    Student* student = readOneStudent(name);
    while (student != NULL) {
        if (first == NULL) {
            first = last = student;   // Empty list case
        }
        else {
            last->next = student;
            last = student;
        }
        student= readOneStudent(name);
    }
    return first;
}


void printOneStudent(Student student)
{
    printf("%s (%d)\n", student.name, student.age);
}


// printStudents: print all students in a list of students, passed
// by reference
void printStudents(const Student* student)
{
    while (student != NULL) {
        printOneStudent(*student);
        student = student->next;
    }
}




Student* insert(Student* student, Student* list) {

	if(list == NULL){
		list = student;
	}
	else if (strcmp(student->name, list->name) < 0)
	{
		student->next = list;
		list = student;
	}
	
	else{
		Student *current;
		Student *previous;
		
		current = list;
		previous = current;
		int found = 0;
		while(current->next != NULL && found == 0)
		{
			if ((strcmp(student->name, current->name) == 0)){
				if(student->age <= current->age){
					found = 1;
					}
					
					
				else if((student->age > current->age)){
					if((strcmp(student->name, current->next->name) == 0) && current->next->age > current->age) {
						previous = current;
						current = current->next;
					}
					
					else(found = 2);
					
					}

	
				else{
					previous = current;
					current = current->next;
					
				}
			}
			
			else if(strcmp(student->name, current->name) < 0){
				found = 1;
			}
			else{
				previous = current;
				current = current->next;
			}
		}
		if(found == 1){
			previous->next = student;
			student->next = current;
		}
		
		else if(found == 2){
			student->next = current->next;
			current->next = student;
	
		}
		
		else{
			current->next = student;
			}
		}
	return list;
}
	
	



// Main program. Read a linked list of students from a csv file, then display
// the contents of that list.
int main(void)
{
	char buffer[100];
	Student* studentList = NULL;
	Student* studentss;

	
	while(fgets(buffer, sizeof(buffer), stdin)){
		studentss = readOneStudent(buffer);
		studentList = insert(studentss, studentList);
	}
	
	
	printStudents(studentList);
    
        // The program could now do various things that make use of
        // the linked list, like deleting students and adding new ones,
        // but the program is already quite long enough!
    }

