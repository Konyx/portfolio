

#include <stdio.h>
#include <malloc.h>
#include <string.h>


/*
 * Structure that describes each node in the binary search tree.
 * 
 * */

struct node_s
{
  char word[100];
  int word_count;
  struct node_s *left_pointer, *right_pointer;
};

/*-----------------------DECLERATIONS--------------------------------*/

typedef struct node_s Node;
typedef Node *NodePointer;

void add_Node(NodePointer *treePointer, char word[]);
void output_words(NodePointer treePointer, int count);
void processInput(int threshold, const char* filename);


/*-------------------------------------------------------------------*/


int main() {
    int allocatedBlocks = 0;
    int leakedBlocks = 0;

    allocatedBlocks = mallinfo().uordblks + mallinfo().hblkhd;
    processInput(100, "output.txt");
    leakedBlocks = mallinfo().uordblks + mallinfo().hblkhd - allocatedBlocks;
    if (leakedBlocks != 0) {
        printf("Memory leak of %d bytes detected!\n", leakedBlocks);
    }
}



void processInput(int threshold, const char* filename){
	
	/*
	 * Function that either takes a files, or stdin depeding
	 * if a file is given with a threshold values. Takes this information
	 * and outputs the words that have occured **THRESHOLD** number of 
	 * times.  
	 * 
	 * */
	
	FILE *file_input;
	file_input = fopen(filename, "r");
	NodePointer rootPointer = NULL;
	char buffer[100];
	
	
	if(filename == NULL){
		while(fgets(buffer, sizeof(buffer), stdin)){
			int stringlen = strlen(buffer);
			buffer[stringlen - 1] = '\0';
			add_Node(&rootPointer, buffer);
		}
		
		output_words(rootPointer, threshold);
	}
	
	else if (file_input == NULL){
		printf("File %s does not exist", filename);
		
	}
	else{
		
		while(fgets(buffer, sizeof(buffer), file_input)){
			int stringlen = strlen(buffer);
			buffer[stringlen - 1] = '\0';
			add_Node(&rootPointer, buffer);
		}
		fclose(file_input);
		output_words(rootPointer, threshold);
	
	}
	
	free(rootPointer);	

}

void add_Node(NodePointer *treePointer, char word[20])

/* 
	* Adds all the inputted words into the binary search tree. Nodes 
	* contain the information of the occurance of the word, and the word
	* itself, aswell as information of the left and right node that flow 
	* from the node added.
	* 
	* */

{
  Node *temp = NULL;
  if (*treePointer == NULL )
  {
    temp = (Node *) malloc(sizeof(Node));
    temp->left_pointer = NULL;
    temp->right_pointer = NULL;
    temp->word_count = 1;
    strcpy(temp->word, word);

    *treePointer = temp;
  }
  else if (strcmp(word, (*treePointer)->word) < 0)
  {
    add_Node(&((*treePointer)->left_pointer), word);
  }
  else if (strcmp(word, (*treePointer)->word) > 0)
  {

    add_Node(&((*treePointer)->right_pointer), word);
  }
  
  else
  {
	  (*treePointer)->word_count += 1; 
	  
	  }

}

void output_words(NodePointer treePointer, int threshold)
{
  /*
   * Recursivly goes through the binary search tree, outputting the 
   * words that occur **THRESHOLD** times. 
   * 
   * */	
	
  int temp_count = threshold;
  if (treePointer != NULL )
  {
    output_words(treePointer->left_pointer, temp_count);
    free(treePointer->left_pointer);
    if(temp_count <= treePointer->word_count){
		printf("%s: %d\n", treePointer->word, treePointer->word_count);
	}
    
    output_words(treePointer->right_pointer, temp_count);
    free(treePointer->right_pointer);
  }
}
