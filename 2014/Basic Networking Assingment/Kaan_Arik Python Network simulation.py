#******************************************************************************#
#*** COSC264 2014 Assignment - Binary Symmetric Channel(BSC) implementation.   #
#*** Contains Functions which simulate BSCs and Two-state Channels (TSC).      #
#*** Uses appropriate support functions and binomial distribution for trials.  #
#******************************************************************************#
#*** AUTHORS NAMES       : Kaan Arik and Stephen Ford                          #
#*** AUTHORS STUDENT IDs : 84254373 and 17714384                               #
#*** DATE                : 22/09/2014                                          #
#******************************************************************************#


from math import factorial
from random import random
from numpy.random import binomial


#****************************************#
#*** BSC and TSC supporting functions ***#
#****************************************#


#Calculate nCr
def combination(n,k):
    numerator=factorial(n)
    denominator=(factorial(k)*factorial(n-k))
    answer=numerator/denominator
    return answer

#Calculate the threshold value, t*, to be compared with s
def calc_thresh(n, k):
    count = 0
    current = combination(n, count)
    while current <= 2**(n-k):
        count += 1
        current += combination(n, count)
    return count - 1

#Calculate the s value (number of errors) using bernoulli trials / binomial 
#distribution
def calculate_s(n, p):
    k = binomial(n, p)
    return k

#Compare the threshold, t, and number of errors, s, to deduce a successful or
#unsuccessful trial
def transmission(s, t):
    if(s <= t ):
        return True
    else:
        return False
    

#********************************************#
#*** Two-state Channel specific functions ***#
#********************************************#


#Determine the initial state (good or bad)
def initial_state(pg, pb):
    if random() < 0.5:
        return pb
    else:
        return pg 

#Determine whether the next state is good or bad
def next_state(current, pgg, pbb, pg, pb):
    changeState = pb
    randNum = random()
    if (current == pg and randNum < pgg) or (current == pb and randNum >= pbb):
        changeState = pg
    return changeState  


#*******************************************************************#
#*** BSC and TSC Funtions to run simulations and average results ***#
#*******************************************************************#


#Find the efficiency values for all N trials for a BSC simulation
def bsc_efficiency(t, p, n, u):
    N = 1000000
    outcomes = []
    k = calculate_s(n, p) #Number of errors present
    count = 0
    while N > 0:
        if transmission(k, t) == True: #Determine whether a retransmission is
            count = count + 1          #needed
            N = N - 1
            ni = u/(count*(n)) #efficiency value for trial N
            outcomes.append(ni)
            count = 0
            k = calculate_s(n, p)
        else:
            count = count + 1
            k = calculate_s(n, p)
    return outcomes #List of efficiency values

#Find the efficiency values for all N trials for a Two-state Channel simulation
def tsc_efficiency(pg, pgg, pb, pbb, u, n, t):
    N = 1000000
    outcomes = []
    current_prob = initial_state(pg, pb)
    k = calculate_s(n, current_prob)
    count = 0
    while N > 0:
        if transmission(k, t) == True:
            count = count + 1
            N = N - 1
            ni = u/(count*(n))
            outcomes.append(ni)
            count = 0
            current_prob = next_state(current_prob, pgg, pbb, pg, pb)
            k = calculate_s(n, current_prob)
        else:
            count = count + 1
            current_prob = next_state(current_prob, pgg, pbb, pg, pb)
            k = calculate_s(n, current_prob)
    return outcomes    

#Finds the average efficiency value for the N trials
def average_efficiency(outcomes):
    sum = 0
    for i in outcomes:
        sum += i
    return sum/1000000 #Sum of all efficiencies / number of trials


#********************************************************#
#*** Functions to run questions 3 - 5 or manual input ***#
#********************************************************#


#Allow for manual input option
def manual_input():
    u = int(input("Enter user data size ")) #user data size
    redundant = int(input("Enter redundant bits ")) 
    p = float(input("Enter bit error probability ")) #bit error rate  
    k = 100 + u
    n = k + redundant
    threshold = calc_thresh(n, k)
    outcomes = bsc_efficiency(threshold, p, n, u)
    result = average_efficiency(outcomes)
    print("Average Efficiency =", result)    
    
#BSC with a variation in user data size for p = 0.01 and p = 0.001
def question_3():
    p = 0.001 #bit error rate
    end = False
    while end == False:
        print("\n*** p =", p, "***\n")
        for u in range(100, 2001, 10): #u = user data size
            k = 100 + u
            redundant = 0.1 * k
            if redundant != int(redundant):
                redundant = int(redundant)+ 1            
            n = k + redundant
            threshold = calc_thresh(n, k)
            outcomes = bsc_efficiency(threshold, p, n, u)
            result = average_efficiency(outcomes)
            print("u =", u, ":", result)   
        if p == 0.01:
            end = True
        else:
            p = 0.01

#BSC with fixed u = 512 and bit rate error values from 0.0001 to 0.01  
def question_4():
    u = 512 #user data size
    k = 100 + u
    redundant = 0.1 * k
    if redundant != int(redundant):
        redundant = int(redundant)+ 1
    n = k + redundant
    threshold = calc_thresh(n, k)
    p_values = []
    for i in range(1, 101, 1): #store bit error rate values
        p_values.append(i/10000)
    for p in p_values: #p = bit error rate
        outcomes = bsc_efficiency(threshold, p, n, u)
        result = average_efficiency(outcomes)
        print("p =", p, ":", result)         

#Comparison of BSC and TSC with varying values of redundant bits
def question_5():  
    question_5a() 
    question_5b() 

#TSC with redundant bits from 0 to 500    
def question_5a():
    print("\n*** TSC ***\n")
    u = 1024 #user data size
    k =  100 + u
    pg = 0.00001
    pb = 0.00199
    pgg = 0.9
    pbb = 0.9
    for i in range(0, 501, 10):
        n = k + i
        threshold = calc_thresh(n, k)
        outcomes = tsc_efficiency(pg, pgg, pb, pbb, u, n, threshold)
        result = average_efficiency(outcomes)
        print("(n-k) =", i, ":", result)
        
#BSC with redundant bits from 0 to 500
def question_5b():
    print("\n*** BSC ***\n")
    u = 1024 #user data size
    k =  100 + u    
    p = 0.001 #bit error probability
    for j in range(0, 501, 10):
        n = k + j
        threshold = calc_thresh(n, k)
        outcomes = bsc_efficiency(threshold, p, n, u)
        result = average_efficiency(outcomes)
        print("(n-k) =", j, ":", result)


#Main funtion to run simulations
def main():
    """Non-commented functions will be executed"""
    #-------------#
    manual_input()
    question_3()
    question_4()
    question_5()
    #-------------#
    return 0

main()