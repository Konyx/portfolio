package thing;

/**
 * Models Food in worlds. People may carry food and they may also be located in
 * a room.
 * 
 * NEED TO ADD MORE FOOD SPECIFIC METHODS
 * 
 * @author Kaan Arik
 * 
 */

public class Food extends Thing {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of the Food
	 */

	public Food(String name) {
		super(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of the Food
	 * @param description
	 *            - Description of the Food
	 */

	public Food(String name, String description) {
		super(name, description);
	}

	/**
	 * Our own formatted string containing property details.
	 */

	public String toString()

	{
		return ("[Food: " + item_Name + ", " + item_Description +"]");

	}

}
