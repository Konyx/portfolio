package thing;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class WeaponsTest {
	
	private Food sword;
	private Food knife;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		sword = new Food("sword", "sharp");
		knife = new Food("knife", "sharp");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testName() {
		Assert.assertEquals(sword.name(), "sword");
		Assert.assertEquals(knife.name(), "knife");
	}

	@Test
	public void testDescription() {
		Assert.assertEquals(sword.description(), "sharp");
		Assert.assertEquals(knife.description(), "sharp");
	}

	@Test
	public void testToString() {
		Assert.assertEquals(sword.toString(), "Weapon contains: sword\nWeapon Description: sharp");
		Assert.assertEquals(knife.toString(), "Weapon contains: knife\nWeapon Description: sharp");
	}

}
