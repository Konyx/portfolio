package thing;

import java.util.Observable;

import thing.Thing;

/**
 * Models things (items) in worlds. People may carry items and they may also be
 * located in a room.
 * 
 * @author Kaan Arik
 * 
 */

public class Thing extends Observable

{
	protected String item_Name;
	protected String item_Description;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of thing.
	 */

	public Thing(String name)

	{
		item_Name = name;
		setChanged();
		notifyObservers(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of thing.
	 * @param description
	 *            - Description of thing.
	 */

	public Thing(String name, String description)

	{
		item_Name = name;
		item_Description = description;
;
	}

	/**
	 * Get name of thing.
	 * 
	 * @return Name of thing.
	 */

	public String name()

	{
		return item_Name;
	}

	/**
	 * Get description of thing.
	 * 
	 * @return Brief description of thing.
	 */

	public String description()

	{
		return item_Description;
	}

	/**
	 * Our own formatted string containing property details.
	 */

	public String toString()

	{
		return ("[Thing: " + item_Name + ", " + item_Description + "]");

	}
	
	

}
