package thing;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FoodTest {
	
	private Food coke;
	private Food pie;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		coke = new Food("Coke", "Yum");
		pie = new Food("Pie", "Yum");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testName() {
		Assert.assertEquals(coke.name(), "Coke");
		Assert.assertEquals(pie.name(), "Pie");
	}

	@Test
	public void testDescription() {
		Assert.assertEquals(coke.description(), "Yum");
		Assert.assertEquals(pie.description(), "Yum");
	}

	@Test
	public void testToString() {
		Assert.assertEquals(coke.toString(), "Food contains: Coke\nThing Description: Yum");
		Assert.assertEquals(pie.toString(), "Food contains: Pie\nThing Description: Yum");
	}

}
