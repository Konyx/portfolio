package thing;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ThingTest {
	
	private Thing coke;
	private Thing computer;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		coke = new Thing("Coke", "Yum");
		computer = new Thing("Apple Computer", "13 inch laptop");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testName() {
		Assert.assertEquals(coke.name(), "Coke");
		Assert.assertEquals(computer.name(), "Apple Computer");
	}

	@Test
	public void testDescription() {
		Assert.assertEquals(coke.description(), "Yum");
		Assert.assertEquals(computer.description(), "13 inch laptop");
	}

	@Test
	public void testToString() {
		Assert.assertEquals(coke.toString(), "Thing contains: Coke\nThing Description: Yum");
		Assert.assertEquals(computer.toString(), "Thing contains: Apple Computer\nThing Description: 13 inch laptop");
	}

}
