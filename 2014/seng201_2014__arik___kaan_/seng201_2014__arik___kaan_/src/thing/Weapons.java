package thing;

/**
 * Models Food in worlds. People may carry food and they may also be located in
 * a room.
 * 
 * NEED TO ADD MORE FOOD SPECIFIC METHODS
 * 
 * @author Kaan Arik
 * 
 */

public class Weapons extends Thing {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of the weapon
	 */

	public Weapons(String name) {
		super(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of the weapon
	 * @param description
	 *            - Description of the weapon
	 */

	public Weapons(String name, String description) {
		super(name, description);
	}

	/**
	 * Our own formatted string containing property details.
	 */

	public String toString()

	{
		return ("[Weapon: " + item_Name + ", " + item_Description + "]");

	}

}
