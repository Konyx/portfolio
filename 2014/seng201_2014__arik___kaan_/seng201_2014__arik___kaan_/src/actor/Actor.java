package actor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;

import place.Place;
import thing.Thing;

/**
 * Models actors in world. actors have their own properties and can also go
 * places and carry things.
 * 
 * @author Kaan Arik
 * 
 */

public class Actor extends Observable {

	protected String actors_Name = "";
	protected Place actors_Location;
	private Collection<Thing> inventory = new ArrayList<Thing>();
	private Collection<Place> placesTraveled = new ArrayList<Place>();

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of Actor
	 */

	public Actor(String name)

	{

		actors_Name = name;
		setChanged();
		notifyObservers(name);

	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of Actor
	 * @param location
	 *            - Location of Actor
	 */

	public Actor(String name, Place location) {
		actors_Name = name;
		actors_Location = location;
		moveTo(actors_Location);
	}

	/**
	 * Tell others our name (i.e., name of person).
	 * 
	 * @return Name of Person.
	 */

	public String name()

	{
		return actors_Name;
	}

	/**
	 * Set or change location of Actor.
	 * 
	 * @param destination
	 *            - Determines(new) location of actor
	 */

	public void moveTo(Place destination)

	{
		actors_Location = destination;
		placesTraveled.add(destination);
		setChanged();
		notifyObservers(actors_Location);

	}

	/**
	 * Tells others where we are (i.e., location of person).
	 * 
	 * @return Current location of person (room).
	 */

	public Place location()

	{

		return actors_Location;

	}

	/**
	 * Add an item to inventory of a actor. Remove it from the place contents.
	 * 
	 * @param t
	 *            - Thing to add to inventory of person.
	 */

	public void take(Thing t)

	{
		inventory.add(t);
		if (actors_Location != null && actors_Location.contents().contains(t)) {
			actors_Location.contents().remove(t);

		}

		setChanged();
		notifyObservers(t);

	}

	/**
	 * Remove item from inventory of actor and add it to room inventory.
	 * 
	 * @param t
	 *            - Thing to be removed.
	 * @return Removed item.
	 */

	public Thing drop(Thing t)

	{
		if (inventory.contains(t)) {
			inventory.remove(t);
			actors_Location.contents().add(t);

		}

		else {
			System.out.println("ERROR");
		}

		setChanged();
		notifyObservers(t);
		return t;

	}

	/**
	 * Inventory of actor suitable for iteration by clients.
	 * 
	 * @return Inventory.
	 */

	public Collection<Thing> inventory()

	{
		return inventory;
	}

	/**
	 * Places a Actor has travelled.
	 * 
	 * @return placesTraveled
	 */

	public Collection<Place> traveled() {
		return placesTraveled;
	}

	/**
	 * Actor-specific version of toString().
	 * 
	 */

	public String toString()

	{
		return ("[Actor: " + actors_Name + ", in " + actors_Location + "]");
	}
}
