package actor;

import place.Room;

/**
 * Models robots in world. robots currently do the same as actor, will
 * eventually have it's own behaviours.
 * 
 * @author Kaan Arik
 * 
 */

public class Robot extends Actor {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of robot
	 */

	public Robot(String name) {
		super(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of robot
	 * @param location
	 *            - Location of robot
	 */

	public Robot(String name, Room location) {
		super(name, location);
	}

	/**
	 * Robot-specific version of toString().
	 * 
	 */

	public String toString()

	{
		return ("[Robot: " + actors_Name + ", in " + actors_Location + "]");
	}

}
