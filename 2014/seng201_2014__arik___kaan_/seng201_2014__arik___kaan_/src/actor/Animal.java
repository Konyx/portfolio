package actor;

import place.Room;

/**
 * Models animals in world. Animals currently do the same as actor, will
 * eventually have it's own behaviours.
 * 
 * @author Kaan Arik
 * 
 */

public class Animal extends Actor {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of animal.
	 */

	public Animal(String name) {
		super(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of animal
	 * @param location
	 *            - Location of animal
	 */

	public Animal(String name, Room location) {
		super(name, location);
	}

	/**
	 * 
	 * Animal-specific version of toString().
	 * 
	 */

	public String toString()

	{
		return ("[Animal: " + actors_Name + ", in " + actors_Location + "]");
	}

}
