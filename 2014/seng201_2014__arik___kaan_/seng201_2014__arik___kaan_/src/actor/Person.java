package actor;

import place.Room;

/**
 * Models person in world. Robots currently do the same as actor, will
 * eventually have it's own behaviours.
 * 
 * @author Kaan Arik
 * 
 */

public class Person extends Actor {

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of person
	 */

	public Person(String name) {
		super(name);
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            - Name of person
	 * @param location
	 *            - Location of person
	 * 
	 */
	public Person(String name, Room location) {
		super(name, location);

	}

	/**
	 * Person-specific version of toString().
	 * 
	 */

	public String toString()

	{
		return ("[Person: " + actors_Name + ", in " + actors_Location + "]");
	}
}
