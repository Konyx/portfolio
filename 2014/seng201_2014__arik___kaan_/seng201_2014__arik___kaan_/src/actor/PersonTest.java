package actor;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import place.Room;
import thing.Thing;

public class PersonTest {
	
	private Room ilam;
	private Room foundry;
	private Thing xbox;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		ilam = new Room("53", 3, "iLam apartments");
		foundry = new Room("Ilam road", 0, "University bar");
		xbox = new Thing("Xbox", "Plays video games");
		new Thing("knife", "small/sharp knife");
		new Thing("computer", "macbook air");
		new Thing("food", "Cookies");
		
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPersonString() {
		Person Kaan = new Person("Kaan");
		Assert.assertNotNull(Kaan);
		Assert.assertEquals(Kaan.name(), "Kaan");
	}



	@Test
	public void testPersonStringRoom() {
		Person Kaan = new Person("Kaan", ilam);
		Assert.assertNotNull(Kaan);
		Assert.assertEquals(Kaan.name(), "Kaan");
		Assert.assertEquals(Kaan.location(), ilam);
	}

	@Test
	public void testMoveTo() {
		Person Kaan = new Person("Kaan", ilam);
		Assert.assertNotNull(Kaan);
		Assert.assertEquals(Kaan.location(), ilam );
		Kaan.moveTo(foundry);
		Assert.assertEquals(Kaan.location(), foundry);
	}

	@Test
	public void testLocation() {
		Person Kaan = new Person("Kaan", ilam);
		Assert.assertNotNull(Kaan);
		Assert.assertEquals(Kaan.location(), ilam);
		Assert.assertEquals(Kaan.location().name(), "53");
		Assert.assertEquals(Kaan.location().description(), "iLam apartments");
		
	}

	@Test
	public void testTake() {
		Person Kaan = new Person("Kaan", ilam);
		Assert.assertEquals(Kaan.inventory().size(), 0);
		Kaan.take(xbox);
		Assert.assertEquals(Kaan.inventory().size(), 1);
		Assert.assertTrue(Kaan.inventory().contains(xbox));
	}

	@Test
	public void testDrop() {
		Person Kaan = new Person("Kaan", ilam);
		Assert.assertEquals(Kaan.inventory().size(), 0);
		Kaan.take(xbox);
		Assert.assertEquals(Kaan.inventory().size(), 1);
		Assert.assertTrue(Kaan.inventory().contains(xbox));
		Kaan.drop(xbox);
		assertEquals(Kaan.inventory().size(), 0);
		
	}



	@Test
	public void testToString() {
		Person Kaan = new Person("Kaan");
		Assert.assertEquals(Kaan.toString(), "Person name is : Kaan");
	}

}
