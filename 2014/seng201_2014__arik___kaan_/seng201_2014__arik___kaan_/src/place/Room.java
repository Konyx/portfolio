package place;


import place.Room;



/**
 * Models a room in a world. People and items may be located in a room.
 * 
 * @author Kaan Arik
 * 
 */

public class Room extends Place

{
	private int room_Level;
	private double room_Width = 0.0d;
	private double room_Depth = 0.0d;


	/**
	 * Constructor
	 * 
	 * @param label
	 *            - Label (e.g., name, room number, door sign) for room.
	 */

	public Room(String label) {
		super(label);
		room_Level = -10000000;

	}

	/**
	 * Constructor
	 * 
	 * @param label
	 *            - Label (e.g., name, room number, door sign) for room.
	 * @param level
	 *            - Level (e.g., floor) of room; can be negative, zero is ground
	 *            floor.
	 */

	public Room(String label, int level)

	{
		super(label);
		room_Level = level;

	}

	/**
	 * Constructor
	 * 
	 * @param label
	 *            - Label (e.g., name, room number, door sign) for room.
	 * @param level
	 *            - Level (e.g., floor) of room; zero is ground floor.
	 * @param description
	 *            - Brief description of room.
	 */

	public Room(String label, int level, String description)

	{
		super(label, description);
		room_Level = level;

	}


	/**
	 * Get level the room is on.
	 * 
	 * @return Room level; can be negative, zero is ground floor.
	 */

	public int level()

	{
		return room_Level;
	}



	/**
	 * Get room width in metres.
	 * 
	 * @return Width of a rectangular room.
	 */

	public double width()

	{
		return room_Width;
	}

	/**
	 * Get room depth in metres
	 * 
	 * @return Depth of a rectangular room.
	 */

	public double depth()

	{
		return room_Depth;
	}



	/**
	 * Set level of room.
	 * 
	 * @param level
	 *            - Level (e.g., floor) of room; can be negative, zero is ground
	 *            floor.
	 */

	public void setLevel(int level)

	{
		room_Level = level;
	}



	/**
	 * Set size / dimensions of room in metres.
	 * 
	 * @param width
	 *            - Room width.
	 * @param depth
	 *            - Room depth.
	 */

	public void setSize(double width, double depth)

	{
		room_Width = width;
		room_Depth = depth;
	}



	/**
	 * Our own formatted string giving state detail.
	 *
	 */
	
    
	public String toString()

	{
		return ("[Room: " + place_Name + ", " + " Level: " + room_Level
				+ ", " + place_Description + "]");
	}

}
