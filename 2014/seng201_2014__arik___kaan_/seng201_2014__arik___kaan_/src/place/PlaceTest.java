package place;


import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import thing.Thing;

public class PlaceTest {
	
	private Place kaans_house;
	private Place dark_room;
	private Thing xbox;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		kaans_house = new Place("flat 53", "6 bedroom");
		dark_room = new Place("DarkRoom");

		xbox = new Thing("Xbox", "Gaming device");
		new Thing("Football", "Nike");

		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPlaceString() {
		Place testPlace = new Place("Hi");
		Assert.assertEquals(testPlace.name(), "Hi");
	}


	@Test
	public void testPlaceStringIntString() {
		Room testRoom = new Room("80 Fergusson St", 1, "Slightly modern house");
		Assert.assertNotNull(testRoom);
		Assert.assertEquals(testRoom.name(), "80 Fergusson St");
		Assert.assertEquals(testRoom.level(), 1);
		Assert.assertEquals(testRoom.description(), "Slightly modern house");
		
	}

	@Test
	public void testName() {
		Assert.assertNotNull(dark_room);
		Assert.assertEquals(dark_room.name(), "DarkRoom");
		Assert.assertEquals(kaans_house.name(), "flat 53");
		
		
	}



	@Test
	public void testDescription() {
		Assert.assertEquals(dark_room.description(), "");
		Assert.assertEquals(kaans_house.description(), "6 bedroom");
	}



	@Test
	public void testSetName() {
		Place testPlace = new Place("Place");
		Assert.assertEquals(testPlace.name(), "Place");
		testPlace.setName("a1");
		Assert.assertEquals(testPlace.name(), "a1");
	}



	@Test
	public void testSetDescription() {
		Place testRoom = new Place("Room");
		Assert.assertEquals(testRoom.description(), "");
		testRoom.setDescription("Slightly big");
		Assert.assertEquals(testRoom.description(), "Slightly big");
	}



	@Test
	public void testAdd() {
		Place testRoom = new Place("Room");
		Assert.assertEquals(testRoom.contents().size(), 0);
		testRoom.add(xbox);
		Assert.assertEquals(testRoom.contents().size(), 1);
		Assert.assertTrue(testRoom.contents().contains(xbox));
		
		
	}

	@Test
	public void testContents() {
		kaans_house.add(xbox);
		Assert.assertEquals(kaans_house.contents().size(), 1 );
		Assert.assertTrue(kaans_house.contents().contains(xbox));
	}



	@Test
	public void testToString() {
		Assert.assertEquals(kaans_house.toString(), "Place: flat 53, 6 bedroom");
	}

}
