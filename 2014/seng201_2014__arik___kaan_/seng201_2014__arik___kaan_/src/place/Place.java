package place;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Observable;

import thing.Thing;



/**
 * Models a place in a world. People and items may be located in a place.
 * 
 * @author Kaan Arik
 * 
 */


public class Place extends Observable {
	
	protected String place_Name;
	protected String place_Description = "";
	private Collection<Thing> place_Contents = new ArrayList<Thing>();
	
	
	/**
	 * Constructor
	 */
	
	public Place(){
		place_Name = "UNKNOWN";
		
	}
	
	/**
	 * Constructor
	 * @param name - Name of place
	 */
	
	public Place(String name){
		place_Name = name;
		
		setChanged();
		notifyObservers(name);


	}
	
	/**
	 * Constructor
	 * @param name - Name of place
	 * @param description - Description of place
	 */
	
	public Place(String name, String description){
		place_Name = name;
		place_Description = description;
		
		setChanged();
		notifyObservers(place_Description);
	}
	
	
	/**
	 * Get the name of the place.
	 * 
	 * @return place_Name - Name of the place.
	 */

	public String name()

	{
		return place_Name;

	}
	
	/**
	 * Get brief description of the place.
	 * 
	 * @return place description - Description of the place.
	 */

	public String description()

	{
		return place_Description;
	}
	
	/**
	 * Sets the of the place.
	 * 
	 * @param name
	 *            - name - Name of the place
	 */

	public void setName(String name)

	{
		place_Name = name;

	}
	
	
	/**
	 * Set the description for a room.
	 * 
	 * @param description
	 *            - New or updated brief description of room.
	 */

	public void setDescription(String description)

	{
		place_Description = description;
	}
	
	/**
	 * Add something to the places's content.
	 * 
	 * @param t
	 *            - Thing to add to place.
	 */

	public void add(Thing t)

	{
		place_Contents.add(t);
		setChanged();
		notifyObservers(t);


	}
	
	
	/**
	 * List content of room.
	 * 
	 * @return String containing comma-delimited list of content item names.
	 */

	public String contentsList()

	{
		String result_String = "";
		for (Thing t : place_Contents) {

			if (result_String.length() != 0) {
				result_String = result_String + ", ";

			}

			result_String += t.name();
		}

		return result_String;

	}
	
	/**
	 * Contents of room.
	 * 
	 * @return Collection containing content of room.
	 */

	public Collection<Thing> contents()

	{

		return place_Contents;

	}
	

	/**
	 * Place-specific version of toString().
	 * 
	 */
	
	public String toString()

	{
		return ("[Place: " + place_Name + ", " + place_Description + "]");
	}

}
