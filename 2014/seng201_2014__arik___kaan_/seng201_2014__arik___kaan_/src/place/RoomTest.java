package place;


import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import thing.Thing;

public class RoomTest {
	
	private Room kaans_house;
	private Room dark_room;
	private Thing xbox;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		kaans_house = new Room("flat 53", 3, "6 bedroom");
		dark_room = new Room("DarkRoom");

		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRoomString() {
		Room testRoom = new Room("Hi");
		Assert.assertEquals(testRoom.name(), "Hi");
	}

	@Test
	public void testRoomStringInt() {
		Room testRoom = new Room("80 Fergusson St", 1);
		Assert.assertEquals(testRoom.name(), "80 Fergusson St");
		Assert.assertEquals(testRoom.level(), 1);
		
	}

	@Test
	public void testRoomStringIntString() {
		Room testRoom = new Room("80 Fergusson St", 1, "Slightly modern house");
		Assert.assertNotNull(testRoom);
		Assert.assertEquals(testRoom.name(), "80 Fergusson St");
		Assert.assertEquals(testRoom.level(), 1);
		Assert.assertEquals(testRoom.description(), "Slightly modern house");
		
	}

	@Test
	public void testLabel() {
		Assert.assertNotNull(dark_room);
		Assert.assertEquals(dark_room.name(), "DarkRoom");
		Assert.assertEquals(kaans_house.name(), "flat 53");
		
		
	}

	@Test
	public void testLevel() {
		Assert.assertEquals(dark_room.level(), -10000000);
		Assert.assertEquals(kaans_house.level(), 3);
	}

	@Test
	public void testDescription() {
		Assert.assertEquals(dark_room.description(), "");
		Assert.assertEquals(kaans_house.description(), "6 bedroom");
	}



	@Test
	public void testSetLabel() {
		Room testRoom = new Room("Room");
		Assert.assertEquals(testRoom.name(), "Room");
		testRoom.setName("a1");
		Assert.assertEquals(testRoom.name(), "a1");
	}

	@Test
	public void testSetLevel() {
		Room testRoom = new Room("Room");
		Assert.assertEquals(testRoom.level(), -10000000);
		testRoom.setLevel(1);
		Assert.assertEquals(testRoom.level(), 1);
	}

	@Test
	public void testSetDescription() {
		Room testRoom = new Room("Room");
		Assert.assertEquals(testRoom.description(), "");
		testRoom.setDescription("Slightly big");
		Assert.assertEquals(testRoom.description(), "Slightly big");
	}



	@Test
	public void testAdd() {
		Room testRoom = new Room("Room");
		Assert.assertEquals(testRoom.contents().size(), 0);
		testRoom.add(xbox);
		Assert.assertEquals(testRoom.contents().size(), 1);
		Assert.assertTrue(testRoom.contents().contains(xbox));
		
		
	}

	@Test
	public void testContents() {
		kaans_house.add(xbox);
		Assert.assertEquals(kaans_house.contents().size(), 1 );
		Assert.assertTrue(kaans_house.contents().contains(xbox));
	}



	@Test
	public void testToString() {
		Assert.assertEquals(kaans_house.toString(), "Room: flat 53,  Level: 3, 6 bedroom");
	}

}
