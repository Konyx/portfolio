package world;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import place.Place;
import place.Room;
import thing.Thing;
import actor.Actor;
import actor.Person;

/**
 * Presents a GUI to a user that allow the user to View the model. Any changes
 * made to the model via GUI commands are processed by the controller. Updates
 * the View accordingly to any changes that happens inside the model and
 * presents it back to the user.
 * 
 * 
 * @author Neville (Updated by Kaan Arik)
 * 
 */

public class WorldView implements Observer {
	
	/*
	 * Controller contains the methods that change the world
	 */
	
	private WorldControl control;
	
	
	/*
	 * Frame for the GUI
	 */

	private JFrame frame = new JFrame("World View");
	
	/*
	 * Panels that will go into the main GUI
	 */

	private JPanel worldInfoPanel;
	private JPanel buttonPanel;

	/*
	 * Information proved via these TextAreas
	 */

	private JTextArea updateBox;
	private JTextArea errorBox;

	/*
	 * Buttons
	 */

	private JButton go;
	private JButton take;
	private JButton clear;
	private JButton drop;
	private JButton travelLog;

	/*
	 * Contents inside the world
	 */

	private JList roomList = new JList();
	private JList actorList = new JList();
	private JList stuff = new JList();
	private JList contents = new JList();

	/*
	 * Information of selected object inside the world.
	 */

	private Actor selectedActor;
	private Place selectedPlace;
	private Thing selectedThing;

	/*
	 * Items inside the MenuBar
	 */

	private JMenuItem newWorld;
	private JMenuItem addActor;
	private JMenuItem addThing;
	private JMenuItem addPlace;
	private JMenuItem exit;
	private JMenuItem save;

	/*
	 * Selection boxes needed when deciding where to place a Person in the World
	 * and to also decide where to place Things when added into the World.
	 */
	
	private JComboBox location_Choices;
	private JComboBox location_Thing_ComboBox;
	private JComboBox selection_ComboBox;
	private DefaultComboBoxModel room_ComboBox_Information;
	private DefaultComboBoxModel person_ComboBox_Information;
	private JPanel adding_Room_DialogBox;

	/**
	 * Builds the GUI that the user is able to interact/view the model with.
	 */

	private void buildGUI() {
		
		/*
		 * Instantiate the controller so the buttons inside 
		 * the GUI are able to change the model. 
		 */
		
		
		control = new WorldControl();

		/*
		 * Buttons that are seen inside the GUI
		 */

		go = new JButton("Go");
		take = new JButton("Take");
		clear = new JButton("Clear");
		drop = new JButton("Drop");
		travelLog = new JButton("Travel Log");

		/*
		 * Placing all the buttons inside a Panel
		 */

		buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(500, 50));
		buttonPanel.setMinimumSize(new Dimension(500, 50));
		buttonPanel.setMaximumSize(new Dimension(500, 50));
		buttonPanel.setBackground(Color.WHITE);

		buttonPanel.add(go);
		buttonPanel.add(take);
		buttonPanel.add(drop);
		buttonPanel.add(clear);
		buttonPanel.add(travelLog);

		/*
		 * Creating Panel that holds the lists of actors, places, items
		 */

		worldInfoPanel = new JPanel();
		worldInfoPanel.setBackground(Color.WHITE);
		worldInfoPanel.setLayout(new BoxLayout(worldInfoPanel,
				BoxLayout.PAGE_AXIS));

		buildPeople();
		buildRooms();
		buildStuff();
		buildContents();
		worldInfoPanel.add(buttonPanel);

		/*
		 * Creates the Error text box and World Update text box adds them to a
		 * JScrollpane then adds them both to a panel.
		 */

		errorBox = new JTextArea(10, 30);
		errorBox.setEditable(false);
		JScrollPane scroller_ErrorBox = new JScrollPane(errorBox);
		scroller_ErrorBox.setBorder(new TitledBorder(new LineBorder(
				Color.black, 1), "ERROR LOG"));
		scroller_ErrorBox.setBackground(Color.WHITE);
		updateBox = new JTextArea(25, 60);
		updateBox.setEditable(false);
		JScrollPane scroller_updateBox = new JScrollPane(updateBox);
		scroller_updateBox.setBorder(new TitledBorder(new LineBorder(
				Color.black, 1), "WORLD CHANGES"));
		scroller_updateBox.setBackground(Color.WHITE);

		JPanel communication_Panel = new JPanel();
		communication_Panel.setLayout(new BoxLayout(communication_Panel,
				BoxLayout.PAGE_AXIS));

		communication_Panel.add(scroller_updateBox, BorderLayout.NORTH);
		communication_Panel.add(scroller_ErrorBox, BorderLayout.SOUTH);

		/*
		 * Creates the Menu Bar along with all the content that is being added to
		 * it. This includes: File, Add Player, Add Place, Add Thing, Exit, Save
		 * Output.
		 */

		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		menuBar.add(file);
		JMenu add = new JMenu("Add");

		addActor = new JMenuItem("Add Actor");
		addThing = new JMenuItem("Add Thing");
		addPlace = new JMenuItem("Add Place");
		exit = new JMenuItem("Exit");
		newWorld = new JMenuItem("New World");
		save = new JMenuItem("Save output");

		/*
		 * Adds Add Player, Add Thing and Add Room to an add subsection in the
		 * File part of the Menu Bar, Also adds Save output, new world and exit to the file
		 * section of the menu bar.
		 */

		add.add(addActor);
		add.add(addThing);
		add.add(addPlace);
		file.add(newWorld);
		file.add(save);
		file.add(add);
		file.add(exit);

		/*
		 * Adds all the panels into the Frame and sets the frame to visble. All
		 * calls the methods to add the functionality to all the buttons
		 * provided in the GUI.
		 */

		frame.setBackground(Color.WHITE);
		frame.add(worldInfoPanel);
		frame.add(communication_Panel, BorderLayout.EAST);
		frame.setJMenuBar(menuBar);
		frame.pack();
		frame.setVisible(true);

		menusControll();
		buildControls();

	}

	/**
	 * Adds the functionality to all the menu items inside the GUI
	 */

	private void menusControll() {

		addActor.addActionListener(new ActionListener() {

			/**
			 * When Add Player in Menu is selected, prompts the user with a
			 * dialog for input of name of the person and where the person will
			 * be situated in the World. Error dialog box prompted if no input
			 * is added into the dialog box/field left out.
			 */

			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Allows user to select of what type the Actor is.
				 * Use full for down the track when each actor is behaves differently
				 */

				String actor_Options[] = { "Person", "Animal", "Robot" };
				
				room_ComboBox_Information = new DefaultComboBoxModel(control
						.getWorld().places().toArray());

				DefaultComboBoxModel actor_ComboBox_Information = new DefaultComboBoxModel(actor_Options);
				
				
				/*
				 * Builds what the user sees when prompted with the dialog box.
				 */

				JComboBox rooms = new JComboBox(room_ComboBox_Information);
				JComboBox choices = new JComboBox(actor_ComboBox_Information);
				JPanel add_Player_Dialog = new JPanel();
				add_Player_Dialog.setLayout((LayoutManager) new BoxLayout(
						add_Player_Dialog, BoxLayout.Y_AXIS));
				add_Player_Dialog.add(new JLabel("What is the Actor?"));
				add_Player_Dialog.add(choices);
				JTextField field1 = new JTextField(10);
				add_Player_Dialog.add(new JLabel("Actors's Name: "));
				add_Player_Dialog.add(field1);
				add_Player_Dialog.add(new JLabel("Actors's location"));
				add_Player_Dialog.add(rooms);
				JOptionPane.showMessageDialog(null, add_Player_Dialog);
				
				/*
				 * Check to see if everything has been filled in
				 */
				
				
				if (field1.getText().isEmpty()
						|| rooms.getSelectedIndex() == -1)
					JOptionPane.showMessageDialog(null,
							"One of the required field is empty!", "Error",
							JOptionPane.ERROR_MESSAGE);

				else if (choices.getSelectedItem().toString() == "Person") {

					/*
					 * Note as this is changing the Model, method from
					 * controller is doing this action.
					 */
					
					updateBox.append("Person " + field1.getText() + " added to " + rooms.getSelectedItem() + "\n");

					control.addPersonControll(field1.getText(),
							(Place) rooms.getSelectedItem());
					
					


				}

				else if (choices.getSelectedItem().toString() == "Animal")
				
					/*
					 * Note as this is changing the Model, method from
					 * controller is doing this action.
					 */

					
				{
					updateBox.append("Animal " + field1.getText() + " added to " + rooms.getSelectedItem() + "\n");
					control.addAnimalControll(field1.getText(),
							(Place) rooms.getSelectedItem());


				}

				else 
				
					/*
					 * Note as this is changing the Model, method from
					 * controller is doing this action.
					 */

				
				{
					updateBox.append("Robot " + field1.getText() + " added to " + rooms.getSelectedItem() + "\n");
					control.addRobotControll(field1.getText(),
							(Place) rooms.getSelectedItem());


				}
			}

		});

		addPlace.addActionListener(new ActionListener() {

			/**
			 * When Add Room in Menu is selected, prompts the user with a dialog
			 * box for input of the name of the room, the level of the room and
			 * room description. Error dialog box prompted if no input is added
			 * into the dialog box/field left out. Otherwise Room is added into
			 * the world, via a method in the controller.
			 * 
			 */

			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Information needed to decide of what type of place is being added.
				 * Generic "Place" is there as an option just until planning of future 
				 * more specific classes are 100% decided.
				 */
				
				
				String Class[] = { "Room", "Place" };
				final JLabel place_Level = new JLabel("Place Level");
				DefaultComboBoxModel choices = new DefaultComboBoxModel(Class);
				location_Choices = new JComboBox(choices);
				
				
				/*
				 * All the dialog information that is required to create the Place.
				 */

				adding_Room_DialogBox = new JPanel();
				adding_Room_DialogBox.setLayout((LayoutManager) new BoxLayout(
						adding_Room_DialogBox, BoxLayout.Y_AXIS));
				adding_Room_DialogBox.add(new JLabel("Place Type : "));
				adding_Room_DialogBox.add(location_Choices);
				JTextField field1 = new JTextField(10);
				adding_Room_DialogBox.add(new JLabel("Room's Name : "));
				adding_Room_DialogBox.add(field1);
				JTextArea textArea = new JTextArea(5, 20);
				adding_Room_DialogBox.add(new JLabel("Room's Description : "));
				JScrollPane scrollPane = new JScrollPane(textArea);
				adding_Room_DialogBox.add(scrollPane);
				
				adding_Room_DialogBox.add(place_Level);
				
				/*
				 * Way of inputting the level of the Room or any future
				 * places that require a level field. 
				 * 
				 * NOTE THIS COULD BE IMPROVED
				 */
				
				SpinnerNumberModel numberModel = new SpinnerNumberModel(
						new Integer(-1), // value
						new Integer(-1), // min
						new Integer(50), // max
						new Integer(1) // step
				);

				final JSpinner numberChooser = new JSpinner(numberModel);
				adding_Room_DialogBox.add(numberChooser);
				
				
		
				location_Choices.addActionListener(new ActionListener() {
					
					/*
					 * Switches the fields that are available in that 
					 * Add Place dialog box to match what is required.
					 */
					
						public void actionPerformed(ActionEvent e) {

							String Item = location_Choices.getSelectedItem()
									.toString();

							if (Item != "Room") {
								
								adding_Room_DialogBox.remove(place_Level);
								adding_Room_DialogBox.remove(numberChooser);
								adding_Room_DialogBox.validate();
						

							} else {
								
								adding_Room_DialogBox.add(place_Level);
								adding_Room_DialogBox.add(numberChooser);
								adding_Room_DialogBox.validate();

							}

						}

					});
					
					
				
						
				/*
				 * Checks to ensure that all fields that are required are filled. 	
				 */
				
				JOptionPane.showMessageDialog(null, adding_Room_DialogBox);
				if (field1.getText().isEmpty())
					JOptionPane.showMessageDialog(null,
							"One of the required field is empty!", "Error",
							JOptionPane.ERROR_MESSAGE);

				
				
				else if ("Place" ==  location_Choices.getSelectedItem().toString()){
					updateBox.append("Place: " + field1.getText() + " added to world." + "\n");
					control.addPlaceControll(field1.getText(), textArea.getText());
				}
				
				
				else {

					/*
					 * Note as this is changing the Model, method from
					 * controller is doing this action.
					 */
					
					updateBox.append("Room: " + field1.getText() + " added to world" + "\n");
					control.addRoomControll(field1.getText(),
							(Integer) numberChooser.getValue(),
							textArea.getText());



				}

			}
		});

		addThing.addActionListener(new ActionListener() {

			/**
			 * When Add Thing is selected prompts the user with a dialog box
			 * asking for the name of the Thing and where the Thing will be
			 * placed. Error occurs if user fails to fill in the correct input.
			 */

			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Allows user to decide where the location of the 
				 * Thing being will be created and also what type the 
				 * Thing will be. 
				 * 
				 * THIS WILL BE VERY USEFUL LATER ON WHEN 
				 * CERTAIN THINGS BEHAVE DIFFERENTLY ETC.
				 */
				
				String location_Choices[] = { "Actors", "Places" };
				String thing_Type[] = { "Thing", "Weapon", "Food" };

				person_ComboBox_Information = new DefaultComboBoxModel(control
						.getWorld().actors().toArray());
				room_ComboBox_Information = new DefaultComboBoxModel(control
						.getWorld().places().toArray());
				location_Thing_ComboBox = new JComboBox(
						person_ComboBox_Information);
				DefaultComboBoxModel selection_Options = new DefaultComboBoxModel(
						location_Choices);
				DefaultComboBoxModel selection_ThingType = new DefaultComboBoxModel(
						thing_Type);
				selection_ComboBox = new JComboBox(selection_Options);
				JComboBox selectionType_ComboBox = new JComboBox(
						selection_ThingType);
				selection_ComboBox.addActionListener(new ActionListener() {
					
					
					public void actionPerformed(ActionEvent e) {
						
						/*
						 * Changes the location options of the Thing 
						 * depending on what the user selects.
						 */

						String Item = selection_ComboBox.getSelectedItem()
								.toString();

						if (Item == "Places") {
							location_Thing_ComboBox
									.setModel(room_ComboBox_Information);

						} else {
							location_Thing_ComboBox
									.setModel(person_ComboBox_Information);
						}

					}

				});
				
				
				
				
				/*
				 * Content that will be visible to the user when 
				 * the user selects Add Thing inside the Menu bar.
				 */
				

				JPanel ThingButton = new JPanel();
				ThingButton.setLayout((LayoutManager) new BoxLayout(
						ThingButton, BoxLayout.Y_AXIS));
				JTextField field1 = new JTextField(10);
				ThingButton.add(new JLabel("What is the Thing?"));
				ThingButton.add(selectionType_ComboBox);
				ThingButton.add(new JLabel("Thing's Name : "));
				ThingButton.add(field1);
				ThingButton.add(new JLabel("Thing's Description : "));
				JTextArea textArea = new JTextArea(5, 20);
				JScrollPane scrollPane = new JScrollPane(textArea);
				ThingButton.add(scrollPane);

				ThingButton.add(new JLabel("Thing's Location?"));
				ThingButton.add(selection_ComboBox);
				ThingButton.add(location_Thing_ComboBox);

				JOptionPane.showMessageDialog(null, ThingButton);
				
				/*
				 * Ensures that all fields that are required to 
				 * create the object and apply certain methods
				 * to the new object are given.
				 */

				if (field1.getText().isEmpty() || textArea.getText().isEmpty()
						|| location_Thing_ComboBox.getSelectedIndex() == -1)
					JOptionPane.showMessageDialog(null,
							"One of the required field is empty!", "Error",
							JOptionPane.ERROR_MESSAGE);
				
				
				/*
				 * Decide what information is given and add call the respective
				 * method inside the controller to update the model.
				 */

				else if (location_Thing_ComboBox.getSelectedItem() instanceof Place
						&& (selectionType_ComboBox.getSelectedItem() == "Thing")) {
					
					updateBox.append("Thing: " + field1.getText() + " added to " + (Place) location_Thing_ComboBox.getSelectedItem() + "\n" );

					control.addThingPlaceControll(field1.getText(),
							textArea.getText(),
							((Place) location_Thing_ComboBox.getSelectedItem()));
					
					

				}

				else if (location_Thing_ComboBox.getSelectedItem() instanceof Place
						&& (selectionType_ComboBox.getSelectedItem() == "Weapon")) {
					
					updateBox.append("Weapon: " + field1.getText() + " added to " + (Place) location_Thing_ComboBox.getSelectedItem() + "\n" );
					control.addWeaponPlaceControll(field1.getText(),
							textArea.getText(),
							((Place) location_Thing_ComboBox.getSelectedItem()));



				}

				else if (location_Thing_ComboBox.getSelectedItem() instanceof Place
						&& (selectionType_ComboBox.getSelectedItem() == "Food")) {
					updateBox.append("Food: " + field1.getText() + " Added to " + (Place) location_Thing_ComboBox.getSelectedItem() + "\n" );
					control.addFoodPlaceControll(field1.getText(),
							textArea.getText(),
							((Place) location_Thing_ComboBox.getSelectedItem()));



				}

				else if (location_Thing_ComboBox.getSelectedItem() instanceof Actor
						&& (selectionType_ComboBox.getSelectedItem() == "Thing")) {
					
					updateBox.append("Thing: " + field1.getText() + " added to " + (Actor) location_Thing_ComboBox.getSelectedItem() + "\n");
					control.addThingPersonControll(field1.getText(),
							textArea.getText(),
							((Actor) location_Thing_ComboBox.getSelectedItem()));

				//	refreshRooms();
				//	refreshActors();

				}

				else if ((location_Thing_ComboBox.getSelectedItem() instanceof Actor && (selectionType_ComboBox
						.getSelectedItem() == "Weapon"))) {
					
					
					updateBox.append("Weapon: " + field1.getText() + " added to " + (Actor) location_Thing_ComboBox.getSelectedItem() + "\n");

					control.addWeaponPersonControll(field1.getText(),
							textArea.getText(),
							((Actor) location_Thing_ComboBox.getSelectedItem()));

				}

				else {
					updateBox.append("Food: " + field1.getText() + " added to " + (Actor) location_Thing_ComboBox.getSelectedItem() + "\n");
					control.addFoodPersonControll(field1.getText(),
							textArea.getText(),
							((Actor) location_Thing_ComboBox.getSelectedItem()));

				}

			}
		});

		exit.addActionListener(new ActionListener() {

			/**
			 * Exits the program if Exit button in menu is selected.
			 */

			public void actionPerformed(ActionEvent ae) {

				System.exit(0);
			}
		});

		newWorld.addActionListener(new ActionListener() {

			/**
			 * Creates a new World by setting the World object to a new object
			 * in the WorldControl class.
			 */

			public void actionPerformed(ActionEvent ae) {
				
				World newWorld = new World();
				control.setWorld(newWorld);
				setWorldControl(control);

		
				updateBox.setText(null);
				errorBox.setText(null);
				updateBox.append("Hello World\n");
				refreshActors();
				refreshRooms();

			}
		});

		save.addActionListener(new ActionListener() {

			/**
			 * Saves the output of players actions in the World to a .txt file
			 * in a location provided by the user.
			 * 
			 */

			public void actionPerformed(ActionEvent ae) {

				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int save = fileChooser.showSaveDialog(null);

				if (save == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();

					try {
						PrintWriter writer = new PrintWriter(file);
						writer.write(updateBox.getText());
						writer.close();
						updateBox.append("SAVED!");
					} catch (FileNotFoundException e) {
						/*
						 * Tells user what went wrong!
						 */
						errorBox.append("File not found!");
					}
				}
			}
		});

	}

	/**
	 * Adds the functionality to all the buttons involved in the GUI. When a
	 * user clicks on a button will do operation that is required.
	 */

	private void buildControls() {

		go.addActionListener(new ActionListener() {

			/**
			 * Moves a selected actor to a new location in the world.
			 */

			public void actionPerformed(ActionEvent ae) {
				Object o = roomList.getSelectedValue();
				if (o instanceof Place) {
					if (selectedActor == null) {
						errorBox.append(("No actor selected" + "\n"));
					}
					
					else if (roomList.getSelectedValue() == selectedActor
							.location()) 
					/*
					 * Tells user what went wrong!
					 */
					{
						errorBox.append("Cannot move to a location that you are already at"
								+ "\n");

					}

					else {
						selectedPlace = (Place) o;
						updateBox.append("Moving: " + selectedActor.toString() + "\nTo: "
								+ ((Place) roomList.getSelectedValue()) + "\n");

						/*
						 * Person is moved to a different location via the
						 * controller.
						 */

						control.goControll(selectedActor,
								(Place) roomList.getSelectedValue());

					}
				} else {
					/*
					 * Tells user what went wrong!
					 */

					errorBox.append("Not a Place---you can't go there" + "\n");
				}
			}
		});

		take.addActionListener(new ActionListener() {

			/**
			 * Takes a Thing from a selected Place and adds it to selected
			 * actors inventory.
			 */

			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Outputs any errors that occurs during
				 * user interactions and tell them what happened 
				 * via the erroBox.
				 */
				
				
				if (selectedPlace == null) {
					errorBox.append("Can't take: no Room selected" + "\n");
					return;
				} else if (selectedActor == null) {
					errorBox.append("Can't take: no Actor selected" + "\n");
					return;
				} else if (selectedActor.location() != selectedPlace) {
					errorBox.append("Must be in " + selectedPlace
							+ " to take this" + "\n");
					return;
				} else {
					selectedThing = (Thing) contents.getSelectedValue();
					if (selectedThing != null) {
						
						/*
						 * Change in model, therefore needs to call a 
						 * method in the controller to make the change. 
						 */
						updateBox.append(selectedActor.name() + " took " + selectedThing.name() + "\n");
						
						control.takeControlls(selectedActor, selectedThing);
					}
				}
			}
		});



		drop.addActionListener(new ActionListener() {

			/**
			 * Drops a Thing from a selected person's inventory to the location
			 * they are currently at.
			 */

			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Checks to see the users interaction with the model is 
				 * suitable, and outputs any errors via the error box.
				 */
				
				
				if (selectedPlace == null) {

					errorBox.append("Can't drop: no Room selected" + "\n");
					return;
				} else if (selectedActor == null) {

					errorBox.append("Can't drop: no Actor selected" + "\n");
					return;
				}
				if (selectedActor.location() != selectedPlace) {
					// will drop it where we are, irrespective of list state
					roomList.setSelectedValue(selectedPlace, true);
				}
				selectedThing = (Thing) stuff.getSelectedValue();
				if (selectedThing != null) {
					updateBox.append(selectedActor.name() + " dropped " + selectedThing.name() + "\n");
					control.droppControlls(selectedActor, selectedThing);
					
					
				}
			}
		});

		/*
		 * Clears all sections that have been made in the GUI
		 */

		clear.addActionListener(new ActionListener()

		/**
		 * Clears all selection made in the GUI.
		 */

		{
			public void actionPerformed(ActionEvent ae) {
				actorList.clearSelection();
				roomList.clearSelection();
				contents.clearSelection();
				stuff.clearSelection();
			}
		});

		/*
		 * Outputs where an Actor has moved during it's lifetime in the World
		 */

		travelLog.addActionListener(new ActionListener()

		/**
		 * Displays the information of where a Actor has moved during there
		 * time in the World.listModel;
		 */

		{
			public void actionPerformed(ActionEvent ae) {
				
				/*
				 * Ensure the user interaction is correct.
				 */
				
				if (selectedActor == null) {

					errorBox.append("No actor selected" + "\n");

				} else {
					
					/*
					 * Makes the information presentation more appealing 
					 * if it gets printed separately.
					 */
					
					updateBox.append(selectedActor.name() + " has traveled in order:" + "\n");
					for (Place o : selectedActor.traveled()) {
						updateBox.append("Location traveld: " + o + "\n");
					}

				}

			}
		});

	}
	
	/**
	 * Presents the information of the Things that are in the specific Places
	 */


	private void buildContents() {
		
		
		/*
		 * Adds the information into a panel, dimensions and 
		 * adds a JScrollPane to ensure that we account for ever
		 * increasing list sizes. Then adds these panels to the Main panel inside the
		 * GUI.
		 */

		JPanel jpc = new JPanel();
		jpc.add(contents);
		jpc.setBackground(Color.WHITE);
		JScrollPane scrollp = new JScrollPane(jpc);
		scrollp.setPreferredSize(new Dimension(600, 125));
		scrollp.setMinimumSize(new Dimension(600, 125));
		scrollp.setBorder(new TitledBorder(new LineBorder(Color.black, 1),
				"Places's Contents"));
		scrollp.setBackground(Color.WHITE);

		worldInfoPanel.add(scrollp);
	}

	/**
	 * Presents the information of Things that are inside a Room, or in a
	 * person's inventory.
	 */

	private void buildStuff() {
		
		/*
		 * Adds the information into a panel, dimensions and 
		 * adds a JScrollPane to ensure that we account for ever
		 * increasing list sizes. Then adds these panels to the Main panel inside the
		 * GUI.
		 */

		JPanel jps = new JPanel();

		jps.add(stuff);
		jps.setBackground(Color.WHITE);
		JScrollPane scrollp = new JScrollPane(jps);

		scrollp.setPreferredSize(new Dimension(600, 125));
		scrollp.setMinimumSize(new Dimension(600, 125));

		scrollp.setBorder(new TitledBorder(new LineBorder(Color.black, 1),
				"Actor's Inventory"));

		scrollp.setBackground(Color.WHITE);
		worldInfoPanel.add(scrollp);

	}

	/**
	 * Presents the information of the People who are inside the model(World).
	 */

	private void buildPeople() {
		
		/*
		 * Adds the information into a panel, dimensions and 
		 * adds a JScrollPane to ensure that we account for ever
		 * increasing list sizes. Then adds these panels to the Main panel inside the
		 * GUI.
		 */

		actorList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		actorList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent lse) {
				selectedActor = (Actor) actorList.getSelectedValue();
				if (selectedActor != null) {
					stuff.setListData(selectedActor.inventory().toArray(
							new Thing[0]));
					roomList.setSelectedValue(selectedActor.location(), true);

				}
			}
		});

		JPanel jpp = new JPanel();

		jpp.add(actorList);
		jpp.setBackground(Color.WHITE);
		JScrollPane scrollp = new JScrollPane(jpp);
		scrollp.setPreferredSize(new Dimension(600, 125));
		scrollp.setMinimumSize(new Dimension(600, 125));

		scrollp.setBorder(new TitledBorder(new LineBorder(Color.black, 1),
				"Actors"));

		scrollp.setBackground(Color.WHITE);
		worldInfoPanel.add(scrollp);

	}

	/**
	 * Presents the information of the Rooms that are inside the model(World).
	 */

	private void buildRooms() {
		
		/*
		 * Adds the information into a panel, dimensions and 
		 * adds a JScrollPane to ensure that we account for ever
		 * increasing list sizes. Then adds these panels to the Main panel inside the
		 * GUI.
		 */

		roomList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		roomList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		roomList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent lse) {
				Place r = (Place) roomList.getSelectedValue();
				selectedPlace = r;
				if (r != null) {
					contents.setListData(r.contents().toArray(new Thing[0]));
				}
			}
		});

		JPanel jpr = new JPanel();

		jpr.add(roomList);
		jpr.setBackground(Color.WHITE);
		JScrollPane scrollp = new JScrollPane(jpr);

		scrollp.setPreferredSize(new Dimension(600, 125));
		scrollp.setMinimumSize(new Dimension(600, 125));

		scrollp.setBorder(new TitledBorder(new LineBorder(Color.black, 1),
				"Places"));
		scrollp.setBackground(Color.WHITE);
		worldInfoPanel.add(scrollp);

	}

	/**
	 * Set up something to view for demonstration purposes
	 */

	private void createWorld() {
		updateBox.append("Hello World\n");
		roomList.setListData(control.getWorld().places().toArray(new Room[0]));
		actorList
				.setListData(control.getWorld().actors().toArray(new Actor[0]));

	}

	/**
	 * Access the World the controller is currently using. Sets the RoomList and
	 * ActorList information to the Actor's and Room's that are currently in the
	 * world and displays this to the user.
	 */
	public void setWorldControl(WorldControl control) {
		this.control = control;
		control.getWorld().addObserver(this);
		roomList.setListData(control.getWorld().places().toArray(new Room[0]));
		actorList
				.setListData(control.getWorld().actors().toArray(new Actor[0]));
	}

	/**
	 * Constructor. Builds the GUI and displays information set in the World
	 * inside the controller.
	 */

	public WorldView() {
		buildGUI();
		createWorld();
	}

	/**
	 * Updates Actors viewed by the user after something in the world has
	 * changed.
	 */

	private void refreshActors() {

		actorList
				.setListData(control.getWorld().actors().toArray(new Actor[0]));
		actorList.clearSelection();
		stuff.clearSelection();
		stuff.setListData(new Thing[0]);
	}

	/**
	 * Updates Rooms viewed by the user after something in the world has
	 * changed.
	 */
	private void refreshRooms() {
		roomList.setListData(control.getWorld().places().toArray(new Place[0]));
		roomList.clearSelection();
		contents.setListData(new Place[1]);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		updateBox.append("Update involving: ");
		updateBox.append(arg1 + "\n");
		refreshActors();
		refreshRooms();

	}

}
