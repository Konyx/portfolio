package world;

import java.util.*;

import place.Place;
import place.Room;
import actor.Actor;
import actor.Person;
import thing.Thing;
import java.util.Observable;

/**
 * Models simple worlds which comprises rooms, persons, and things. Currently
 * stores these and can deliver to clients.
 * 
 * @author Kaan Arik
 * 
 */

public class World extends Observable implements Observer

{

	private Person Neville;
	private Person Phil_Holland;

	private Room Place1;
	private Room Place2;
	private Room Place3;

	private Thing chair;
	private Thing fax;
	private Thing torch;
	private Thing food;
	private Thing tool;
	private Thing book;

	boolean world_empty = true;
	private Collection<Actor> WorldActors = new ArrayList<Actor>();
	private Collection<Thing> WorldThings = new ArrayList<Thing>();
	private Collection<Place> WorldRooms = new ArrayList<Place>();

	/**
	 * Constructor. Makes an empty world; not really very useful unless more
	 * world-building methods are available.
	 */

	public World()

	{

	}

	/**
	 * Constructor. Either makes an empty world or a demo world with sufficient
	 * content to demonstrate the available functionality.
	 * 
	 * @param makingDemo
	 *            - Whether to build a demo world.
	 */

	World(boolean makingDemo)

	{
		world_empty = makingDemo;
		if (world_empty = true) {
			demoWorld();

		}

	}

	/**
	 * Sets up a World. Places People, Rooms and Things in a world.
	 */

	public void demoWorld() {

		chair = new Thing("chair", "Swivel chair");

		fax = new Thing("fax", "A fax machine");
		torch = new Thing("torch", "small LED torch");
		food = new Thing("food", "Some ham sandwhcihes");
		book = new Thing("Big Java", "Horstmann's Big Java Book");
		tool = new Thing("screw driver", "Phillips head screwdriver");

		Place1 = new Room("number = 031", -1, "CSSE lecture theatre");
		Place1.addObserver(this);
		Place1.add(chair);

		Place2 = new Room("number = 247", 2, "SEVG lav");
		Place2.addObserver(this);
		Place2.add(fax);
		Place2.add(torch);
		Place2.add(chair);
		Place3 = new Room("number = 112", 1, "Lab supervisor");
		Place3.addObserver(this);
		Place3.add(torch);
		Place3.add(chair);
		Place3.add(food);
		Place3.add(tool);
		Place3.add(book);
		Place3.add(fax);

		Neville = new Person("Neville");
		Phil_Holland = new Person("Phil Holland");
		Neville.addObserver(this);
		Phil_Holland.addObserver(this);

		Neville.take(book);
		Phil_Holland.take(food);
		Phil_Holland.take(tool);
		Neville.moveTo(Place2);
		Phil_Holland.moveTo(Place3);
		WorldActors.add(Phil_Holland);
		WorldActors.add(Neville);

		WorldRooms.add(Place1);
		WorldRooms.add(Place2);
		WorldRooms.add(Place3);
		WorldThings.add(chair);
		WorldThings.add(fax);
		WorldThings.add(torch);
		WorldThings.add(food);
		WorldThings.add(tool);
		WorldThings.add(book);

	}

	/**
	 * Actors in the world in a (possibly) empty collection of an appropriate
	 * class (e.g., ArrayList).
	 * 
	 * @return Collection of actors.
	 */

	public Collection<Actor> actors()

	{

		return WorldActors;

	}

	/**
	 * Places in the world in a (possibly) empty collection of an appropriate
	 * class (e.g., ArrayList).
	 * 
	 * @return Collection of places.
	 */

	/**
	 * Allows the ability to add a place
	 * 
	 * @param place
	 *            - Place that is being added to the collection.
	 */

	public void addPlace(Place place) {
		WorldRooms.add(place);
		setChanged();
		notifyObservers(place);

	}

	/**
	 * Allows the ability to add actors into the collection.
	 * 
	 * @param actor
	 *            - actor that is being added
	 */

	public void addActor(Actor actor) {
		WorldActors.add(actor);
		setChanged();
		notifyObservers(actor);
	}

	public Collection<Place> places()

	{

		return WorldRooms;

	}

	/**
	 * Items (things) in the world in a (possibly) empty collection of an
	 * appropriate class (e.g., ArrayList).
	 * 
	 * @return Collection of items.
	 */

	public Collection<Thing> items()

	{

		return WorldThings;

	}

	@Override
	public void update(Observable o, Object arg1) {
		setChanged();
		notifyObservers(arg1);

	}

}
