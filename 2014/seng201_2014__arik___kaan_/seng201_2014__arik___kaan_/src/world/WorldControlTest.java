package world;


import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import actor.Person;
import thing.Thing;
import place.Room;

public class WorldControlTest {
	
	World newWorld;
	Thing xbox;
	Thing computer;
	Person kaan;
	Person aydin;
	Room room1;
	Room room2;
	WorldControl controller;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		controller = new WorldControl();
		newWorld = new World();
		controller.setWorld(newWorld);

		

	    xbox = new Thing("xbox", "gaming device");
	    computer = new Thing("apple", "productivity");
		kaan = new Person("Kaan");
		aydin = new Person("Aydin");
		room1 = new Room("Random Room", 1, "Quite Cold");
		room2 = new Room("New room", 1, "Quite Warm");
		kaan.moveTo(room1);
		aydin.moveTo(room2);
		kaan.take(computer);
		room2.add(xbox);
		
		controller.getWorld().actors().add(aydin);
		controller.getWorld().actors().add(kaan);
		controller.getWorld().places().add(room1);
		controller.getWorld().places().add(room2);
		controller.getWorld().items().add(computer);
		controller.getWorld().items().add(xbox);
		

		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDroppControlls() {
		controller.droppControlls(kaan, computer);
		Assert.assertEquals(room1.contents().contains(computer), true);
		Assert.assertEquals(kaan.inventory().contains(computer), false);

		
	}

	@Test
	public void testTakeControlls() {
		controller.takeControlls(aydin, xbox);
		Assert.assertEquals(room2.contents().contains(xbox), false);
		Assert.assertEquals(aydin.inventory().contains(xbox), true);
		
	}

	@Test
	public void testAddPersonControll() {
		Assert.assertEquals((controller.getWorld().actors().size()), 2);
		
		controller.addPersonControll("john", room1);
		Assert.assertEquals((controller.getWorld().actors().toString().contains("john")), true);
		
	}
	
	@Test
	public void testAddAnimalControll() {
		Assert.assertEquals((controller.getWorld().actors().size()), 2);
		controller.addAnimalControll("john", room1);
		Assert.assertEquals((controller.getWorld().actors().size()), 3);
		
	}
	
	
	@Test
	public void testAddRobotControll() {
		Assert.assertEquals((controller.getWorld().actors().size()), 2);
		controller.addRobotControll("john", room1);
		Assert.assertEquals((controller.getWorld().actors().size()), 3);
		
	}

	@Test
	public void testAddPlaceControll() {
		Assert.assertEquals((controller.getWorld().places().size()), 2);
		controller.addPlaceControll("room3", "test room");
		Assert.assertEquals((controller.getWorld().places().size()), 3);
	}
	
	
	@Test
	public void testAddRoomControll() {
		Assert.assertEquals((controller.getWorld().places().size()), 2);
		controller.addRoomControll("room3", 1, "test room");
		Assert.assertEquals((controller.getWorld().places().size()), 3);
	}

	@Test
	public void testAddThingControll() {
		
		Assert.assertEquals((room1.contents().size()), 0);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addThingPlaceControll("Test Item", "An item used for a test", room1);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((room1.contents().size()), 1);
		
	}
	
	@Test
	public void testAddFoodControll() {
		
		Assert.assertEquals((room1.contents().size()), 0);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addFoodPlaceControll("Test Item", "An item used for a test", room1);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((room1.contents().size()), 1);
		
	}
	
	@Test
	public void testAddWeaponControll() {
		
		Assert.assertEquals((room1.contents().size()), 0);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addWeaponPlaceControll("Test Item", "An item used for a test", room1);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((room1.contents().size()), 1);
		
	}

	@Test
	public void testAddThingPersonControll() {
		Assert.assertEquals((kaan.inventory().size()), 1);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addThingPersonControll("Test Item", "An item used for a test", kaan);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((kaan.inventory().size()), 2);
	}
	
	
	@Test
	public void testAddFoodPersonControll() {
		Assert.assertEquals((kaan.inventory().size()), 1);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addFoodPersonControll("Test Item", "An item used for a test", kaan);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((kaan.inventory().size()), 2);
	}
	
	@Test
	public void testAddWeaponPersonControll() {
		Assert.assertEquals((kaan.inventory().size()), 1);
		Assert.assertEquals((controller.getWorld().items().size()), 2);
		controller.addWeaponPersonControll("Test Item", "An item used for a test", kaan);
		Assert.assertEquals((controller.getWorld().items().size()), 3);
		Assert.assertEquals((kaan.inventory().size()), 2);
	}

	@Test
	public void testGoControll() {
		Assert.assertEquals(kaan.location(), room1);
		controller.goControll(kaan, room2);
		Assert.assertEquals(kaan.location(), room2);

	}



}
