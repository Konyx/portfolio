package world;

import java.util.Observable;

import place.Place;
import place.Room;
import thing.Food;
import thing.Thing;
import thing.Weapons;
import actor.Actor;
import actor.Animal;
import actor.Person;
import actor.Robot;

/**
 * A controller that acts as a middle man between the View (GUI the user
 * sees/interacts with) and the Model (World). Actions are performed in the view
 * which then change the model via the Controller.
 * 
 * 
 * 
 * @author Neville (updated by Kaan Arik)
 * 
 */
public class WorldControl extends Observable {

	/*
	 * Model (World)
	 */

	private World w;

	/**
	 * Drops an item from persons inventory and adds it to the persons location.
	 * 
	 * 
	 * @param selectedActor
	 *            - Actor inside a World
	 * @param selectedThing
	 *            - Thing inside a World
	 */

	public void droppControlls(Actor selectedActor, Thing selectedThing) {
		selectedActor.drop(selectedThing);
	}

	/**
	 * Takes an item which exists inside the location of the actor, adds it to
	 * the persons inventory and removes it from actors location.
	 * 
	 * @param selectedActor
	 *            - Actor inside a World
	 * @param selectedThing
	 *            - Thing inside a World
	 */

	public void takeControlls(Actor selectedActor, Thing selectedThing)

	{
		selectedActor.take(selectedThing);
	}

	/**
	 * 
	 * Adds a person to a location inside a world.
	 * 
	 * @param name
	 *            - Name of person
	 * @param person_Location
	 *            - Location the person will be at.
	 */

	public void addPersonControll(String name, Place person_Location)

	{

		Person new_Player = new Person(name);
		new_Player.addObserver(w);
		new_Player.moveTo(person_Location);
		w.addActor(new_Player);

	}

	/**
	 * 
	 * Adds a Animal to a location inside a World.
	 * 
	 * @param name
	 *            - Name of animal
	 * @param person_Location
	 *            - Location the animal will be at.
	 */

	public void addAnimalControll(String name, Place person_Location)

	{
		Animal new_Player = new Animal(name);
		new_Player.addObserver(w);
		new_Player.moveTo(person_Location);
		w.addActor(new_Player);

	}

	/**
	 * 
	 * Adds a Robot to a location inside a World.
	 * 
	 * @param name
	 *            - Name of robot
	 * @param person_Location
	 *            - Location the robot will be at.
	 */

	public void addRobotControll(String name, Place person_Location)

	{
		Robot new_Player = new Robot(name);
		new_Player.addObserver(w);
		new_Player.moveTo(person_Location);
		w.addActor(new_Player);

	}

	/**
	 * Adds a Place into a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the place.
	 * 
	 * @param description
	 *            - Brief description of place.
	 */

	public void addPlaceControll(String name, String description) {
		Place new_Place = new Place(name, description);
		new_Place.addObserver(w);
		w.addPlace(new_Place);

	}

	/**
	 * Adds a Place into a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the room.
	 * @param level
	 *            - Level the room is on.
	 * @param description
	 *            - Brief description of room
	 */

	public void addRoomControll(String name, int level, String description) {
		Room new_Room = new Room(name, level, description);
		new_Room.addObserver(w);
		w.addPlace(new_Room);

	}

	/**
	 * Adds a thing to a place which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the thing.
	 * @param description
	 *            - Brief description of the thing.
	 * 
	 * @param selectedPlace
	 *            - Place inside the world that the thing is being added to.
	 */

	public void addThingPlaceControll(String name, String description,
			Place selectedPlace)

	{
		Thing new_Thing = new Thing(name, description);

		selectedPlace.add(new_Thing);
		w.items().add(new_Thing);

	}

	/**
	 * Adds a weapon in a place which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the weapon.
	 * @param description
	 *            - Brief description of the weapon.
	 * 
	 * @param selectedPlace
	 *            - Place inside the world that the weapon is being added to.
	 */

	public void addWeaponPlaceControll(String name, String description,
			Place selectedPlace)

	{
		Weapons new_Thing = new Weapons(name, description);

		selectedPlace.add(new_Thing);
		w.items().add(new_Thing);

	}

	/**
	 * Adds a Food in a Place which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the food.
	 * @param description
	 *            - Brief description of the food.
	 * 
	 * @param selectedPlace
	 *            - Place inside the World that the food is being added to
	 */

	public void addFoodPlaceControll(String name, String description,
			Place selectedPlace)

	{
		Food new_Thing = new Food(name, description);

		selectedPlace.add(new_Thing);
		w.items().add(new_Thing);
	}

	/**
	 * Adds a food to a actor which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the food
	 * @param description
	 *            - Brief description of the food.
	 * 
	 * @param actor
	 *            - Actor that will be receiving the food inside the World.
	 */

	public void addFoodPersonControll(String name, String description,
			Actor actor)

	{
		Food new_Thing = new Food(name, description);
		actor.take(new_Thing);
		w.items().add(new_Thing);
	}

	/**
	 * Adds a weapon to a actor which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the weapon
	 * @param description
	 *            - Brief description of the weapon.
	 * 
	 * @param actor
	 *            - Actor that will be receiving the weapon inside the World.
	 */

	public void addWeaponPersonControll(String name, String description,
			Actor actor)

	{
		Weapons new_Thing = new Weapons(name, description);
		actor.take(new_Thing);
		w.items().add(new_Thing);
	}

	/**
	 * Adds a thing to a Actor which exists inside a World.
	 * 
	 * 
	 * @param name
	 *            - Name of the thing
	 * @param description
	 *            - Brief description of the thing.
	 * 
	 * @param actor
	 *            - Actor that will be receiving the thing inside the world.
	 */

	public void addThingPersonControll(String name, String description,
			Actor actor)

	{
		Thing new_Thing = new Thing(name, description);
		actor.take(new_Thing);
		w.items().add(new_Thing);
	}

	/**
	 * 
	 * Moves an actor from there current location to a new location.
	 * 
	 * @param selectedActor
	 *            - Person inside a World
	 * @param selectedRoom
	 *            - Room inside a World
	 */

	public void goControll(Actor selectedActor, Place selectedRoom)

	{
		selectedActor.moveTo(selectedRoom);
	}

	/**
	 * Builds a demoWorld.
	 */

	private void buildWorld() {
		w = new World();
		w.demoWorld();

	}

	/**
	 * Allows the View to access the World inside the Controller.
	 * 
	 * @return w - Current world.
	 */

	public World getWorld() {
		return w;
	}

	/**
	 * Allows the ability to set the World the controller is controlling.
	 * 
	 * @param w
	 *            - World that you would like to control.
	 */

	public void setWorld(World w) {
		this.w = w;

	}

	/**
	 * Constructor.
	 */

	public WorldControl() {
		buildWorld();
	}

}
