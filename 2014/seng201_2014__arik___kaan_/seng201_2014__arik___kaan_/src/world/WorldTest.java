package world;


import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import actor.Person;
import place.Place;
import place.Room;
import thing.Thing;


public class WorldTest {
	World w;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		w = new World(true);
		
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testActors() {
		Assert.assertEquals(w.actors().size(), 2);
		Person newPerson = new Person("Test");
		w.actors().add(newPerson);
		Assert.assertEquals(w.actors().size(), 3);
		
		
	}

	@Test
	public void testPlaces() {
		Assert.assertEquals(w.places().size(), 3);
		Room newRoom = new Room("Test Room", 2, "TEST TEST TEST");
		w.places().add(newRoom);
		Assert.assertEquals(w.places().size(), 4);
		Place newPlace= new Place("Test Room", "TEST TEST TEST");
		w.places().add(newPlace);
		Assert.assertEquals(w.places().size(), 5);
	}

	@Test
	public void testItems() {
		Assert.assertEquals(w.items().size(), 6);
		Thing thing = new Thing("Test", "TEST TEST TEST");
		w.items().add(thing);
		Assert.assertEquals(w.items().size(), 7);
	}

}
