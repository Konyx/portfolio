#!/usr/bin/env python

import unittest, io, sys, comparisons, phrase_parser, frequency_nway

# This unit test is worth a total of 14%
# The unit test used in marking will be different but have approximately the
# same weightings.

S = lambda x : x

class TestFrequencyNWay(unittest.TestCase):
    def reset(self):
        frequency_nway.COUNTER.reset()

    def lock(self):
        pass

    def assertCounterEquals(self, count):
        self.assertEqual(count, frequency_nway.COUNTER.count)

    def test_find_minimum(self):
        """Ensure that the minimum index is returned. (2%)"""

        self.reset()
        
        phrases = [
            [(S("1"),), (S("3"),)],
            [(S("2"),), (S("4"),)]
        ]

        self.assertEqual(0, frequency_nway.find_minimum([0, 0], phrases))
        self.assertEqual(1, frequency_nway.find_minimum([1, 0], phrases))
        self.assertEqual(0, frequency_nway.find_minimum([0, 1], phrases))
        self.assertEqual(0, frequency_nway.find_minimum([1, 1], phrases))
        
        self.lock()
    
    def test_find_minimum_counter(self):
        """Ensure that the minimum index is returned. (2%)"""
        
        self.test_find_minimum()
        
        self.assertCounterEquals(4)

    def test_merge_phrase_empty(self):
        """Check that the last phrase is merged in correctly when the list is
        empty. (1%)"""
        
        self.reset()
        
        merged = []
        
        frequency_nway.merge_phrase(merged, [S("A"), [0, 1]])
        
        self.lock()
        
        self.assertEqual(1, len(merged))
    
    def test_merge_phrase_empty_counter(self):
        """Check that the last phrase is merged in correctly when the list is
        empty. (1%)"""
        
        self.test_merge_phrase_empty()
        
        self.assertCounterEquals(0)
    
    def test_merge_phrase(self):
        """Check that the last phrase is merged in correctly. (2%)"""
        
        self.reset()
        
        merged = [
            [S("A"), [0, 1]]
        ]
        
        frequency_nway.merge_phrase(merged, [S("A"), [0, 1]])
        
        self.lock()
        
        self.assertEqual([S("A"), [0, 2]], merged[0])
        self.assertEqual(1, len(merged))
    
    def test_merge_phrase_empty_counter(self):
        """Check that the last phrase is merged in correctly when the list is
        empty. (2%)"""
        
        self.test_merge_phrase()
        
        self.assertCounterEquals(1)

    def test_calculate_frequencies(self):
        """Test that texts are merged correctly. (2%)"""
        
        self.reset()
        
        paths = ["texts/emma60.txt", "texts/mp60.txt", "texts/pnp60.txt", "texts/sns60.txt", "texts/unknown60.txt"]
        
        texts = phrase_parser.load_texts(paths, 3)
        texts = [[[S(phrase[0]), phrase[1]] for phrase in text] for text in texts]
        
        frequencies = frequency_nway.calculate_frequencies(texts)
        
        self.lock()
        
        self.assertEqual(2897, len(frequencies))
    
    def test_calculate_frequencies_counter(self):
        """Test that texts are merged correctly. (2%)"""
        
        self.test_calculate_frequencies()
        
        self.assertCounterEquals(14745)


if __name__ == '__main__':
    print("This unit test is worth a total of 14%.", file=sys.stderr)
    unittest.main(exit=False)
