#!/usr/bin/env python

"""
A program to calculate the correlation between texts.
Kaan Arik, 84254373. 10/10/13.
"""

from frequency_linear import flatten_phrases, merge_phrases, COUNTER
import phrase_parser, correlation, collections, sys, time, comparisons

def calculate_frequencies(texts):
    """
    Merge the phrases for each text, returning a list of [phrase, frequencies]
    pairs in order, such that phrase only appears once.
    """
    
    texts = [flatten_phrases(text) for text in texts]
    texts.sort(key = len)
    merged = collections.deque(texts)
    
    while len(merged) > 1:
        first_phrase = merged.popleft()
        second_phrase = merged.popleft()
        merged.append(merge_phrases(first_phrase, second_phrase))
        
    # <--[ WRITE CODE HERE ]--
    
    return merged.pop()


def main(args):
    """Usage: python frequency_balanced.py phrase_length a.txt b.txt"""
    
    if len(args) >= 3:
        phrase_length = int(args[1])
        paths = args[2:]
        
        start = time.clock()
        texts = phrase_parser.load_texts(paths, phrase_length)
        end = time.clock()
        print("Time to load phrases =", (end - start))
        
        start = time.clock()
        merged = calculate_frequencies(texts)
        end = time.clock()
        print("Time to calculate", len(merged), "frequencies =", (end - start))
        
        start = time.clock()
        similarities = correlation.calculate_correlations(len(paths), merged)
        end = time.clock()
        print("Time to calculate correlations =", (end - start))
        
        print("Number of comparisons:", COUNTER.count)
        
        print("Checksum", comparisons.digest(merged))
        
        print("Phrases of length", phrase_length)
        for rank in similarities:
            print(paths[rank[0]], paths[rank[1]], rank[2])
    else:
        print(main.__doc__)


if __name__ == "__main__":
    main(sys.argv)
