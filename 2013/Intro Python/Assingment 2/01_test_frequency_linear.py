#!/usr/bin/env python

import unittest, io, sys, comparisons, phrase_parser, frequency_linear

# This unit test is worth a total of 20%
# The unit test used in marking will be different but have approximately the
# same weightings.

S = lambda x : x

class TestFrequencyLinear(unittest.TestCase):
    def reset(self):
        frequency_linear.COUNTER.reset()

    def lock(self):
        pass

    def assertCounterEquals(self, count):
        self.assertEqual(count, frequency_linear.COUNTER.count)
    
    def test_flatten_phrases(self):
        """Test that the flatten phrases is returning a unique list. (2%)"""
        
        self.reset()
        
        phrases = [
            [S("A"), [0, 1]],
            [S("A"), [0, 1]],
            [S("B"), [0, 1]],
            [S("C"), [0, 1]],
            [S("C"), [0, 1]],
        ]
        
        flattened_phrases = frequency_linear.flatten_phrases(phrases)
        
        self.lock()
        
        self.assertEqual(3, len(flattened_phrases))
        self.assertEqual([
            [S("A"), [0, 2]],
            [S("B"), [0, 1]],
            [S("C"), [0, 2]],
        ], flattened_phrases)
    
    def test_flatten_phrases_counter(self):
        """Test that the flatten phrases is returning a unique list. (2%)"""
        
        self.test_flatten_phrases()
        
        self.assertCounterEquals(4)
    
    def test_merge_phrases(self):
        """Test that the merge phrases function is working correctly. (4%)"""
        
        self.reset()
        
        phrases_a = [
            [S("A"), [0, 1]],
            [S("B"), [0, 1]],
            [S("C"), [0, 1]],
            [S("D"), [0, 1]],
        ]
        
        phrases_b = [
            [S("B"), [1, 0]],
            [S("D"), [1, 0]],
            [S("a"), [1, 0]],
            [S("c"), [1, 0]],
        ]
        
        merged = frequency_linear.merge_phrases(phrases_a, phrases_b)
        
        self.lock()
        
        self.assertEqual([
            ['A', [0, 1]],
            ['B', [1, 1]],
            ['C', [0, 1]],
            ['D', [1, 1]],
            ['a', [1, 0]],
            ['c', [1, 0]],
        ], merged)
    
    def test_merge_phrases_counter(self):
        """Test that the merge phrases function is working correctly. (4%)"""
        
        self.test_merge_phrases()
        self.assertCounterEquals(6)
    
    def test_calculate_frequencies(self):
        """Test that texts are merged correctly. (4%)"""
        
        self.reset()
        
        paths = ["texts/emma60.txt", "texts/mp60.txt", "texts/pnp60.txt", "texts/sns60.txt", "texts/unknown60.txt"]
        
        texts = phrase_parser.load_texts(paths, 3)
        texts = [[[S(phrase[0]), phrase[1]] for phrase in text] for text in texts]
        
        frequencies = frequency_linear.calculate_frequencies(texts)
        
        self.lock()
        
        self.assertEqual(2897, len(frequencies))
    
    def test_calculate_frequencies_counter(self):
        """Test that texts are merged correctly. (4%)"""
        
        self.test_calculate_frequencies()
        
        self.assertCounterEquals(13337)


if __name__ == '__main__':
    print("This unit test is worth a total of 20%.", file=sys.stderr)
    unittest.main(exit=False)
