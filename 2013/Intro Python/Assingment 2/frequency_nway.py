#!/usr/bin/env python

"""
A program to calculate the correlation between texts.
Kaan Arik, 84254373. 10/10/13.
"""

import sys, time, comparisons, phrase_parser, correlation
from frequency_linear import COUNTER

def find_minimum(top, texts):
    """
    Given a list of indices and corresponding texts, find the index for the
    lexicographically smallest phrase.
    """
    
    
    
    
    min_index = 0
    cur_index = 1
    
    while cur_index < len(texts):
        
        COUNTER.increment()
        if texts[cur_index][top[cur_index]] < texts[min_index][top[min_index]]:
            min_index = cur_index
            cur_index += 1
        else:
            min_index = min_index
            cur_index += 1

    
    return min_index
    


def merge_phrase(merged, phrase):
    """
    If the given phrase can be merged in with the last item in merged, do so
    and return True, otherwise return False.
    """
    mergable = False
    if len(merged) == 0:
        #COUNTER.increment()
        merged.append(phrase)
    else:
        COUNTER.increment()
        if merged[-1][0] != phrase[0]:
            merged.append(phrase)
            
        else:
            merged_freq = merged[-1][1]
            phrase_freq = phrase[1]
            new_freq = [sum(i) for i in zip(merged_freq, phrase_freq)] 
            merged[-1][1] = new_freq
            mergable =  True
            
    return mergable

def calculate_frequencies(texts):
    """
    Merge the phrases for each text, returning a list of [phrase, frequencies]
    pairs in order, such that phrase only appears once.
    """
    
    merged = []
    top = [0] * len(texts)

    while len(texts) > 0:

        index = find_minimum(top, texts)
        merge_phrase(merged, texts[index][top[index]])
        if texts[index][top[index]] == texts[index][-1]:
            texts.remove(texts[index])
            top.remove(top[index])
        else:
            top[index] += 1
   
    #merged += texts[0][top[0]:]
    

        


    return merged



def main(args):
    """Usage: python frequency_nway.py phrase_length a.txt b.txt"""
    
    if len(args) >= 3:
        phrase_length = int(args[1])
        paths = args[2:]
        
        start = time.clock()
        texts = phrase_parser.load_texts(paths, phrase_length)
        end = time.clock()
        print("Time to load phrases =", (end - start))
        
        start = time.clock()
        merged = calculate_frequencies(texts)
        end = time.clock()
        print("Time to calculate", len(merged), "frequencies =", (end - start))
        
        start = time.clock()
        similarities = correlation.calculate_correlations(len(paths), merged)
        end = time.clock()
        print("Time to calculate correlations =", (end - start))
        
        print("Number of comparisons:", COUNTER.count)
        
        print("Checksum", comparisons.digest(merged))
        
        print("Phrases of length", phrase_length)
        for rank in similarities:
            print(paths[rank[0]], paths[rank[1]], rank[2])
    else:
        print(main.__doc__)


if __name__ == "__main__":
    main(sys.argv)

