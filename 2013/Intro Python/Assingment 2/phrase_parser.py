#!/usr/bin/env python
"""
A set of functions for extracting n-word phrases.
"""

import sys, collections, re, comparisons

COUNTER = comparisons.Counter()

WORD_PATTERN = r"((?:[a-zA-Z0-9]|(?<=[a-zA-Z0-9])(?:-|')(?=[a-zA-Z0-9]))+)"

def words_in_line(line):
    "Returns a list of words from a line by removing formatting."
    
    line = line.replace('_','')
    words = re.findall(WORD_PATTERN, line)
    return [x.lower() for x in words]


def extract_phrases(input_buffer, phrase_length):
    "Return a list of phrases of length phrase_length from an input buffer."
    
    word_deque = collections.deque()
    phrases = []
    
    for line in input_buffer:
        for word in words_in_line(line):
            word_deque.append(word)
            
            if len(word_deque) == phrase_length:
                phrases.append(' '.join(word_deque))
                word_deque.popleft()
    
    return phrases


def load_texts(paths, phrase_length):
    """Load lists of [phrase,frequency] pairs for each path."""
    texts = []
    
    for index, path in enumerate(paths):
        initial_frequency = [0] * len(paths)
        initial_frequency[index] = 1
        
        with open(path) as input_buffer:
            phrases = extract_phrases(input_buffer, phrase_length)
            
            phrases.sort()
            
            text = [[phrase, initial_frequency] for phrase in phrases]
            
            texts.append(text)
    
    return texts


def print_text(text, width = 40):
    """Print out the [phrase,frequency] pairs."""
    
    print('[')
    for item in text:
        phrase_bit = (repr(item[0]) + ',').ljust(width)
        freq_list = repr(item[1])
        
        print("\t[{0}{1}],".format(phrase_bit, freq_list))
    print(']')


def main(args):
    "Usage: python phrase_parser.py phrase_length file1.txt file2.txt"
    
    if len(args) > 3:
        phrase_length = int(args[1])
        paths = args[2:]
        
        texts = load_texts(paths, phrase_length)
        
        for index, text in enumerate(texts):
            print(paths[index])
            print_text(text, phrase_length * 12)
    else:
        print(main.__doc__)

if __name__ == "__main__":
    main(sys.argv)
