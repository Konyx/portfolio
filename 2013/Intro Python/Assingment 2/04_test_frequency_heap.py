#!/usr/bin/env python

import unittest, io, sys, comparisons, phrase_parser, frequency_heap

# This unit test is worth a total of 20%
# The unit test used in marking will be different but have approximately the
# same weightings.

S = lambda x : x

class TestFrequencyHeap(unittest.TestCase):
    def reset(self):
        frequency_heap.COUNTER.reset()

    def lock(self):
        pass

    def assertCounterEquals(self, count):
        self.assertEqual(count, frequency_heap.COUNTER.count)

    def test_heapify(self):
        """Ensure that the heap is structured correctly. (2%)"""
        
        self.reset()
        
        items = [(S(5),), (S(4),), (S(3),), (S(2),), (S(1),)]

        frequency_heap.heapify(items)
        
        self.lock()
        
        self.assertEqual([(1,), (2,), (3,), (5,), (4,)], items)
    
    def test_heapify_counter(self):
        """Ensure that the heap is structured correctly. (2%)"""
        
        self.test_heapify()
        
        self.assertCounterEquals(6)

    def test_siftdown(self):
        """Ensure that siftdown is working correctly. (2%)"""
        
        self.reset()
        
        items = [(S(10),), (S(2),), (S(3),), (S(5),), (S(4),)]
        frequency_heap.siftdown(items, 0)
        
        self.lock()
        
        self.assertEqual([(2,), (4,), (3,), (5,), (10,)], items)
    
    def test_siftdown_counter(self):
        """Ensure that siftdown is working correctly. (2%)"""
        
        self.test_siftdown()
        
        self.assertCounterEquals(4)
    
    def test_siftdown_partial(self):
        """Test that you are not too sifty (2%)"""
        
        items = [(S(10),), (S(9),), (S(8),), (S(13),), (S(12),), (S(18),), (S(20),)]
        
        self.reset()
        
        frequency_heap.siftdown(items, 0)
        
        self.lock()
        
        self.assertEqual([(S(8),), (S(9),), (S(10),), (S(13),), (S(12),), (S(18),), (S(20),)], items)
    
    def test_siftdown_partial_counter(self):
        """Ensure that your siftyness is at the right level (2%)"""
        self.test_siftdown_partial()
        
        self.assertCounterEquals(4)
    
    def test_siftdown_insertion(self):
        """Ensure that siftdown is working correctly. (2%)"""
        
        self.reset()
        
        items = [(S(2),), (S(8),), (S(4),), (S(3),), (S(5),), (S(10),)]
        frequency_heap.siftdown(items, 1)
        
        self.lock()
        
        self.assertEqual([(2,), (3,), (4,), (8,), (5,), (10,)], items)
    
    def test_siftdown_insertion_counter(self):
        """Ensure that siftdown is working correctly. (2%)"""
        
        self.test_siftdown_insertion()
        
        self.assertCounterEquals(2)

    def test_calculate_frequencies(self):
        """Test that texts are merged correctly. (2%)"""
        
        self.reset()
        
        paths = ["texts/emma60.txt", "texts/mp60.txt", "texts/pnp60.txt", "texts/sns60.txt", "texts/unknown60.txt"]
        
        texts = phrase_parser.load_texts(paths, 3)
        texts = [[[S(phrase[0]), phrase[1]] for phrase in text] for text in texts]
        
        frequencies = frequency_heap.calculate_frequencies(texts)
        
        self.lock()
        
        self.assertEqual(2897, len(frequencies))
    
    def test_calculate_frequencies_counter(self):
        """Test that texts are merged correctly. (2%)"""
        
        self.test_calculate_frequencies()
        
        self.assertCounterEquals(11852)


if __name__ == '__main__':
    print("This unit test is worth a total of 20%.", file=sys.stderr)
    unittest.main(exit=False)
