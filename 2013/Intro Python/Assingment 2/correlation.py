
import math

def pearson_correlation(points):
    '''
    Return pearson correlation coefficient for points.
    The argument must be a list in [(x1,y1),(x2,y2),(x3,y3),...] format.
    '''
    
    sum_x = 0
    sum_y = 0
    sum_xy = 0
    sum_xx = 0
    sum_yy = 0
    
    n = len(points)
    
    for x,y in points:
        sum_x += x
        sum_y += y
        sum_xy += x*y
        sum_xx += x*x
        sum_yy += y*y
    
    try:
        r = (sum_xy - (sum_x*sum_y)/n)/math.sqrt((sum_xx-sum_x*sum_x/n)*(sum_yy-sum_y*sum_y/n))
    except ZeroDivisionError:
        # No correlation:
        return 0.0
    
    return abs(r)

def calculate_correlations(columns, phrases):
    correlations = []
    
    for i in range(columns):
        for j in range(i+1, columns):
            data = []
            
            for phrase in phrases:
                if phrase[1][i] > 0 and phrase[1][j] > 0:
                    data.append((phrase[1][i], phrase[1][j],))
            
            correlations.append([i, j, pearson_correlation(data)])

    return correlations