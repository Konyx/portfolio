#!/usr/bin/env python

"""
A program to calculate the correlation between texts.
Kaan Arik, Student ID: 84254373. 10/10/13.
"""

import sys, time, comparisons, phrase_parser, correlation

COUNTER = comparisons.Counter()

def sum_frequencies(counts_a, counts_b):
    """
    Calculate the pairwise sum of two arrays.
    """
    combined_count = []
    
    for items in range(0, len(counts_a) - 1):
        combined_count.append(counts_a[items] + counts_b[items])
    return combined_count

def flatten_phrases(phrases):
    """
    Remove duplicate phrases from a sorted list of [phrase, frequency]
    pairs. Sum the frequencies for items which are the same.
    """
    merged = []
    merged.append(phrases[0]) 
    for index in range(1, len(phrases)):
        COUNTER.increment()
        prev_phrase = phrases[index - 1][0]
        curr_phrase = phrases[index][0]
        if prev_phrase == curr_phrase:
            old_flattened_freq = merged[-1][1]
            curr_phrase_freq = phrases[index][1] 
            new_flattened_freq = [sum(i) for i in 
            zip(old_flattened_freq, curr_phrase_freq)]            
            merged[-1][1] = new_flattened_freq
        else:
            merged.append(phrases[index])
    return merged

    



def merge_phrases(phrases_a, phrases_b):
    """
    Merge two sorted lists of [phrase,frequency] pairs into a single list.
    Frequencies for the same phrase should be summed together.
    Note: Each list is assumed to contain no duplicates.
    """
    
    merged = []

    i, j = 0, 0
    
    while i < len(phrases_a) and j < len(phrases_b):
        COUNTER.increment()
        if phrases_a[i][0] < phrases_b[j][0]:
            merged.append(phrases_a[i])
            i += 1
        else:
            COUNTER.increment()
            if phrases_a[i][0] > phrases_b[j][0]:
                merged.append(phrases_b[j])
                j += 1
            else:
                merged.append(phrases_a[i])
                phrase_1_freq = phrases_a[i][1]
                phrase_2_freq = phrases_b[j][1]
                new_merged_freq = [sum(i) for i in 
                zip(phrase_1_freq, phrase_2_freq)]            
                merged[-1][1] = new_merged_freq 
                i += 1
                j += 1
                
    merged += phrases_a[i:]
    merged += phrases_b[j:]

    return merged


def calculate_frequencies(texts):
    """
    Merge the phrases for each text, returning a list of [phrase, frequencies]
    pairs in order, such that each phrase only appears once.
    """
 
    merged = []
    phrases = []
  
   
    for i in texts:
        phrases.append(flatten_phrases(i))
    for j in range(0, len(phrases)):
        merged = merge_phrases(merged, phrases[j])
        

    return merged


def main(args):
    """Usage: python frequency_linear.py phrase_length a.txt b.txt"""
    
    if len(args) >= 3:
        phrase_length = int(args[1])
        paths = args[2:]
        
        start = time.clock()
        texts = phrase_parser.load_texts(paths, phrase_length)
        end = time.clock()
        print("Time to load phrases =", (end - start))
        
        start = time.clock()
        merged = calculate_frequencies(texts)
        end = time.clock()
        print("Time to calculate", len(merged), "frequencies =", (end - start))
        
        start = time.clock()
        similarities = correlation.calculate_correlations(len(paths), merged)
        end = time.clock()
        print("Time to calculate correlations =", (end - start))
        
        print("Number of comparisons:", COUNTER.count)
        
        print("Checksum", comparisons.digest(merged))
        
        print("Phrases of length", phrase_length)
        for rank in similarities:
            print(paths[rank[0]], paths[rank[1]], rank[2])
    else:
        print(main.__doc__)


if __name__ == "__main__":
    main(sys.argv)
