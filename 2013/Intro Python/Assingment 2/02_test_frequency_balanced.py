#!/usr/bin/env python

import unittest, io, sys, comparisons, phrase_parser, frequency_balanced

# This unit test is worth a total of 6%
# The unit test used in marking will be different but have approximately the
# same weightings.

S = lambda x : x

class TestFrequencyBalanced(unittest.TestCase):
    def reset(self):
        frequency_balanced.COUNTER.reset()

    def lock(self):
        pass

    def assertCounterEquals(self, count):
        self.assertEqual(count, frequency_balanced.COUNTER.count)

    def test_calculate_frequencies(self):
        """Test that texts are merged correctly. (3%)"""
        
        self.reset()
        
        paths = ["texts/emma60.txt", "texts/mp60.txt", "texts/pnp60.txt", "texts/sns60.txt", "texts/unknown60.txt"]
        
        texts = phrase_parser.load_texts(paths, 3)
        texts = [[[S(phrase[0]), phrase[1]] for phrase in text] for text in texts]
        
        frequencies = frequency_balanced.calculate_frequencies(texts)
        
        self.lock()
        
        self.assertEqual(2897, len(frequencies))
    
    def test_calculate_frequencies_counter(self):
        """Test that texts are merged correctly. (3%)"""
        
        self.test_calculate_frequencies()
        
        self.assertCounterEquals(13523)


if __name__ == '__main__':
    print("This unit test is worth a total of 6%.", file=sys.stderr)
    unittest.main(exit=False)
