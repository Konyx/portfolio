#!/usr/bin/env python

"""
A program to calculate the correlation between texts.
Kaan Arik, 84254373. 10/10/13.
"""

import sys, time, comparisons, phrase_parser, correlation
from frequency_nway import merge_phrase, COUNTER

def siftdown(heap, pos):
    """
    The child indices of heap index pos are already heaps, and we want to make
    a heap at index pos too. Move the item heap[pos] down the tree by swapping
    with the smallest child to restore the heap invariant.
    """
    
    # --[ WRITE CODE HERE ]-->
    endpos = len(heap)
    startpos = pos
    leftchildpos = 2*pos + 1
    #current_node = 0 # leftmost child position

    # Repeatedly move the item heap[pos] down the tree by swapping with the
    # smallest child to restore the heap invariant.
    # <--[ WRITE CODE HERE ]--
    
    while leftchildpos < endpos:
        min_child_pos = leftchildpos
        right_child_pos = leftchildpos + 1
        
        if right_child_pos < endpos:
            COUNTER.increment()
            if heap[right_child_pos][0] < heap[leftchildpos][0]:
                min_child_pos = right_child_pos
        COUNTER.increment()
        if  heap[min_child_pos][0] < heap[startpos][0]:
            temp = heap[startpos]
            heap[startpos] = heap[min_child_pos]
            heap[min_child_pos] = temp
            startpos = min_child_pos
            leftchildpos = startpos*2+1
        else:
            break
        # ----------UNTILL HERE---------------



def heapify(items):
    """Transform list into a heap, in-place, in O(|items|) time."""
    # --[ WRITE CODE HERE ]-->
    # Update items into a min-heap using the in-place heapify algorithm.
    # all the last part of the list will be valid heaps, ie, nodes with no
    # children. Make a reversed list from len//2 to 0, eg, 
    # if list has ten items then want 5, 4, 3, 2, 1, 0 and use siftdown on
    # these indices.
    # <--[ WRITE CODE HERE ]--
    half_length = len(items)//2
    for i in range(half_length, -1, -1):
        siftdown(items, i)
    
def calculate_frequencies(texts):
    """
    Merge the phrases for each text, returning a list of [phrase, frequencies]
    pairs in order, such that phrase only appears once.
    """
    
    # --[ WRITE CODE HERE ]-->
    # Initialise the heap with lists containing the event in the 0th position.
    merged = []
    heap = []

    
    # Prime the heap:
    for text in texts:
        heap.append([text[0][0], 0, text])
    
    # Firstly you need to heapify the items in heap.

    heapify(heap)
    
    while len(heap) > 0:
        top_item = heap[0]
        top_text = top_item[2]
        top_index = top_item[1]
        top_phrase_freq = top_text[top_index]
        merge_phrase(merged, top_phrase_freq)
        top_item[1] = top_index + 1
        top_index = top_item[1]
        if top_index < len(top_text):
            text_phrase_freq = top_text[top_index]
            heap[0][0] = text_phrase_freq[0]
        else:
            if len(heap) > 1:
                heap[0] = heap.pop()
            else:
                heap.pop()
        siftdown(heap, 0)
        


    return merged


def main(args):
    """Usage: python frequency_heap.py phrase_length a.txt b.txt"""
    
    if len(args) >= 3:
        phrase_length = int(args[1])
        paths = args[2:]
        
        start = time.clock()
        texts = phrase_parser.load_texts(paths, phrase_length)
        end = time.clock()
        print("Time to load phrases =", (end - start))
        
        start = time.clock()
        merged = calculate_frequencies(texts)
        end = time.clock()
        print("Time to calculate", len(merged), "frequencies =", (end - start))
        
        start = time.clock()
        similarities = correlation.calculate_correlations(len(paths), merged)
        end = time.clock()
        print("Time to calculate correlations =", (end - start))
        
        print("Number of comparisons:", COUNTER.count)
        
        print("Checksum", comparisons.digest(merged))
        
        print("Phrases of length", phrase_length)
        for rank in similarities:
            print(paths[rank[0]], paths[rank[1]], rank[2])
    else:
        print(main.__doc__)


if __name__ == "__main__":
    main(sys.argv)
