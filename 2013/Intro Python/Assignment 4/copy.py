'''implements ancestors query to determine heritage'''
def extractrelationshipdata(lines_list):
    '''gets data'''
    rel_dic = {}
    for line in lines_list:
        rel_list = line.split(':')  
        mother = rel_list[0].strip()
        child_list = rel_list[1].split(',')  
        clean_child_list = []
        for child in child_list:
            clean_child_list.append(child.strip())
        rel_dic[mother] = clean_child_list
    return rel_dic


def splitfile(lines_list):
    '''cleans data up'''
    clean_lines_list = []
    for line in lines_list:
        clean_lines_list.append(line.strip())
    lines_list = clean_lines_list
    index = lines_list.index('')
    
    return (lines_list[0:index], lines_list[index + 1:])


def processmotherquery(ret_dict, query):
    '''processes mother query'''
    child = query.replace('mother', '').strip()
    for mother, children in ret_dict.items():
        if child in children:
            return mother
    return 'Mother not known'

def processancestorsquery(ret_dict, query):
    person = query.replace('ancestors', '').strip()
    return_str = ancestorsqueryerror(ret_dict, person)
    if return_str == '':
        ancestors_list = []
        while True:
            person_not_found = True
            for mother, children in ret_dict.items():
                if person in children:
                    ancestors_list.append(mother)
                    person = mother
                    person_not_found = False
                    
            if person_not_found:
                return_str = ', '.join(ancestors_list)
                break

    return return_str


def ancestorsqueryerror(ret_dict, person):
    unknown_person = True
    no_known_ancestors = True
    for mother, children in ret_dict.items():          
        if person in mother:             
            unknown_person = False
        if person in children:
            unknown_person = False
            no_known_ancestors = False
            break

    error = ''
    if unknown_person:
        error = 'Unknown person'
    elif no_known_ancestors:
        error = 'No known ancestors'
    return error

def processquery(rel_dict, query):
    '''process queries'''
    if 'mother' in query:
        return processmotherquery(rel_dict, query)
    elif 'ancestors' in query:
        return processancestorsquery(rel_dict, query)

def processallqueries(rel_dict, queries):
    '''process all queries'''
    results_list = []
    for query in queries:
        results_list.append(processquery(rel_dict, query))
    return results_list
        

# Step 1: Creating input file
test_file = open('relationships.txt', 'w')
test_file.write('''Elizabeth: Peter, Angela, Thomas
Mary: Tom
Angela: Fred, Alison
Alison: Beatrice, Dick, Harry


ancestors Tom
ancestors Harry
ancestors Bernard
ancestors Mary
ancestors Beatrice
mother Dick
''')
test_file.close()

# Step 2: Read lines.
TEST_FILE = open('relationships.txt', 'r')
FILE_LINE_LIST = TEST_FILE.readlines()
TEST_FILE.close()


# Step 3: Split file up.
REL_LINE_LIST, QUERY_LINE_LIST = splitfile(FILE_LINE_LIST)
#print(rel_lines_list)
#print(query_lines_list)

# Step 4: Parse lines. Create relationship dictionary.
REL_DIC = extractrelationshipdata(REL_LINE_LIST)
#print(rel_dict)


# Step 5: Querying the relationship data.
RESULTS_LIST = processallqueries(REL_DIC, QUERY_LINE_LIST)
for result in RESULTS_LIST:
    print(result)