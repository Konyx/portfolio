def extractRelationshipData(lines_list):
    rel_dict = {}
    for line in lines_list:
        rel_list = line.split(':')  # [�mother�, � child1, child2, ...�]
        mother = rel_list[0].strip()
        child_list = rel_list[1].split(',')  # [�child1�, �child2�, �]
        clean_child_list = []
        for child in child_list:
            clean_child_list.append(child.strip())
        rel_dict[mother] = clean_child_list  # {�mother�: [�child1�, �child2�, �]}
    return rel_dict


def splitFile(lines_list):
    clean_lines_list = []
    for line in lines_list:
        clean_lines_list.append(line.strip())
    lines_list = clean_lines_list
    index = lines_list.index('')
    
    return (lines_list[0:index], lines_list[index + 1:])


def processMotherQuery(ret_dict, query):
    child = query.replace('mother', '').strip()
    for mother, children in ret_dict.items():
        if child in children:
            return mother
    return 'Mother not known'

def processAncestorsQuery(ret_dict, query):
    person = query.replace('ancestors', '').strip()

    unknown_person = True
    no_known_ancestors = True
    for mother, children in ret_dict.items():          
        if person in mother:             
            unknown_person = False
        if person in children:
            unknown_person = False
            no_known_ancestors = False
            break

    if unknown_person:
        return 'Unknown person'
    elif no_known_ancestors:
        return 'No known ancestors'       

    ancestors_list = []
    while True:
        person_not_found = True
        for mother, children in ret_dict.items():
            if person in children:
                ancestors_list.append(mother)
                person = mother
                person_not_found = False
                
        if person_not_found:
            return ', '.join(ancestors_list)
    

def processQuery(rel_dict, query):
    if 'mother' in query:
        return processMotherQuery(rel_dict, query)
    elif 'ancestors' in query:
        return processAncestorsQuery(rel_dict, query)
    else:
        return ''


def processAllQueries(rel_dict, queries):
    results_list = []
    for query in queries:
        results_list.append(processQuery(rel_dict, query))
    return results_list
        

# Step 1: Creating input file
test_file = open('relationships.txt', 'w')
test_file.write('''Elizabeth: Peter, Angela, Thomas
Mary: Tom
Angela: Fred, Alison
Alison: Beatrice, Dick, Harry

mother Elizabeth
mother Tom
mother Angela
mother Gurgle
ancestors Tom
ancestors Harry
ancestors Bernard
ancestors Mary
ancestors Beatrice
mother Dick
''')
test_file.close()

# Step 2: Read lines.
test_file = open('relationships.txt', 'r')
file_lines_list = test_file.readlines()
test_file.close()


# Step 3: Split file up.
rel_lines_list, query_lines_list = splitFile(file_lines_list)
#print(rel_lines_list)
#print(query_lines_list)

# Step 4: Parse lines. Create relationship dictionary.
rel_dict = extractRelationshipData(rel_lines_list)
#print(rel_dict)


# Step 5: Querying the relationship data.
results_list = processAllQueries(rel_dict, query_lines_list)
for result in results_list:
    print(result)