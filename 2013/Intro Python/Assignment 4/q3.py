def extractRelationshipData(lines_list):
    rel_dict = {}
    for line in lines_list:
        rel_list = line.split(':')  # [�mother�, � child1, child2, ...�]
        mother = rel_list[0].strip()
        child_list = rel_list[1].split(',')  # [�child1�, �child2�, �]
        clean_child_list = []
        for child in child_list:
            clean_child_list.append(child.strip())
        rel_dict[mother] = clean_child_list  # {�mother�: [�child1�, �child2�, �]}
    return rel_dict


def splitFile(lines_list):
    clean_lines_list = []
    for line in lines_list:
        clean_lines_list.append(line.strip())
    lines_list = clean_lines_list
    index = lines_list.index('')
    
    return (lines_list[0:index], lines_list[index + 1:])


def processMotherQuery(ret_dict, query):
    child = query.replace('mother', '').strip()
    for mother, children in ret_dict.items():
        if child in children:
            return mother
    return 'Mother not known'

def processAncestorsQuery(ret_dict, query):
    person = query.replace('ancestors', '').strip()

    unknown_person = True
    no_known_ancestors = True
    for mother, children in ret_dict.items():          
        if person in mother:             
            unknown_person = False
        if person in children:
            unknown_person = False
            no_known_ancestors = False
            break

    if unknown_person:
        return 'Unknown person'
    elif no_known_ancestors:
        return 'No known ancestors'       

    ancestors_list = []
    while True:
        person_not_found = True
        for mother, children in ret_dict.items():
            if person in children:
                ancestors_list.append(mother)
                person = mother
                person_not_found = False
                
        if person_not_found:
            return ', '.join(ancestors_list)
    

def processRelationshipQuery(rel_dict, query):
    query_args = query.replace('relationship', '').strip()
    people_list = query_args.split(' ')
    person_xxx = people_list[0]
    person_yyy = people_list[1]
    
    relationship_str = ''
    relationship_str = searchAncestorTree(rel_dict, person_xxx, person_yyy)
    if 'Unknown relationship' in relationship_str:  #First search failed. Nothing found from search.
        relationship_str = searchAncestorTree(rel_dict, person_yyy, person_xxx)
    
    return relationship_str


def searchAncestorTree(rel_dict, child, relation): 
    ancestor_iter = 0
    orig_child = child
    search_run = True
    while search_run:
        child_not_found = True
        for mother, children in rel_dict.items():
            if child in children:
                child_not_found = False
                ancestor_iter += 1
                if relation in mother:    
                    search_run = False
                else:
                    child = mother
                    break
                
        if child_not_found:
            return 'Unknown relationship'
    
    return generateRelationshipStr(ancestor_iter, orig_child, relation)
    
    
def generateRelationshipStr(ancestor_iter, child, relation):
    GREAT_OFFSET = 2
    return_str = relation + ' %s ' + child
    if ancestor_iter == 1:
        return (return_str % 'is the mother of')    
    elif ancestor_iter >= 2:
        relationship_str = 'is the %sgrandmother of'
        great_str = ''
        for i in range(0, ancestor_iter - GREAT_OFFSET):
            great_str = great_str + 'great-'         
        relationship_str = relationship_str % great_str
        return (return_str % relationship_str)
                    

def processQuery(rel_dict, query):
    if 'mother' in query:
        return processMotherQuery(rel_dict, query)
    elif 'ancestors' in query:
        return processAncestorsQuery(rel_dict, query)
    elif 'relationship' in query:
        return processRelationshipQuery(rel_dict, query)
    else:
        return ''


def processAllQueries(rel_dict, queries):
    results_list = []
    for query in queries:
        results_list.append(processQuery(rel_dict, query))
    return results_list
        

# Step 1: Creating input file
test_file = open('relationships.txt', 'w')
test_file.write('''Elizabeth: Peter, Angela, Thomas
Mary: Tom
Angela: Fred, Alison
Alison: Beatrice, Dick, Harry

mother Elizabeth
mother Tom
mother Angela
mother Gurgle
ancestors Tom
ancestors Harry
ancestors Bernard
ancestors Mary
ancestors Beatrice
mother Dick
relationship Tom Mary
relationship Mary Tom
relationship Harry Angela
relationship Elizabeth Harry
relationship Harry Mary
''')
test_file.close()

# Step 2: Read lines.
test_file = open('relationships.txt', 'r')
file_lines_list = test_file.readlines()
test_file.close()


# Step 3: Split file up.
rel_lines_list, query_lines_list = splitFile(file_lines_list)
#print(rel_lines_list)
#print(query_lines_list)

# Step 4: Parse lines. Create relationship dictionary.
rel_dict = extractRelationshipData(rel_lines_list)
#print(rel_dict)


# Step 5: Querying the relationship data.
results_list = processAllQueries(rel_dict, query_lines_list)
for result in results_list:
    print(result)