'''Dumbo
Author: Hector McTavish'''
    for for for  # Should count as 1 for statement
while_im_alive # Shouldn't count as a while
while blah # But this one should
  if defined # Should be an if but not a def
  def if # Should be a def but not an if
    x = (2 * 3) + 4 * 2 * 7 / 1 - 2  # Various operators
