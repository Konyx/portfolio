import os
import sys

class StudentProgram (object):
    STATEMENTS = {'for_loops': 'for',
                  'while_loops': 'while',
                  'if_statements': 'if',
                  'function_definitions': 'def'}
    OPERATORS = {'multiplications': '*',
                 'divisions': '/',
                 'additions': '+',
                 'subtractions': '-'}
    METRICS_LIST = ['lines_of_code',
                    'for_loops',
                    'while_loops',
                    'if_statements',
                    'function_definitions',
                    'multiplications',
                    'divisions',
                    'additions',
                    'subtractions']
      
    def __init__(self, filepath):
        with open(filepath, 'r') as file:
            file_lines = file.readlines()

        file_lines = self._clean_lines(file_lines)
        self.filename = os.path.basename(filepath)
        self.author = self._determine_author(file_lines)
        self.metrics = self._extract_metrics(file_lines)
        
    def _clean_lines(self, file_lines):
        clean_file_lines = []
        for line in file_lines:
            clean_file_lines.append(line.strip())
        return clean_file_lines

    def _determine_author(self, file_lines):
        for line in file_lines:
            if line.startswith('Author: '):
                author = line.replace('Author: ', '')
                author = ''.join(char for char in author if char not in ('\'', '\"'))
                return author
        return 'Unknown'
    
    def _extract_metrics(self, file_lines):
        metrics = {}
        metrics['lines_of_code'] = len(file_lines)
        
        for metric_key, keyword in self.STATEMENTS.items():
            metrics[metric_key] = self._determine_statement_count(file_lines, keyword)

        for metric_key, operator_str in self.OPERATORS.items():
            metrics[metric_key] = self._determine_char_count(file_lines, operator_str)

        return metrics
    
    def _determine_statement_count(self, file_lines, keyword):
        count = 0
        keyword = keyword.strip() + ' '
        for line in file_lines:
            if line.startswith(keyword):
                count += 1
        return count
    
    def _determine_char_count(self, file_lines, operator):
        count = 0
        for line in file_lines:
            count += line.count(operator)
        return count

    def __str__(self):      
        print_str = ''
        print_str += 'Filename             %s\n' % self.filename
        print_str += 'Author               %s\n' % self.author
        for metric_key in self.METRICS_LIST:
            print_str += '{0:<21}{1}\n'.format(metric_key, self.metrics[metric_key])
        return print_str


def process_metric_queries(programs_list):
    print('%d programs found' % len(programs_list))
    while(True):
        metric_query = input('Metric of interest? ')         
        if metric_query.lower() == 'quit':
            sys.exit()
        elif metric_query in StudentProgram.METRICS_LIST:
            bin_dict = process_metrics_into_bins(metric_query, programs_list)
            print_str = generate_metric_queries_str(sorted(bin_dict.keys()), bin_dict)
            print(print_str)
        else:
            print('Unknown metric')

def process_metrics_into_bins(metric_query, programs_list):
    bin_dict = {}            
    for program in programs_list:
        bin_num = program.metrics[metric_query] // 10
        bin_start = 10 * bin_num
        bin_end = bin_start + 9
        bin_range = (bin_start, bin_end)
        
        if bin_range in bin_dict.keys():
            bin_dict[bin_range] += 1
        else:
            bin_dict[bin_range] = 1
    return bin_dict

def generate_metric_queries_str(sorted_keys, bin_dict):
    print_str = '\n'
    print_str += 'Bin range    Count\n'
    for key in sorted_keys:
        print_str += '{0:<12}{1:>4}\n'.format('%d-%d' % key, bin_dict[key])
    return print_str


def process_student_programs_in_dir(input_path):
    abs_filepath_list = []  
    for dirpath, dirnames, filenames in os.walk(input_path):
        for filename in filenames:
            if filename.endswith('.py'):
                file_path = os.path.join(dirpath, filename)      
                abs_filepath_list.append(os.path.abspath(file_path))

    programs_list = []
    for file_path in abs_filepath_list:
        programs_list.append(StudentProgram(file_path))
        
    return programs_list


def code_runner():
    input_path = input('Enter file or directory: ') 
    if os.path.exists(input_path): 
        if os.path.isdir(input_path):
            program_data_list = process_student_programs_in_dir(input_path)
            process_metric_queries(program_data_list)
        elif os.path.isfile(input_path) and input_path.endswith('.py'):
            file_path = os.path.abspath(input_path)
            program = StudentProgram(file_path)
            print(program)   
    else:
        sys.exit('File/directory not found')

if  __name__ =='__main__':
    code_runner()