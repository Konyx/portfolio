#!/usr/bin/env python
"""
A set of functions for extracting n-word phrases.
Kaan Arik
"""

import sys, collections, re, comparisons

COUNTER = comparisons.Counter()

WORD_PATTERN = r"((?:[a-zA-Z0-9]|(?<=[a-zA-Z0-9])(?:-|')(?=[a-zA-Z0-9]))+)"

def words_in_line(line):
    "Returns a list of words from a line by removing formatting."
    
    line = line.replace('_','')
    words = re.findall(WORD_PATTERN, line)
    return [x.lower() for x in words]


def extract_phrases(input_buffer, phrase_length):
    "Return a list of phrases of length phrase_length from an input buffer."
    
    word_deque = collections.deque()
    phrases = []
    unsorted_phrases = []

    for line in input_buffer:
        line_words_list = words_in_line(line)
        for word in line_words_list:
            word_deque.append(word)
            
    while len(word_deque) != 0:
        unsorted_phrases.append(word_deque.popleft())
        if len(unsorted_phrases) == phrase_length:
            joint_phrases = ' '.join(unsorted_phrases)
            phrases.append(joint_phrases)
            unsorted_phrases.pop(0)
            
        

    
    return phrases


def exclude_duplicates(items):
    """Return a sorted list of unique items, assuming items is originally
    sorted."""
    
    if len(items) < 2:
        COUNTER.increment()
        return items

    unique_items = []
    unique_items.append(items[0])
    
    for words in items[1:]:
        if words != unique_items[-1]:
            COUNTER.increment()
            unique_items.append(words)
                
        else:
            COUNTER.increment()

    
    return unique_items



def load_phrases(input_buffer, phrase_length):
    "Load a sorted list of phrases with no duplicates."
    
    phrases = extract_phrases(input_buffer, phrase_length)
    
    phrases.sort()
    
    return exclude_duplicates(phrases)


def main(args):
    "Usage: python phrase_parser.py phrase_length input.txt"
    
    if len(args) == 3:
        load_phrases(open(args[2]), int(args[1]))
        
    else:
        print(main.__doc__)

if __name__ == "__main__":
    main(sys.argv)
