#!/usr/bin/env python

import unittest, io, common_phrases_binary

# This unit test is worth a total of 15%.
# The unit test used in marking will be different but have approximately the
# same weightings.

class TestCommonPhrasesBinary(unittest.TestCase):
    def test_find_in_true(self):
        'Test that the find_in function is working as expected (3%)'
        
        common_phrases_binary.COUNTER.reset()
        
        phrases = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        
        self.assertTrue(common_phrases_binary.find_in(phrases, "M"))
    
    def test_find_in_true_counter(self):
        'Test the count of the find_in_true test (1%)'
        
        self.test_find_in_true()
        
        self.assertEqual(common_phrases_binary.COUNTER.count, 7)
    
    def test_find_in_false(self):
        'Test that the find_in function is working as expected (1%)'
        
        common_phrases_binary.COUNTER.reset()
        
        phrases = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        
        self.assertFalse(common_phrases_binary.find_in(phrases, "z"))
    
    def test_find_in_false_counter(self):
        'Test the count of the find_in_false test (1%)'
        
        self.test_find_in_false()
        
        self.assertEqual(common_phrases_binary.COUNTER.count, 8)
    
    def test_compare_phrases(self):
        'Test that the compare_phrases function is working as expected (3%)'
        
        common_phrases_binary.COUNTER.reset()
        
        phrases_a = list("ABCDEFGHIJKL")
        phrases_b = list("ADEGJKMNOP")
        
        common_phrases = common_phrases_binary.common_phrases(phrases_a, phrases_b)
        self.assertEqual(common_phrases, list("ADEGJK"))
    
    def test_compare_phrases_counter(self):
        'Test the count of the compare_phrases test (2%)'

        self.test_compare_phrases()
        
        self.assertEqual(common_phrases_binary.COUNTER.count, 83)

    def test_binary_search(self):
        'Test that binary search is working correctly (2%)'
        
        common_phrases_binary.COUNTER.reset()
        
        items = list(range(1, 10))
        items.remove(5)
        
        for index, item in enumerate(items):
            self.assertTrue(common_phrases_binary.find_in(items, item))
        
        self.assertFalse(common_phrases_binary.find_in(items, 5))
    
    def test_binary_search_counter(self):
        'Test the count of the binary_search test (2%)'
        
        self.test_binary_search()
        
        self.assertEqual(common_phrases_binary.COUNTER.count, 40)


if __name__ == '__main__':
    print("This unit test is worth a total of 15%.")
    unittest.main(exit=False)
