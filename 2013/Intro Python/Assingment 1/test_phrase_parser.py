#!/usr/bin/env python

import unittest, phrase_parser, io

# This unit test is worth a total of 10%.
# The unit test used in marking will be different but have approximately the
# same weightings.

class TestPhraseParser(unittest.TestCase):
    def test_extract_phrases(self):
        'Test that phrases are being extracted correctly. (2%)'
        
        phrase_parser.COUNTER.reset()
        
        buffer = io.StringIO("The quick\n\nbrown fox\njumped over\n\n\n\nthe lazy\ndog")
        phrases = phrase_parser.extract_phrases(buffer, 3)
        
        self.assertEqual(len(phrases), 7)
        self.assertEqual(phrases[0], "the quick brown")
        self.assertEqual(phrases[1], "quick brown fox")
        self.assertEqual(phrases[2], "brown fox jumped")
        self.assertEqual(phrases[3], "fox jumped over")
        self.assertEqual(phrases[4], "jumped over the")
        self.assertEqual(phrases[5], "over the lazy")
        self.assertEqual(phrases[6], "the lazy dog")
        
        # No comparisons required.
        self.assertEqual(phrase_parser.COUNTER.count, 0)

    def test_extract_phrases_max(self):
        'Test the case where n = number of words. (1%)'
        
        buffer = io.StringIO("The quick\n\nbrown fox\njumped over\n\n\n\nthe lazy\ndog")
        phrases = phrase_parser.extract_phrases(buffer, 9)
        
        self.assertEqual(len(phrases), 1)
        self.assertEqual(phrases[0], "the quick brown fox jumped over the lazy dog")
    
    def test_extract_phrases_min(self):
        'Test the case where n = 1. (1%)'
        
        buffer = io.StringIO("The quick\n\nbrown fox\njumped over\n\n\n\nthe lazy\ndog")
        phrases = phrase_parser.extract_phrases(buffer, 1)
        
        self.assertEqual(len(phrases), 9)
        self.assertEqual(phrases, ["the", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog"])
    
    def test_exclude_duplicates(self):
        'Check that duplicates are removed correctly. (2%)'
        
        phrase_parser.COUNTER.reset()
        
        items = ["apples", "banannas", "banannas", "carrots", "carrots", "carrots", "oranges", "pineapples"]
        
        items = phrase_parser.exclude_duplicates(items)
        
        self.assertEqual(items, ["apples", "banannas", "carrots", "oranges", "pineapples"])
    
    def test_exclude_duplicates_counter(self):
        'Test the count of the exclude_duplicates test (1%)'
        
        self.test_exclude_duplicates()
        
        # Some comparisons required for uniquness:
        self.assertEqual(phrase_parser.COUNTER.count, 7)
    
    def test_load_phrases(self):
        'Check that phrases are being sorted and uniqued. (2%)'
        
        phrase_parser.COUNTER.reset()
        
        buffer = io.StringIO("Apples and oranges and apples and oranges and apples")
        phrases = phrase_parser.load_phrases(buffer, 3)
        
        self.assertEqual(len(phrases), 4)
        self.assertEqual(phrases[0], "and apples and")
        self.assertEqual(phrases[1], "and oranges and")
        self.assertEqual(phrases[2], "apples and oranges")
        self.assertEqual(phrases[3], "oranges and apples")
    
    def test_load_phrases_counter(self):
        'Test the count of the load_phrases test (1%)'
        
        self.test_load_phrases()
        
        # Some comparisons required for uniquness:
        self.assertEqual(phrase_parser.COUNTER.count, 6)


if __name__ == '__main__':
    print("This unit test is worth a total of 10%.")
    unittest.main(exit=False)
