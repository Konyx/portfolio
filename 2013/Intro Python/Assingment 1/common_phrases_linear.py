#!/usr/bin/env python
"""
Find common phrases using a linear search.
Kaan Arik
"""

import sys, phrase_parser, comparisons

COUNTER = comparisons.Counter()

def find_in(phrases, key_phrase):
    """Returns true if key_phrase can be found in phrases, which is a sorted
    list."""

    found = False
    for word in phrases:
        COUNTER.increment()
        if word == key_phrase:
            found = True
            break
        COUNTER.increment()
        if word > key_phrase:
            break

    return found
    


def common_phrases(phrases_a, phrases_b):
    """Returns all phrases in phrases_a that exist also in phrases_b, which
    are non-empty sorted lists."""
    
    common = []
    COUNTER.increment()
    if phrases_a[-1] < phrases_b[0]:
        return common
    else:
        for phrase in phrases_a:
            if find_in(phrases_b, phrase):
                
                common.append(phrase)            

            elif phrase > phrases_b[-1]:
                COUNTER.increment()
                break

            else:
                COUNTER.increment()
       
    return common


def main(args):
    "Usage: python common_phrases_linear.py phrase_length file_a.txt " \
    "file_b.txt"
    
    if len(args) == 4:
        phrase_length = int(args[1])
        
        phrases_a = phrase_parser.load_phrases(open(args[2]), phrase_length)
        phrases_b = phrase_parser.load_phrases(open(args[3]), phrase_length)
        
        for phrase in common_phrases(phrases_a, phrases_b):
            print(phrase)
    else:
        print(main.__doc__)


if __name__ == "__main__":
    main(sys.argv)
