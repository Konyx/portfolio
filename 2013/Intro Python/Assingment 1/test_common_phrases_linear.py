#!/usr/bin/env python

import unittest, io, common_phrases_linear

# This unit test is worth a total of 15%.
# The unit test used in marking will be different but have approximately the
# same weightings.

class TestCommonPhrasesLinear(unittest.TestCase):
    def test_find_in_true(self):
        'Test that the find_in function is working as expected (3%)'
        
        common_phrases_linear.COUNTER.reset()
        
        phrases = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        
        self.assertTrue(common_phrases_linear.find_in(phrases, "M"))
    
    def test_find_in_true_counter(self):
        'Test the count of the find_in_true test (2%)'
        
        self.test_find_in_true()
        
        self.assertEqual(common_phrases_linear.COUNTER.count, 25)
    
    def test_find_in_false(self):
        'Test that the find_in function is working as expected (1%)'
        
        common_phrases_linear.COUNTER.reset()
        
        phrases = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        
        self.assertFalse(common_phrases_linear.find_in(phrases, "z"))
        
    def test_find_in_false_counter(self):
        'Test the count of the find_in_false test (1%)'
        
        self.test_find_in_false()
        
        self.assertEqual(common_phrases_linear.COUNTER.count, 52)
    
    def test_find_in_disjunct(self):
        "Test that find_in doesn't do unnecessary comparisons (1%)"
        
        common_phrases_linear.COUNTER.reset()
        
        phrases = list("BCDEFGHIJKLMNOPQRSTUVWXYZ")
        
        self.assertFalse(common_phrases_linear.find_in(phrases, "A"))
        
        self.assertEqual(common_phrases_linear.COUNTER.count, 2)
    
    def test_compare_phrases(self):
        'Test that the compare_phrases function is working as expected (3%)'
        
        common_phrases_linear.COUNTER.reset()
        
        phrases_a = list("ABCDEFGHIJKL")
        phrases_b = list("ADEGJKMNOP")
        
        common_phrases = common_phrases_linear.common_phrases(phrases_a, phrases_b)
        self.assertEqual(common_phrases, list("ADEGJK"))
    
    def test_compare_phrases_counter(self):
        'Test the count of the compare_phrases test (2%)'
        
        self.test_compare_phrases()
        
        self.assertEqual(common_phrases_linear.COUNTER.count, 93)
    
    def test_common_phrases_empty(self):
        'Test when the phrases do not overlap (1%)'
        
        common_phrases_linear.COUNTER.reset()
        
        phrases_a = list("ABCDEFGHIJK")
        phrases_b = list("MNOPQRSTUVWXYZ")
        
        common_phrases = common_phrases_linear.common_phrases(phrases_a, phrases_b)
        
        self.assertEqual(common_phrases, [])
        self.assertEqual(common_phrases_linear.COUNTER.count, 1)

    def test_common_phrases_intersect(self):
        'Test when the phrases_b is contained completely within (1%)'
        
        common_phrases_linear.COUNTER.reset()
        
        phrases_a = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        phrases_b = list("CDEFGH")
        
        common_phrases = common_phrases_linear.common_phrases(phrases_a, phrases_b)
        
        self.assertEqual(common_phrases, phrases_b)
        self.assertEqual(common_phrases_linear.COUNTER.count, 56)


if __name__ == '__main__':
    print("This unit test is worth a total of 15%.")
    unittest.main(exit=False)
