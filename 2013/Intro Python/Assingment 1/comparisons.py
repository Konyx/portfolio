"A module to assist with tracking various algorithmic operations."

class Counter(object):
    "A counter can be used to keep track of the number of operations."
    
    def __init__(self):
        self.count = 0
    
    def increment(self, count = 1):
        "Increment the count."
        
        self.count += count
    
    def reset(self):
        "Reset the count to zero."
        
        self.count = 0
    
    def __repr__(self):
        return repr(self.count)
