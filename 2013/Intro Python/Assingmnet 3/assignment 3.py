shape = (input("Enter shape to draw: "))
shape = shape.lower()
while shape != "triangle" and shape != "square":
    print ("Invalid input. Try again.")
    shape = (input("Enter shape to draw: "))
    shape = shape.lower()    
fill = (input("Filled or unfilled? "))
fill = fill.lower()
while fill != "filled" and fill != "unfilled":
    print ("Invalid input. Try again.")
    fill = (input("Filled or unfilled? "))
    fill = fill.lower()
size = (input("Size: "))
size = int(size)
while size < 1 or size > 100:
    print ("Invalid input. Try again.")
    size = (input("Size: "))
    size = int(size)
print()
if shape == "square" and fill == "filled":
    for i in range (size):
        print ("*" * size)
if shape == "square" and fill == "unfilled":
    if size > 2:
        print ("*"*size)
        for i in range (size-2):
            print (("*" + " " * (size-2) + "*"))
        print ("*"*size)
    else:
        for i in range (size):
                print ("*" * size)        
if shape == "triangle" and fill == "filled":
    for i in range (1, size+1):
        print ("*"*(i))
if shape == "triangle" and fill == "unfilled": 
    if size > 2:
        for i in range (1, 3):
            print ("*"*(i))    
        for j in range (1):
            for k in range(3, size):
                print ("*" + " "*(k-2)+"*")
        print ("*" * size)    
    else:
        for i in range (1, size+1):
            print ("*"*(i))       