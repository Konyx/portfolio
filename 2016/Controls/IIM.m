%------------------------------Question 2----------------------------------
%Author - Kaan Arik
%Forward simulates the provided equation using eulars forward simulation
%method. 

%Performs the iterative intergral method on the given equation. 

%Compares the plots with the orignal data. 

%--------------------------------------------------------------------------

clc
beep off
clear all
close all
format short g

set(0,'defaultfigureposition',[60 60 420,330])
T=[25.34,24.28,24.26,25.24,24.82,30.01,28.36,27.00,25.89,25.18,25.60, 25.46,24.78,37.61,32.00,28.41,27.68,27.02,25.03,26.53,25.22,33.69,30.66,28.08,27.51,26.90,25.97,25.45,24.42,24.79,25.11,24.06,24.51,25.10,24.40, 24.62,25.16,25.10,25.06,26.22,25.34];
t=0:0.25:10;   

%Parent parameter values. 
k =   2;
j1=  200000;
j2 = 400000;
j3 = 300000;

cp = 4181;
V = 5; %Volumn of water.
Tamb = 25; %Ambient Tempeture.

dt=t(2)-t(1); %sampling data at 4Hz. 


%analytical solution to the model
parent_model = zeros(1, 41);
parent_impulse = zeros(1, 41); 
parent_impulse(5)=(j1)/(V*cp);
parent_impulse(13)=(j2)/(V*cp);
parent_impulse(21)=(j3)/(V*cp);
parent_model(1) = Tamb;
for r=2:41
   parent_model(r) = parent_model(r-1) + (((-k*(parent_model(r-1) - Tamb)))*dt) + (parent_impulse(1, r-1));
end


b= T'-Tamb; %Sampled data minus the intial condition. 
iim_impulses=zeros(size(t)); %Hot steel is dropped into the water at
%time = 1 second, 3 seconds and 5 seconds. 
iim_simulation = zeros(1, 41);
Tsim=ones(size(t));

for III=1:100
    
    %Simulated data intergrated. 
    iTminusTamb = cumtrapz(t, Tsim); 
    
    %integral of an impulse is just a unit step multiplied by the 
    %impulse value. 
    iV1 = [zeros(1, 5) (ones(1, 36) .* (1/(V*cp)))];
    iV2 = [zeros(1, 13) (ones(1, 28) .* (1/(V*cp)))];
    iV3 = [zeros(1, 21) (ones(1, 20) .* (1/(V*cp)))];


    A=[iTminusTamb' iV1' iV2' iV3']; % fill up a matrix with the coefficient integrals at the sample points
    ss=A\b;             % forward simulate with new model values  
    
    iim_impulses(5) = (ss(2))/(V*cp);
    iim_impulses(13) = (ss(3))/(V*cp);
    iim_impulses(21) = (ss(4))/(V*cp);
    iim_simulation(1) = 25;
    for r=2:41
       iim_simulation(r) = iim_simulation(r-1) + (((-ss(1)*(iim_simulation(r-1) - Tamb)))*dt) + iim_impulses(r-1);
    end
    
    %Remeber it's -(T-Tambient) infront of the k value we want to find. 
    Tsim = -(iim_simulation - Tamb);

end

%Simulate model using the k,j1,j2 and j3 values found using IMM. 
iim_simulation(1) = 25;
for r=2:41
   iim_simulation(r) = iim_simulation(r-1) + (((-ss(1)*(iim_simulation(r-1) - Tamb)))*dt) + iim_impulses(r-1);
end

%plot parents and found model. 
plot(t, parent_model); hold all
plot(t, iim_simulation);

