% Authors: Kaan Arik

%Prepare the workspace:
clear;
clc;
clf; %Enable when using figures. Disable to prevent empty figure
%appearing

%Constants:
MpArr = [0.215, 0.425, 0.530, 0.642]; %pendulum masses (kg)
Mc = 1.608; %cart mass [kg]
LpArr = [0.314, 0.477, 0.516, 0.546]; % pendulum half-lengths [m]
IoArr = (10^-3) * [7.06, 18.74, 21.89, 24.62]; % pendulum inertia about C of G [kg*m^2]
R = 0.16; % motor terminal resistance [Ohms]
r = 0.0184; % radius of pinion [m]
kg = 3.71; % gearing ratio
km = 1.68 * (10^-2); % back EMF constant [V / rad / sec]
g = 9.81; %Gravity.

%%Guessing:
damping = 3.68; %Damping Coeff. Taken from Cart lab where it was either 0 or 3.68

%Configure Scenario:
index = 3;
Mp = MpArr(index);
Lp = LpArr(index);
Io = IoArr(index);


%Find Matrices for the State Space, A, B, C, D
%Find A, Plant Matrix:
A = zeros(4,4);
denom1 = (Mc + Mp)*Io + Mc*Mp*(Lp^2);
denom2 = denom1*R*r^2;
denomB = denom2/r; %The denom for B mtx
num1 = (damping*R*r^2 - (km^2)*(kg^2)); %Common numerator in A mtx

A(1,3) = 1;
A(2,4) = 1;
A(3,2) = (-(Mp^2)*(Lp^2)*g)/denom1;
A(4,2) = ((Mc + Mp)*Mp*Lp*g)/denom1;
A(3,3) = (Io + Mp*(Lp^2))*num1/denom2;
A(4,3) = -Mp*Lp*num1/denom2;

%Find B (Control Input):
B = zeros(4,1);
B(3,1) = (Io + Mp*(Lp^2))*km*kg/denomB;
B(4,1) = -Mp*Lp*km*kg/denomB;

%Find C (Sensor's)
C = [1 0 0 0; 0 1 0 0]; %As specified in asgn, for plotting purposes

%Find D
D = 0;

%Check Controlability
ctrl = [B, A*B, A*A*B, A*A*A*B]
if(rref(ctrl) == eye(4))
    disp('System is Controllable');
    %disp('Eigenvalues of A:'); disp(eig(A));
    
else
    disp('System is Uncontrollable!');
    rref(ctrl)
    break;
end

disp('Eigenvalues of A:');
disp(eig(A));

%Create Open-Loop State-Space Model:
sysOL = ss(A,B,C,D); %Open Loop

% Create Open Loop State-Space Model:
sysOL = ss(A, B, C, 0);


%Using the LQR method:
R = 1;

% WTF is happening here...???
% Q = eye(4)*R;
% Q(1,1) = Q(1,1) / R * 40;

%Adjusted to give what similar to what worked for pole placement
Q = eye(4);
Q(1,1) = Q(1,1) * 400;
Q(2,2) = Q(2,2) * 10;
Q(3,3) = Q(3,3) * 15;
Q(4,4) = Q(4,4) * 1;


gains = lqr(A,B,Q,R); %Calculate Gains.
disp('Gains LQR:');
disp(gains);
Acl = A - B*gains; %Closed loop plant matrix

stepsize = 0.05; %Step input magnituide
C = [1 0 0 0]; %As specified in asgn
N = -inv(C * inv(Acl) * B);
Bhat = B * N;

h = figure(1)
% subplot(2,1,1);
t = 0:0.001:40;
U = stepsize*(square(pi*t/10)); %0.1 magnituide square wave input
[Y,X] = lsim(Acl,Bhat,C,D,U,t);
plot(t,Y,t,U)
sysCL = ss(Acl, Bhat, C, 0);
grid on
ylabel('Cart''s Displacement (m)','FontSize', 12)
xlabel('Time (s)','FontSize', 12)
legend('Response','Pulse')
% subplot(2,1,2);
% pzplot(sysCL);
eig(sysCL)
print(h, '-dbmp', 'Pendulum_LQR.bmp')


% %Using place technique
%poles = [-45, -40, (-2+2i), (-2-2i)];
poles = [-30, -20, (-2+2i), (-2-2i)]; %This worked well
gains = place(A,B,poles);
disp('Gains Pole Place:');
disp(gains);

Acl1 = A-B*gains;
N1 = -inv(C * inv(Acl1) * B);
Bhat1 = B * N1;

sysCL1 = ss(Acl1, Bhat1, C, 0);

h = figure(2)
% subplot(2,1,1);
[Y,X] = lsim(Acl1,Bhat1,C,D,U,t);
plot(t,Y,t,U);
grid on
ylabel('Cart''s Displacement (m)','FontSize', 12)
xlabel('Time (s)','FontSize', 12)
legend('Response','Pulse')
% subplot(2,1,2);
% pzplot(sysCL1);
eig(sysCL1)
print(h, '-dbmp', 'Pendulum_PolePlacement.bmp')


