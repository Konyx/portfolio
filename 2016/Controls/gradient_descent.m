%------------------------------Question 4----------------------------------
%Author - Kaan Arik
%Forward simulates the provided equation using eulars forward simulation
%method. 

%Performs Gauss-Newton algorithm to find [k, J1, J2, J3] then performs
%eulars algorithm to forward simulate the model. 

%Compares the plots with the orignal data. 

%--------------------------------------------------------------------------

clc
beep off
clear all
close all
format short g

set(0,'defaultfigureposition',[60 60 420,330])
T=[25.34,24.28,24.26,25.24,24.82,30.01,28.36,27.00,25.89,25.18,25.60, 25.46,24.78,37.61,32.00,28.41,27.68,27.02,25.03,26.53,25.22,33.69,30.66,28.08,27.51,26.90,25.97,25.45,24.42,24.79,25.11,24.06,24.51,25.10,24.40, 24.62,25.16,25.10,25.06,26.22,25.34];
t=0:0.25:10;   

%Parent parameter values. 
k =   2;
j1=  200000;
j2 = 400000;
j3 = 300000;

cp = 4181;
V = 5; %Volumn of water.
Tamb = 25; %Ambient Tempeture.

dt=t(2)-t(1); %sampling data at 4Hz. 


%analytical solution to the model
parent_model = zeros(1, 41);
parent_impulse = zeros(1, 41); 
parent_impulse(5)=(j1)/(V*cp);
parent_impulse(13)=(j2)/(V*cp);
parent_impulse(21)=(j3)/(V*cp);
parent_model(1) = Tamb;
for r=2:41
   parent_model(r) = parent_model(r-1) + (((-k*(parent_model(r-1) - Tamb)))*dt) + (parent_impulse(1, r-1));
end


alpha=0.5;
KUN=[0.5;15000;30000; 20000]; %INITIAL CONDITION
dKUN=[k/1000; j1/1000; j2/1000; j3/1000]; %perturbations (STEPS)
simulated_impulse = zeros(1,41);
j1_simulation = zeros(1, 41);
j2_simulation = zeros(1, 41);
j3_simulation = zeros(1, 41);
k_simulation = zeros(1, 41);


for III=1:10
    simulated_impulse(5) = KUN(2, III)/(cp * V);
    simulated_impulse(13) = KUN(3, III)/(cp * V);
    simulated_impulse(21) = KUN(4, III)/(cp * V);
    gaus_newton_simulation(1) = 25;
    
    %Forward simulate intial guess, as well as [k, j1, j2, j3] values
    %found using gauss-newton. 

    for r=2:41
        gaus_newton_simulation(r) = gaus_newton_simulation(r-1) + (((-KUN(1, III)*(gaus_newton_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
    end

    
    %From here to finding JJ is just evaluating objective function at known
    %points around a particula point in parameter space. I.e. add the
    %permutation value to the related parameter and forward simulate. Take
    %the residual between the points found with respect to the data from
    %the simulated model. Do these for all parameters. The remaining steps
    %were taken directly from the notes!
    
    psi2(III,1:41)=gaus_newton_simulation-T;  % The model simulation (CCt) at the sample times (TT+1) minus the raw data
    norm(psi2);
    
    k_simulation(1) = 25;
    for r=2:41
         k_simulation(r) = k_simulation(r-1) + (((-(KUN(1, III) + dKUN(1))*(k_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
    end 
    nk_simulation= k_simulation - T;
    
    simulated_impulse(5) = simulated_impulse(5) + ((dKUN(2)/(cp * V)));
    j1_simulation(1) = 25;
    for r=2:41
         j1_simulation(r) = j1_simulation(r-1) + (((-KUN(1, III)*(j1_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
    end
    nj1_simulation= j1_simulation - T;
    simulated_impulse(5) = simulated_impulse(5) - ((dKUN(2)/(cp * V)));
    
    simulated_impulse(13) = simulated_impulse(13) + ((dKUN(3)/(cp * V)));
    j2_simulation(1) = 25;
    for r=2:41
         j2_simulation(r) = j2_simulation(r-1) + (((-KUN(1, III)*(j2_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
    end
    nj2_simulation= j2_simulation - T;
     simulated_impulse(13) = simulated_impulse(13) - ((dKUN(3)/(cp * V)));
    
    simulated_impulse(21) = simulated_impulse(21) + ((dKUN(4)/(cp * V)));
    j3_simulation(1) = 25;
    for r=2:41
         j3_simulation(r) = j3_simulation(r-1) + (((-KUN(1, III)*(j3_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
    end
    nj3_simulation= j3_simulation - T;
    simulated_impulse(21) = simulated_impulse(21) - ((dKUN(4)/(cp * V)));
    
    %From notes
    JJ=[(nk_simulation'-psi2(III,1:41)')/dKUN(1) (nj1_simulation'-psi2(III,1:41)')/dKUN(2) (nj2_simulation'-psi2(III,1:41)')/dKUN(3) (nj3_simulation'-psi2(III,1:41)')/dKUN(4)];
    
    
    % this time with added Gauss Newton goodness:
    KUN(:,III+1)=KUN(:,III)-alpha*(JJ'*JJ)^-1*JJ'*psi2(III,1:41)';
    
   
   
end


%Forward simulates the provided equation using eulars forward simulation
%method using [k, j1. j2. j3] found using gaus-newton method.  

gaus_newton_simulation(1) = 25;
for r=2:41
    gaus_newton_simulation(r) = gaus_newton_simulation(r-1) + (((-KUN(1, III)*(gaus_newton_simulation(r-1) - Tamb)))*dt) + (simulated_impulse(1, r-1));
end

plot(t, parent_model); hold all
plot(t, gaus_newton_simulation);
