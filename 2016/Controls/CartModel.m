% A script file to simulate the spring-mass-damper system
% Authors: Kaan Arik

%Prepare the workspace:
clear;
clc;
clf; %Enable when using figures. Disable to prevent empty figure
%appearing

%Constants:
alpha = 12.45; %"Experimentally derived fiddle factor"
km = 0.00176; %Motor back emf constant
kg = 3.71; %Gear ratio
R = 1.4; %Resistance of motor armature
r = 0.0184; %Radius of the pinion

velDampingTerm = (km^2*kg^2/(R*r^2))

%Physical Attributes:
mass = [1.608, 0.75, 0.75]; %Mass in kilograms of the carts, mass[1] = cart 1
extraMass = [0, .5, .5] %Extra mass that can be added to carts, extraMass[1] = cart 1
springConstants = [ 175, 400, 800]; %Spring constants in N/m for each type of spring.
damping = [velDampingTerm, 3.68, 3.68]; %Damping coeff for each cart damper. Ns/m.

spring = springConstants(1); %Choose the spring to use

%Create intermediate matrices
%Setup Mass:
M = zeros(3,3); %The mass array
M(1,1) = mass(1)%+extraMass(1);
M(2,2) = mass(2)%+extraMass(2);
M(3,3) = mass(3)%+extraMass(3);

%Setup Damping:
c = zeros(3,3);
c(1,1) = +damping(1);
c(2,2) = +damping(2);
c(3,3) = +damping(3);

%Setup Spring:
K = zeros(3,3);
K(1,1) = +spring;
K(1,2) = -spring;
K(1,3) = 0;
K(2,1) = -spring;
K(2,2) = 2*spring;
K(2,3) = -spring;
K(3,1) = 0;
K(3,2) = -spring;
K(3,3) = +spring;



%Find Matrices for the State Space, A, B, C, D
%Find A, Plant Matrix:
temp1 = -(inv(M)*K); %Sub-matrix 1
temp2 = -(inv(M)*c); %Sub-matrix 2
A = [zeros(3,3), eye(3); temp1, temp2];

%Find B (Control Input):
voltageDampingTerm = alpha*km*kg/(R*r)
b = [1;0;0]*voltageDampingTerm;
B = [zeros(3,1); inv(M)*b ]

%Find C (Sensor's)
C = eye(6);
C=[0,0,1,0,0,0;0,0,0,0,0,1]; %Don't need velocity.
C = [0 0 1 0 0 0];
%Find D
D = 0

%Check Controlability
ctrl = [B, A*B, A*A*B, A*A*A*B, A*A*A*A*B, A*A*A*A*A*B]
if(rref(ctrl) == eye(6))
    disp('System is Controllable');
    %disp('Eigenvalues of A:'); disp(eig(A));
    
else
    disp('System is Uncontrollable!');
    M,c,K,A
    rref(ctrl)
end

%Create Open-Loop State-Space Model:


%Using the LQR method:
Q1 = eye(6);
Q1(1,1) = Q1(1,1) * 0.00000001;
Q1(2,2) = Q1(2,2) * 0.00000001;
Q1(4,4) = Q1(4,4) * 0.00000001;
Q1(5,5) = Q1(5,5) * 0.00000001;
Q1(6,6) = Q1(6,6) * 25;
Q1(3,3) = Q1(3,3) * 450;
R = 1;
gains1 = lqr(A,B,Q1,R) %Calculate Gains.



Acl1 = A - B*gains1;

r = 1;
N1 = -inv(C * inv(Acl1) * B)
Bhat1 = B * N1 * r;


sysOL = ss(A,B,C,0); %Open Loop

figure(1)
step(sysOL)

sysCL1 = ss(Acl1, Bhat1, C, 0); %Build System
eig(sysCL1)
h = figure(1)
step(sysCL1)
grid on
ylabel('Cart 3''s Displacement (m)','FontSize', 12)
xlabel('Time (s)','FontSize', 12)
print(h, '-dbmp', 'Cart_StepResponse.bmp')

h = figure(3)
t = 0:0.001:10;
U = r/2*(square(pi*t/1.5));
[Y,X] = lsim(Acl1,Bhat1/r,C,D,U,t);
plot(t,Y,t,U)
grid on
title('Cart 3''s Position for a Pulse Train of Period T=3s')
ylabel('Cart 3''s Displacement (m)','FontSize', 12)
xlabel('Time (s)','FontSize', 12)
legend('Response','Pulse Train')
%ylim([0.24, 0.26]);
print(h, '-dbmp', 'Cart_PulseTrain.bmp')

h = figure(4)
pzplot(sysCL1)
print(h, '-dbmp', 'Cart_PZPlot.bmp')
