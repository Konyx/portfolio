%------------------------------Question 1----------------------------------
%Author - Kaan Arik
%Forward simulates the provided equation using eulars forward simulation
%method. 

%Performs a linear least-square using 3 - downwards ramp functions and
%constants to find K, J1, J2 and J3.

%Performs another linear least-square operation using 3 - e(-2t) and a
%contant to find K, J1, J2 and J3. 

%Compares the plots with the orignal data. 

%--------------------------------------------------------------------------
clc; clear all; close all; clf
t = 0:.25:10; %TIMESTEP(sampling data at 4Hz (1/4 = 0.25s)).
V = 5; %Volumn of water. 
Cp = 4181;
Tamb = 25; %Ambient Temperture
T=[ 25.34,24.28,24.26,25.24,24.82,30.01,28.36,27.00,25.89,25.18,25.60,25.46,24.78,37.61,32.00,28.41,27.68,27.02,25.03,26.53,25.22,33.69,30.66,28.08,27.51,26.90,25.97,25.45,24.42,24.79,25.11,24.06,24.51,25.10,24.40,24.62,25.16,25.10,25.06,26.22,25.34];
k = 2; %Parent model value for k. 
J1 = 200000; %Parent model value for J1.
J2 = 400000; %Parent model value for J2.
J3 = 300000; %Parent model value for J3.
T_dot = (- k * (T - Tamb)) + (J1 / (V*Cp)) + (J2 / (V*Cp)) + (J3 / (V*Cp));
b = T'; %b matrix - sampled data!
dt = 0.25; %4 HZ. 


%Didn't work - but this is how 
%Ta = exp(-k*t).*(25 + cumtrapz(t,(exp(k*t).*(Tamb*k + UX))));


%analytical solution to the model
parent_model = zeros(1, 40); 
parent_impulse = zeros(1, 40); %Steel is dropped in a T=1, 3 and 5. 
parent_impulse(5)=(J1)/(V*Cp);
parent_impulse(13)=(J2)/(V*Cp);
parent_impulse(21)=(J3)/(V*Cp);
parent_model(1) = 25; %Water Temperture is 25 degree intially. 
for r=1:40
   parent_model(r+1) = parent_model(r) + (-k*(parent_model(r) - Tamb))*dt + parent_impulse(1, r); 
end


% create the a matrix parameters
for i = 0:40
    %Function 1 - Triangle at time = 1 second. 
    if i < 4 | i >= 13;
        f1(i + 1) = 0;        
    end
    if i == 5;
        f1(i + 1) = 1;
    end
    if i > 5 && i <= 13;
        f1(i + 1) = f1(i) - 0.125;
    end
    
    %Function 2 - Triangle at time = 3 second. 
    if i < 12 | i >= 21;
        f2(i + 1) = 0;        
    end
    if i == 13;
        f2(i + 1) = 1;
    end
    if i > 13 && i <= 21;
        f2(i + 1) = f2(i) - 0.125;
    end
    
    %Function 3 - Triangle at time = 5 second. 
    if i < 20 | i >= 29;
        f3(i + 1) = 0;        
    end
    if i == 21;
        f3(i + 1) = 1;
    end
    if i > 21 && i <= 29;
        f3(i + 1) = f3(i) - 0.125;
    end
end

%A matrix consists of three triangle functions + a constant. 
A = [f1.', f2.', f3.', ones(41,1)];
%Give you a*triangle1(t) + b*triangle2(t) + ctriangle3(t) + d(t) which 
%is the parameterised model. 
theta = A\b;
% Multiply the matricies together to get the simulated model found
% using least sqaure fit. 
BA = A * theta;

% create the a matrix parameters
for i = 0:40
    %Function 1 - exp(-2t) at time = 1 second. 
    if i < 4
        f1(i + 1) = 0;        
    end
    if i > 4
        f1(i + 1) = exp(-2 * ((i - 5) * 0.25));
    end
    
    %Function 2 - exp(-2t) at time = 3 second. 
    if i < 12
        f2(i + 1) = 0;        
    end
    if i > 12
        f2(i + 1) = exp(-2 * ((i - 13) * 0.25));
    end
    
    %Function 3 - exp(-2t) at time = 5 second. 
    if i < 20
        f3(i + 1) = 0;        
    end
    if i > 20
        f3(i + 1) = exp(-2 * ((i - 21) * 0.25));
    end
end

%A matrix consists of three negative exponential functions + a constant. 
A2 = [f1.', f2.', f3.', ones(41,1)];
%Give you a*exp(-2t) + b*exp(-2t) + cexp(-2t) + d(t) which 
%is the parameterised model. 
theta2 = A2\b;
% Multiply the matricies together to get the simulated model found
% using least sqaure fit. 
BA2 = A2 * theta2;


%Plot forward simulated parent model and two parameterised models. 
figure(1)
plot(t, BA, 'r'); hold all
plot(t, BA2, 'b'); hold all
plot(t,parent_model, 'g')