%------------------------------Question 5----------------------------------
%Author - Kaan Arik
%Forward simulates the provided equation using eulars forward simulation
%method. 

%Performs a generic algorithm determine a model to represent the data. The
%method works using the following steps: -

%1: Defines 100 random point combination for k, j1, j2 and j3.
%2: Forward simulates using these data points and finds the residuals with
%respect to the data found (T).
%3: Determines the 20 best residual values. 
%4: Takes the 20 best, add some randomly distributed noise four times
%to the points.
%Re-simulate this 10 times. 
%Take the winner and complete. 

%Compares the plots with the orignal data. 

%--------------------------------------------------------------------------

clc
beep off
clear all
close all
format short g

set(0,'defaultfigureposition',[60 60 420,330])
T=[25.34,24.28,24.26,25.24,24.82,30.01,28.36,27.00,25.89,25.18,25.60, 25.46,24.78,37.61,32.00,28.41,27.68,27.02,25.03,26.53,25.22,33.69,30.66,28.08,27.51,26.90,25.97,25.45,24.42,24.79,25.11,24.06,24.51,25.10,24.40, 24.62,25.16,25.10,25.06,26.22,25.34];
t=0:0.25:10;   

%Parent parameter values. 
k =   2;
j1=  200000;
j2 = 400000;
j3 = 300000;

cp = 4181;
V = 5; %Volumn of water.
Tamb = 25; %Ambient temp_random_dataeture.

dt=t(2)-t(1); %sampling data at 4Hz. 


%analytical solution to the model
parent_model = zeros(1, 41);
parent_impulse = zeros(1, 41); 
parent_impulse(5)=(j1)/(V*cp);
parent_impulse(13)=(j2)/(V*cp);
parent_impulse(21)=(j3)/(V*cp);
parent_model(1) = Tamb;
%same old. 
for r=2:41
   parent_model(r) = parent_model(r-1) + (((-k*(parent_model(r-1) - Tamb)))*dt) + (parent_impulse(1, r-1));
end



%To simulate all 100 random data values, and winners from these random data
%values.
generic_algorithm_sim = zeros(1, 41);
%impules for j.
j_impulse = zeros(1, 41);
%Always intial condition of 25.
generic_algorithm_sim(1) = 25;
%Random data for r, j1, j2 and j3. 
random_data = zeros(101, 5);
random_data(1:end, 2) = 2*rand(101, 1);
random_data(1:end, 3:5) = 100000*rand(101, 3);
%temp variable for the random data. 
temp_random_data = zeros(101, 5);
for III=1:10 %Do this 10 times. 
    for a=1:101 %Process each 100 data samples. 
        
        %Impulses for forward simulation as every example. 
        j_impulse(1, 5) = random_data(a, 3)/(V*cp);
        j_impulse(1, 13) = random_data(a, 4)/(V*cp);
        j_impulse(1, 21) = random_data(a, 5)/(V*cp);
        %forward simulate 1 of the 100 data points. 
        for r=2:41
            generic_algorithm_sim(r) = generic_algorithm_sim(r-1) + (((-random_data(a, 2)*(generic_algorithm_sim(r-1) - Tamb)))*dt) + (j_impulse(1, r-1));
        end
        random_data(a, 1)=norm(generic_algorithm_sim-T);
          
    end
    %Orders the residuals in order. Y in the residual value, Index is the
    %index of that particular value.
    [Y, Index] = sortrows(random_data(1:end, 1));  
    
    %PROBABLY NOT NEEDED.
    %Add top 20 residual values at the start position before their
    %respective additon of noise. I.e. position 1, 6, 11 ...
%     for i=1:20
%         temp_random_data((i-1)*5 + 1, 1:5) = random_data(Index(i), 1:end);
%     end
    %Add random noise four times to each of the top 20 winners.
    temp_random_data(1, 1:5) = random_data(Index(1), 1:5);
    for j=1:20
        temp_random_data(((j-1)*5) + 2:(j*5)+1, 1) = random_data(Index(j), 1);
        temp_random_data(((j-1)*5) + 2:(j*5)+1, 2) = ((ones(5, 1).*random_data(Index(j), 2)) + (0.1*randn(5, 1)));
        temp_random_data(((j-1)*5) + 2:(j*5)+1, 3) = ((ones(5, 1).*random_data(Index(j), 3)) + (10000*randn(5, 1)));
        temp_random_data(((j-1)*5) + 2:(j*5)+1, 4) = ((ones(5, 1).*random_data(Index(j), 4)) + (10000*randn(5, 1)));
        temp_random_data(((j-1)*5) + 2:(j*5)+1, 5) = ((ones(5, 1).*random_data(Index(j), 5)) + (10000*randn(5, 1)));
    end
    

    %Make the new random data the winners with and without added noise. 
    random_data = temp_random_data;
   
end

%Take winner and forard simulate to victory
j_impulse(1, 5) = random_data(Index(1), 3)/(V*cp);
j_impulse(1, 13) = random_data(Index(1), 4)/(V*cp);
j_impulse(1, 21) = random_data(Index(1), 5)/(V*cp);    
for r=2:41
    generic_algorithm_sim(r) = generic_algorithm_sim(r-1) + (((-random_data(Index(1), 2)*(generic_algorithm_sim(r-1) - Tamb)))*dt) + (j_impulse(1, r-1));
end 

%Plot that shit. 
plot(t,parent_model); hold all 
plot(t, generic_algorithm_sim); 
