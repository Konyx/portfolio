%------------------------------Question 3----------------------------------
%Author - Kaan Arik
%Forward simulates the provided equation using eulars forward simulation
%method. 

%Performs the autogressive correlation method to find a model for the
%system.

%Compares the plots with the orignal data. 

%--------------------------------------------------------------------------

clc
beep off
clear all
close all
format short g

set(0,'defaultfigureposition',[60 60 420,330])
T=[25.34,24.28,24.26,25.24,24.82,30.01,28.36,27.00,25.89,25.18,25.60, 25.46,24.78,37.61,32.00,28.41,27.68,27.02,25.03,26.53,25.22,33.69,30.66,28.08,27.51,26.90,25.97,25.45,24.42,24.79,25.11,24.06,24.51,25.10,24.40, 24.62,25.16,25.10,25.06,26.22,25.34];
t=0:0.25:10;   

%Parent parameter values. 
k =   2;
j1=  200000;
j2 = 400000;
j3 = 300000;

cp = 4181;
V = 5; %Volumn of water.
Tamb = 25; %Ambient Tempeture.

dt=t(2)-t(1); %sampling data at 4Hz. 


%analytical solution to the model
parent_model = zeros(1, 41);
parent_impulse = zeros(1, 41); 
parent_impulse(5)=(j1)/(V*cp);
parent_impulse(13)=(j2)/(V*cp);
parent_impulse(21)=(j3)/(V*cp);
parent_model(1) = Tamb;

%Again, simulate parent model using eulars forward substituion method. 
for r=2:41
   parent_model(r) = parent_model(r-1) + (((-k*(parent_model(r-1) - Tamb)))*dt) + (parent_impulse(1, r-1));
end


%ARX part

%Three impulse inputs and an offset. 

%impulse 1
j1_arx=zeros(1, 41);
j1_arx(5)= 1; 

%impulse 2
j2_arx=zeros(1, 41);
j2_arx(13)= 1;

%impulse 3
j3_arx=zeros(1, 41);
j3_arx(21)= 1;

%offset
offset = ones(1, 41);

%First order with three impulses and an offset. A matrix contains T->T-1
%data as it begins at T(t-1), which does not exist. Impulses means b2-bn
%are removed.

A = [[T(1,1:end-1)]' [j1_arx(1,1:end-1)]' [j2_arx(1,1:end-1)]' [j3_arx(1,1:end-1)]' [offset(1, 1:end-1)]'];
b = [T(1,2:end)]';

ss = (A\b);

%simulating using the ARX model found. 
ARX_sim = zeros(size(t));
ARX_sim(1) = 25; %initial value for forward sim.
for I = 2:length(t)
    ARX_sim(I) = (ARX_sim(I-1)*ss(1)) + (ss(2) * j1_arx(I-1)) + (ss(3) * j2_arx(I-1)) + (ss(4) * j3_arx(I-1)) + (ss(5)*offset(I-1));
end


%Simulating the training data stuff
%Needed to use J2 = 2*J1 and J3 = 1.5J1. 

training_a =  zeros(12, 3);
training_a(1:12, 1) = A(1:12, 1);
training_a(1:12, 2) = A(1:12, 2);
training_a(1:12, 3) = A(1:12, 5);
training_b = b(1:12, 1);
training_theta = training_a\training_b;


impulses = ones(3, 1);
impules(1, 1) = training_theta(2);
impules(2, 1) = training_theta(2) * 2;
impules(3, 1) = training_theta(2) * 1.5;


%simulate training data set. 
ARX_sim_training = zeros(size(t));
ARX_sim_training(1) = 25; %initial value for forward sim.
for I = 2:length(t)
    ARX_sim_training(I) = (ARX_sim_training(I-1)*training_theta(1)) + (impules(1) * j1_arx(I-1)) + (impules(2) * j2_arx(I-1)) + (impules(3) * j3_arx(I-1)) + (training_theta(3)*offset(I-1));
end


%plot data and compare.
%ARX_sim2 = A*ss
plot(t, parent_model); hold all
%plot(t, ARX_sim2); hold all
plot(t, ARX_sim); hold all 

plot(t, ARX_sim_training);
