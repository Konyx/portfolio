#include "pacer.h"
#include "tcm8230.h"
#include "pio.h"
#include "delay.h"
#include "target.h"
#include "twi_protocol.h"
#include "command.h"
#include "usb_tty.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


#define TWI_ADDR CAMERA_TWI_ADDR
#define BOARD_NAME "Camera"
#define VERSION "V1.0"

#define PACER_RATE 10000
#define LED_FLASH_RATE 1

#define LED_FLASH_PIO LED1_PIO
#define LED_ERROR_PIO LED2_PIO
#define LED_GENERAL_PIO LED3_PIO

static const tcm8230_cfg_t camera_cfg =
{
    .picsize = TCM8230_PICSIZE_SQCIF
};


static twi_protocol_t *twi_protocol;
static tty_t *usb_tty;
static twi_protocol_t *twi_protocol;
static uint8_t image[SQCIF_WIDTH * SQCIF_HEIGHT * 2];
static int image_width = SQCIF_WIDTH;
static int image_height = SQCIF_HEIGHT;
static int image_row = 0;
static int captured = 0;
static int state = 1;


static int
shutdown (char *command, char *response, int size)
{
    tty_printf (usb_tty, "Shutting down...\n");
    /* TODO...  */
    return 1;
}


static int
leds_set (char *command, char *response, int size)
{
    int value = atoi (command + 1);

    pio_output_set (LED1_PIO, value & 1);
    pio_output_set (LED2_PIO, (value >> 1) & 1);
    pio_output_set (LED3_PIO, (value >> 2) & 1);
    return 1;
}


/* Capture an image.  This blocks until a frame is captured.  */
static int
image_capture (char *command, char *response, int size)
{
    int timeout_ms;
    int ret;

    if (command[1] == 0 && response)
        return snprintf (response, size, "%d", captured);

    timeout_ms = atoi (command + 1);

    /* Wait for VSYNC to go low.  */
    ret = tcm8230_vsync_low_wait (timeout_ms * 1000);
    if (ret <= 0)
        return ret;

    /* Wait for VSYNC to go high then capture image.  */
    ret = tcm8230_capture (image, sizeof(image), timeout_ms * 1000);
    captured = (ret > 0);
    return ret;
}


static void
image_test_pattern (void)
{
    int col;
    int row;

    for (row = 0; row < image_height; row++)
    {
        for (col = 0; col < image_width; col++)
        {
            image[(row * image_width + col) * 2] = row;
            image[(row * image_width + col) * 2 + 1] = col;
        }
    }
}


static void
image_zero (void)
{
    memset (image, 0, image_width * image_height * 2);
}


/* Generate test image.  */
static int
image_test_set (char *command, char *response, int size)
{
    int value = atoi (command + 1);

    if (value)
        image_test_pattern();
    else
        image_zero();
    return 1;
}


static void
bytetohex (uint8_t value, char *str)
{
    uint8_t tmp;

    tmp = (value >> 4) + '0';
    if (tmp > '9')
        tmp += 'A' - '9' - 1;
    str[0] = tmp;
    tmp = (value & 0x0f) + '0';
    if (tmp > '9')
        tmp += 'A' - '9' - 1;
    str[1] = tmp;
}


static uint8_t
checksum_xor (char *buffer, int size)
{
    uint8_t checksum = 0;
    int i;

    for (i = 0; i < size; i++)
        checksum ^= buffer[i];

    return checksum;
}


/* Return an image row.  Each byte is encoded as two hex digits, with
   the most significant digit first.  The first byte is the row
   number.  The second byte is the number of pixels.  The rest of the
   data is encoded as 4 bytes per pixel.  This is followed by a one
   byte checksum and a zero byte.   */
static int
image_row_pixels_get (char *command, char *response, int size)
{
    int col;
    int cols;
    uint8_t *rowptr;
    char *p;

    /* Have overhead of 8 bytes; row number, number of cols, checksum, and null terminator.  */
    cols = (size - 8) / 4;
    if (cols > image_width)
        cols = image_width;

    p = response;
    bytetohex (image_row, p);
    p += 2;

    bytetohex (image_width, p);
    p += 2;

    rowptr = &image[image_row * image_width * 2];

    for (col = 0; col < cols; col++)
    {
        bytetohex (rowptr[col * 2], p);
        p += 2;
        bytetohex (rowptr[col * 2 + 1], p);
        p += 2;
    }

    bytetohex (checksum_xor (response, p - response), p);
    p += 2;

    p[0] = 0;
    p[1] = 0;

    return size;
}


/* Get/set the row number for the next row transfer.  */
static int
image_row_set (char *command, char *response, int size)
{
    int value;

    if (command[1] == 0 && response)
        return snprintf (response, size, "%d", image_row);

    value = atoi (command + 1);

    if (value < 0)
        value = 0;
    if (value >= image_height)
        value = image_height - 1;
    image_row = value;
    return 1;
}


static int
hello (char *command, char *response, int size)
{
    tty_printf (usb_tty, "Hello world\n");
    return 1;
}


static int
usb (char *command, char *response, int size)
{
    /* For debugging, send message to USB/CDC device.  */
    tty_printf (usb_tty, command + 1);
    tty_printf (usb_tty, "\n");
    return 1;
}


static int
whoami (char *command, char *response, int size)
{
    return snprintf (response, size, BOARD_NAME " " VERSION);
}


static int
info (char *command, char *response, int size)
{
    return snprintf (response, size, "%d %d 2", image_width, image_height);
}


static int
twi_forward (char *command, char *response, int size)
{
    return command_twi_forward (twi_protocol, command, response, size);
}


void toggleCamera(void){


  if(state == 1){
    pio_output_low (LED0_PIO);
    pio_config_set(CAM_EN, PIO_OUTPUT_LOW);
    state = 0;
  }

  else if(state == 0){

    pio_config_set(CAM_EN, PIO_OUTPUT_HIGH);
    state = 1;
    delay_ms(100);
    tcm8230_init (&camera_cfg);
    delay_ms(10000);
    pio_output_high (LED0_PIO);
  }

}


command_t command_table[] =
{
    {'>', twi_forward},
    {'C', image_capture},
    {'L', leds_set},
    {'G', image_row_pixels_get},
    {'H', hello},
    {'I', info},
    {'R', image_row_set},
    {'T', image_test_set},
    {'U', usb},
    {'Z', toggleCamera},
    {'?', whoami},
    {'\0', 0},
};


static twi_protocol_cfg_t twi_protocol_cfg =
{
    .slave_addr = TWI_ADDR,
};


int
main (void)
{
    int flash_ticks = 0;

    /* Configure LED PIOs as outputs.  */
    pio_config_set (LED_ERROR_PIO, PIO_OUTPUT_LOW);
    pio_config_set (LED_FLASH_PIO, PIO_OUTPUT_LOW);
    pio_config_set (LED_GENERAL_PIO, PIO_OUTPUT_LOW);
    pio_config_set (LED4_PIO, PIO_OUTPUT_LOW);
    pio_config_set (LED0_PIO, PIO_OUTPUT_HIGH);
    pio_config_set (SGPIO, PIO_OUTPUT_LOW);
    pio_config_set (SGPIO2, PIO_OUTPUT_LOW);
    pio_config_set (PWM1, PIO_OUTPUT_LOW);
    pio_config_set (PWM2, PIO_OUTPUT_LOW);
    pio_config_set (PWM0, PIO_OUTPUT_LOW);
    pio_config_set (IR_RC5_RX_PIO, PIO_OUTPUT_LOW);




    /* Enable regulators here for the image sensor.
       TODO:  */


    pio_config_set (CAM_EN, PIO_OUTPUT_HIGH);

    /* Wait for power to come up for the image sensor.  */
    delay_ms (10);

    /* Initialise image sensor by providing EXTCLK and configuring
       through the I2C bus.  */
    tcm8230_init (&camera_cfg);

    /* At this point DCLK, HD, and VD should be output from the image
       sensor.  If not, read the debugging comments in tcm8230.c.  */

    twi_protocol = twi_protocol_init (&twi_protocol_cfg);

    usb_tty = usb_tty_init ();

    command_init (command_table);

    pacer_init (PACER_RATE);

    while (1)
    {
        pacer_wait ();

        command_tty_poll (usb_tty);

        if (command_twi_poll (twi_protocol) < 0)

        flash_ticks++;
        if (flash_ticks > PACER_RATE / LED_FLASH_RATE / 2)
        {
            flash_ticks = 0;
        }
    }

    return 0;
}
