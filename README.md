# README #

** NOTE - As the years go back, the code gets worse as I began coding in 2013. **

**2016**

Contains:

* Matlab code for a mechanical engineering controls class.

* Embedded systems code for a camera module.

* NOTE-There is more code from other classes that would give you a better perspective of me, however, I've signed agreements making it not possible to show this code off. 

**2015**

Contains:

* Helicopter embedded project (C).

* A project for an OS class (C).

* A computer graphics assignment (C++).

**2014**

Contains:

* A pong game made for an embedded system project (C). 

* A software engineering project (Java).

* A basic networking project (Python).

* A basic binary search tree (C)

**2013**

Contains:

* Basic python programs that were given in order to provide students fundamental knowledge of python, as well as some basic computer science algorithms (sorting etc).