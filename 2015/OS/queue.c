#include "queue.h"
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>

#define handle_error_en(en, msg) \
        do { errno = en; perror(msg); exit(EXIT_FAILURE); } while (0)

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)
        
        
typedef struct linkingStruct Link;

/*
 * Responsible for linking items in the queue.
 * 
 * */
      
typedef struct linkingStruct {
	void *data;
	Link *previous;
	Link *next;
} Link;

/*
 * Responsible queue related items
 * 
 * */
 
typedef struct QueueStruct {
	Link *head;
	Link *tail;
	sem_t add;
	sem_t remove;
	pthread_mutex_t threads;
  
} Queue;

/*
 * queue_free:
 * 
 * Free a concurrent queue and associated memory.
 * NOTE: A user should not free a queue until any
 * users are finished. Any calling queue_free() while 
 * any consumer/producer is waiting on queue_put or queue_get 
 * will cause queue_free to print an error and exit the program.
 * 
 */


Queue *queue_alloc(int size) {
	Queue* queue = (Queue*)malloc(sizeof(Queue));
	sem_init(&queue -> add, 0, size);
	sem_init(&queue -> remove, 0, 0);
	pthread_mutex_t init = PTHREAD_MUTEX_INITIALIZER;
	queue -> threads = init;
	queue -> head = queue -> tail = NULL;
	return queue;
}

/*
 * free queue.
 * 
 * */

void queue_free(Queue *queue) {
   free(queue);

}


/*
 * queue_put:
 * 
 * Place an item into the concurrent queue. 
 * 
 * If there is no space available, queue_put will
 * block until a space becomes available when it will
 * put the item into the queue and immediately return.
 * 
 * Uses void* to hold an arbitrary type of item,
 * it is the users responsibility to manage memory 
 * and ensure it is correctly typed.
 * 
 */

void queue_put(Queue *queue, void *item) {
	sem_wait(&queue->add);
	pthread_mutex_lock(&queue -> threads);
	Link* item_link = (Link*)malloc(sizeof(Link));
	item_link->data = item;
	if(queue->head == NULL){
		//fill empty queue
		queue->head = item_link;
		queue->tail = item_link;
		item_link->next = NULL;
		item_link->previous = NULL;	
	}
	
	else{
		//fill non-empty queue
		queue->tail->previous = item_link;
		item_link->next = queue->tail;
		item_link->previous = NULL;
		queue->tail = item_link;
	}
	 pthread_mutex_unlock(&queue -> threads);
     sem_post(&queue->remove);
	
}

/*
 * queue_get:
 * 
 * Get an item from the concurrent queue.
 * 
 * If there is no item available then queue_get 
 * will block until an item becomes avaible when 
 * it will immediately return that item.
 * 
 */

void *queue_get(Queue *queue) {
	sem_wait(&queue -> remove);
	pthread_mutex_lock(&queue->threads);
	Link *item_link = queue->head;
	void *item = item_link->data;
	if(item_link == NULL){
		return NULL;
	}
	
	if(queue->head == queue->tail){
		//empty queue
		queue->head = NULL;
		queue->tail = NULL;
	}
	else{
		//non-empty queue
		queue->head = queue->head->previous;
		queue->head->next = NULL;
	}
	pthread_mutex_unlock(&queue->threads);
	sem_post(&queue->add); 
	free(item_link);
	return item;
}
