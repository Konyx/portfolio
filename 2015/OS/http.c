#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "http.h"
#define DEFAULT "\n"
#define BUF_SIZE 1024


/*
 * Creates a new buffer
 * */
 
 Buffer *new_buffer(size_t reserved) {
	Buffer *copy = (Buffer*)malloc(sizeof(Buffer));
	copy->data = (char *)malloc(reserved);
	copy->length = 0;
	return copy;
}
 
 
/*
 * Function used for dynamically increasing buffer
 * */

size_t append_buffer(Buffer *buffer, char *data, size_t length, size_t chunk_size) {
	while((buffer->length + length) > chunk_size){
		chunk_size = chunk_size * 2;
		buffer->data = realloc(buffer->data, chunk_size);
  }
	memcpy((buffer->data + buffer->length), data, length);
	buffer->length = buffer->length + length;
	return chunk_size;
}
    

/*
 * http_query:
 * 
 * Perform an HTTP 1.0 query to a given host and page and port number.
 * host is a hostname and page is a path on the remote server.
 * 
 * On any error (for example an invalid path or host name)
 * NULL is returned and any resources associated with the query 
 * are cleaned up. An error may be printed on stderr.
 * 
 * The data returned is the raw http response with content, 
 * and the user is responsible for freeing the memory.
 *
 * arguments:
 *    host - hostname e.g. www.canterbury.ac.nz
 *    page - e.g.  /index.html
 * 
 */

Buffer* http_query(char *host, char *page, int port_num) {
	char port[20];
	size_t chunk_size = BUF_SIZE;
	if (page[0] == '/') page++; //Removes the '/' if there is one
	char* Message = "GET /%s HTTP/1.0\r\nHost: %s\r\nUser-Agent: %s\r\n\r\n";
	char* Requires = (char*)malloc(strlen(host)+strlen(page)+strlen(DEFAULT)+ strlen(Message)-5);
    sprintf(Requires, Message, page, host, DEFAULT);
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 	// error check
    {
        perror("socket\n");
        exit(1);
    }
	struct addrinfo their_addrinfo; // server address info
	struct addrinfo *their_addr = NULL; // connector's address information
	memset(&their_addrinfo, 0, sizeof(struct addrinfo));
	their_addrinfo.ai_family = AF_INET; /* use an internet address */
	their_addrinfo.ai_socktype = SOCK_STREAM; /* use TCP rather than datagram */
	sprintf(port, "%d", port_num);
	int errono = getaddrinfo(host, port, &their_addrinfo, &their_addr);/* get IP info */
	if(errono != 0)	// error check
	{ 
		perror("Web address is wrong\n");
		exit(2);
	}
	int rc = connect(sockfd, their_addr->ai_addr, their_addr->ai_addrlen); 
	if(rc == -1) 	// error check
	{
		perror("connection problems\n");
		exit(3);
	}
	FILE *f_recv = fdopen(dup(sockfd), "r");
    char *data = malloc(BUF_SIZE);
    Buffer *buffer = new_buffer(BUF_SIZE);
    size_t bytes = 0;
	int message_length  = write(sockfd, Requires, strlen(Requires));
	if (message_length < 0)
    {
       perror("write problems");
       exit(4);
    }
    while((bytes = fread(data, 1, BUF_SIZE, f_recv)) > 0) {
		chunk_size = append_buffer(buffer, data, bytes, chunk_size);
    }

    free(data); 
	free(Requires);
	freeaddrinfo(their_addr);
	close(sockfd);
	fclose(f_recv); 
	return buffer;
}

// split http content from the response string
char* http_get_content(Buffer *response) {
	char* header_end = strstr(response->data, "\r\n\r\n"); 

    
	if(header_end) {
		return header_end + 4;
	} 
	else {
		return response->data; 
	}
}


Buffer *http_url(const char *url) {
  char host[BUF_SIZE];
  strncpy(host, url, BUF_SIZE);
  
  char *page = strstr(host, "/");
  if(page) {
    page[0] = '\0';
  
    ++page;
    return http_query(host, page, 80);
  } else {
    
    fprintf(stderr, "could not split url into host/page %s\n", url);
    return NULL;
  }
}
