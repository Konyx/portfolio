/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The CONE Class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/




#include "Cone.h"
#include <math.h>


float Cone::intersect(Vector pos, Vector dir)
{
	float X = pos.x - base.x;
	float Z = pos.z - base.z;
	float D = height - pos.y + base.y;

	float tan = (radius / height) * (radius / height);

	float a = (dir.x * dir.x) + (dir.z * dir.z) - (tan*(dir.y * dir.y));
	float b = (2 * X*dir.x) + (2 * Z*dir.z) + (2 * tan*D*dir.y);
	float c = (X*X) + (Z*Z) - (tan*(D*D));

	float discriminant = b*b - 4 * (a*c);


	float t1 = (-b - sqrt(discriminant)) / (2 * a);
	float t2 = (-b + sqrt(discriminant)) / (2 * a);


	if (fabs(t1) < 0.001)
	{
		if (t2 > 0) return t2;
		else t1 = -1.0;
	}

	if (fabs(t2) < 0.001) {

		t2 = -1.0;
	}



	float t;

	if (t1>t2) t = t2;
	else t = t1;

	float r = pos.y + t*dir.y;

	if ((r > base.y) && (r < base.y + height)) {
		return t;

	}
	else {

		return -1;

	}
}

Vector Cone::normal(Vector p)
{
	float r = sqrt((p.x - base.x)*(p.x - base.x) + (p.z - base.z)*(p.z - base.z));
	Vector n = Vector(p.x - base.x, r*(radius / height), p.z - base.z);
	n.normalise();
	return n;
}