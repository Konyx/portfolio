/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The CONE Class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/

#ifndef H_CONE
#define H_CONE

#include "Object.h"

class Cone : public Object
{

private:
	float height;
	Vector base;
	float radius;

public:
	Cone()
		: base(Vector()), radius(1), height(1)  
	{
		color = Color::WHITE;
	};

	Cone(Vector c, float r, float h, Color col)
		: base(c), radius(r), height(h)
	{
		color = col;
	};

	float intersect(Vector pos, Vector dir);

	Vector normal(Vector p);

};

#endif //!H_Cone