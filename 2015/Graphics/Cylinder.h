/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The CYLINDER class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/

#ifndef H_CYLINDER
#define H_CYLINDER

#include "Object.h"

class Cylinder : public Object
{

private:
	float height;
	Vector base;
	float radius;
	

public:
	Cylinder()
		: base(Vector()), radius(1), height(0.1) 
	{
		color = Color::BLACK;
	};

	Cylinder(Vector c, float r, float h, Color col)
		: base(c), radius(r), height(h)
	{
		color = col;
	};

	float intersect(Vector pos, Vector dir);

	Vector normal(Vector p);

};

#endif //!H_CYLINDER
