﻿// ========================================================================
// COSC 363  Computer Graphics  Assingment 2
// AUTHOR:- KAAN ARIK
// Student ID:- 84254373
// A ray tracer for COSC363 assingment. This was build ontop of source code
// provided in labratory sessions. 
// ========================================================================

#include <iostream>
#include <cmath>
#include <vector>
#include "Vector.h"
#include "Sphere.h"
#include "Color.h"
#include "Object.h"
#include "Plane.h"
#include "Cylinder.h"
#include "Floor.h"
#include "Cone.h"
#include <GL/glut.h>
using namespace std;




const float WIDTH = 30.0;
const float HEIGHT = 30.0;
const float EDIST = 40.0;
const int PPU = 30;     //Total 600x600 pixels
const int MAX_STEPS = 5;
const float XMIN = -WIDTH * 0.5;
const float XMAX = WIDTH * 0.5;
const float YMIN = -HEIGHT * 0.5;
const float YMAX = HEIGHT * 0.5;

vector<Object*> sceneObjects;


Vector light = Vector(10, 40.0, -5.0);
Vector light2 = Vector(-10.0, 40, -5.0);
Color backgroundCol;

//A useful struct
struct PointBundle
{
	Vector point;
	int index;
	float dist;
};

/*
* This function compares the given ray with all objects in the scene
* and computes the closest point  of intersection.
*/
PointBundle closestPt(Vector pos, Vector dir)
{
	Vector  point(0, 0, 0);
	float min = 10000.0;

	PointBundle out = { point, -1, 0.0 };

	for (unsigned int i = 0; i < sceneObjects.size(); i++)
	{
		float t = sceneObjects[i]->intersect(pos, dir);
		if (t > 0)        //Intersects the object
		{
			point = pos + dir*t;
			if (t < min)
			{
				out.point = point;
				out.index = i;
				out.dist = t;
				min = t;
			}
		}
	}

	return out;
}

/*
* Computes the colour value obtained by tracing a ray.
* If reflections and refractions are to be included, then secondary rays will
* have to be traced from the point, by converting this method to a recursive
* procedure.
*/

Color trace(Vector pos, Vector dir, int step)
{

	Color colorSum;
	PointBundle q = closestPt(pos, dir);
	if (q.index == -1) return backgroundCol;        //no intersection

	Color col = sceneObjects[q.index]->getColor(q.point); //Object's colour

	float spec;
	float spec2;
	Vector n = sceneObjects[q.index]->normal(q.point);
	Vector l = light - q.point;
	Vector l2 = light2 - q.point;
	float lightDist = l.length();
	float light2Dist = l2.length();
	Vector v(-dir.x, -dir.y, -dir.z); //View vector;

	l.normalise();
	l2.normalise();
	float lDotn = l.dot(n); //Note ‘l’ is the letter el, not the number 1

	float l2Dotn = l2.dot(n);



	if ((lDotn <= 0) && (l2Dotn <= 0)) {
		colorSum = col.phongLight(backgroundCol, 0.0, 0.0);
	}

	else {

		Vector r = ((n * 2) * lDotn) - l; 
		Vector r2 = ((n * 2) * l2Dotn) - l2; 
		r.normalise();
		r2.normalise();

		float rDotv = r.dot(v);
		float r2Dotv = r2.dot(v);

		if (rDotv < 0) spec = 0.0;
		else spec = pow(rDotv, 10);


		if (r2Dotv < 0) spec2 = 0.0;
		else spec2 = pow(r2Dotv, 10); 
					
		PointBundle s = closestPt(q.point, l);
		PointBundle s2 = closestPt(q.point, l2);

		if (s.index == -1) {
			colorSum = col.phongLight(backgroundCol, lDotn, spec);
		}
		else if (s2.index == -1) {
			colorSum = col.phongLight(backgroundCol, l2Dotn, spec2);

		}
		else if (s.index != -1 || s2.index != -1)
		{
			if (s.dist < lightDist) {
				colorSum = col.phongLight(backgroundCol, 0, 0);
			}

			else if (s2.dist < light2Dist) {
				colorSum = col.phongLight(backgroundCol, 0, 0);
			}
		}

	}





	if ((q.index == 0 || q.index == 3) && step < MAX_STEPS)
	{
		Vector reflectionVector = ((n * 2) * n.dot(v)) - v;
		reflectionVector.normalise();
		float reflCoeff = 0.5f;
		Color reflectionCol = trace(q.point, reflectionVector, step + 1);
		colorSum.combineColor(reflectionCol, reflCoeff);


	}



	if ((q.index == 1 || q.index == 2) && step < MAX_STEPS)

		{
			float n2;
			float n1;
			float pt = 0.95;

			if (q.index == 1) {
				n1 = 1;
				n2 = 2.10;
			}

			else{
				n1 = 1;
				n2 = 1.01;

			}
	

			float nRatio = n1 / n2;

			float DdotN = dir.dot(n);

			float cosThetaT = sqrt(1 - nRatio*nRatio * (1 - DdotN*DdotN));
			Vector inRefVector = dir*nRatio - n*(nRatio * DdotN + cosThetaT);


			PointBundle outer = closestPt(q.point, inRefVector);

			if (outer.index != -1) {

				Vector outRefVector;
				Object *outerObj = sceneObjects[outer.index];
				Vector normal = outerObj->normal(outer.point);
				normal.scale(-1);
				float tmp = n2 / n1;
				DdotN = inRefVector.dot(normal);
				cosThetaT = sqrt(1 - tmp*tmp * (1 - DdotN*DdotN));
				outRefVector = inRefVector*tmp - normal*(tmp * DdotN + cosThetaT);
				Color refractionCol = trace(outer.point, outRefVector, step + 1);
				colorSum.combineColor(refractionCol, pt);



			}

			else {

				Color refractionCol = trace(outer.point, inRefVector, step + 1);
				colorSum.combineColor(refractionCol, pt);


			}





		}

		


		return colorSum;


	}

	




Color SuperSAMPLE(Vector eye, Vector dir, float pixel, float x1, float y1)
{

	float R = 0;
	float G = 0;
	float B = 0;
	float half = pixel / 2.0;
	float quarter = pixel / 4.0;
	float xc = x1 + quarter;
	float yc = y1 + quarter;
	Color SetOfPixel[4];
	Vector current_dir;

	current_dir = Vector(xc, yc, -EDIST);
	current_dir.normalise();
	SetOfPixel[0] = trace(eye, current_dir, 1);
	current_dir = Vector(xc + half, yc, -EDIST);
	current_dir.normalise();
	SetOfPixel[1] = trace(eye, current_dir, 1);
	current_dir = Vector(xc, yc + half, -EDIST);
	current_dir.normalise();
	SetOfPixel[2] = trace(eye, current_dir, 1);
	current_dir = Vector(xc + half, yc + half, -EDIST);
	current_dir.normalise();
	SetOfPixel[3] = trace(eye, current_dir, 1);

	for (int i = 0; i<4; i++) {
		R += SetOfPixel[i].r;
		G += SetOfPixel[i].g;
		B += SetOfPixel[i].b;
	}

	Color col(R / 4, G / 4, B / 4);
	return col;
}





//---The main display module -----------------------------------------------------------
// In a ray tracing application, it just displays the ray traced image by drawing
// each pixel as quads.
//---------------------------------------------------------------------------------------
void display()
{
	int widthInPixels = (int)(WIDTH * PPU);
	int heightInPixels = (int)(HEIGHT * PPU);
	float pixelSize = 1.0 / PPU;
	float halfPixelSize = pixelSize / 2.0;
	float x1, y1, xc, yc;
	Vector eye(0., 0., 0.);

	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_QUADS);  //Each pixel is a quad.

	for (int i = 0; i < widthInPixels; i++)	//Scan every "pixel"
	{
		x1 = XMIN + i*pixelSize;
		xc = x1 + halfPixelSize;
		for (int j = 0; j < heightInPixels; j++)
		{
			y1 = YMIN + j*pixelSize;
			yc = y1 + halfPixelSize;

			Vector dir(xc, yc, -EDIST);	//direction of the primary ray
			Color average_super = SuperSAMPLE(eye, dir, pixelSize, x1, y1);
			dir.normalise();
		

		
			Color col = average_super;
			
			glColor3f(col.r, col.g, col.b);
			glVertex2f(x1, y1);				//Draw each pixel with its color value
			glVertex2f(x1 + pixelSize, y1);
			glVertex2f(x1 + pixelSize, y1 + pixelSize);
			glVertex2f(x1, y1 + pixelSize);
		}
	}

	glEnd();
	glFlush();
}

void make_cube() {

	Vector top_left_front(1, -1, -40);
	Vector top_right_front(1, 1, -40);
	Vector top_left_back(1, -1, -30);
	Vector top_right_back(1, 1, -30);
	Vector back_left_front(3, -1, -40);
	Vector back_right_front(3, 1, -40);
	Vector back_left_back(3, -1, -30);
	Vector back_right_back(3, 1, -30);

	Plane *boxtop = new Plane(top_right_back, top_right_front, top_left_front, top_left_back, Color(1, 0, 1));
	Plane *boxbottom = new Plane(back_right_back, back_right_front, back_left_front, back_left_back, Color(1, 0, 1));
	Plane *boxleft = new Plane(back_left_front, top_left_front, top_left_back, back_left_back, Color(1, 0, 1));

	Plane *boxright = new Plane(back_right_front, top_right_front, top_right_back, back_right_back, Color(1, 0, 1));
	Plane *boxfront = new Plane(top_right_front, top_left_front, back_left_front, back_right_front, Color(1, 0, 1));
	Plane *boxback = new Plane(top_right_back, top_left_back, back_left_back, back_right_back, Color(1, 0, 1));

	sceneObjects.push_back(boxtop);
	sceneObjects.push_back(boxbottom);
	sceneObjects.push_back(boxleft);
	sceneObjects.push_back(boxright);


	sceneObjects.push_back(boxfront);
	sceneObjects.push_back(boxback);



}



void initialize()
{
	backgroundCol = Color::GRAY;
	glMatrixMode(GL_PROJECTION); backgroundCol = Color::GRAY;
	gluOrtho2D(XMIN, XMAX, YMIN, YMAX);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0, 0, 0, 1);
	Sphere *sphere1 = new Sphere(Vector(0, 3, -60), 7.0, Color::RED);
	sceneObjects.push_back(sphere1);
	Sphere *sphere2 = new Sphere(Vector(0, 1, -40), 3.0, Color::GRAY);
	sceneObjects.push_back(sphere2);
	Sphere *sphere3 = new Sphere(Vector(7, 3, -30), 3.0, Color::GRAY);
	sceneObjects.push_back(sphere3);

	Sphere *sphere4 = new Sphere(Vector(-7, 3, -30), 3.0, Color::GRAY);
	sceneObjects.push_back(sphere4);


	Sphere *sphere5 = new Sphere(Vector(7, 3, -50), 3.0, Color::RED);
	sceneObjects.push_back(sphere5);

	Cylinder *cylinder1 = new Cylinder(Vector(6, -5, -45), 1.0, 2.0, Color::GREEN);

	sceneObjects.push_back(cylinder1);


	Cone *cone1 = new Cone(Vector(-4, -5, -35), 1.5, 3.0, Color::BLUE);

	sceneObjects.push_back(cone1);



	Floor *floor = new Floor(Vector(-400 / 2, -10, 0),
		Vector(+400 / 2, -10, 0),
		Vector(+400 / 2, -10, -400 / 2),
		Vector(-400 / 2, -10, -400 / 2),
		Color::BLACK, Color::WHITE, 60);
	sceneObjects.push_back(floor);


	make_cube();


}


int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(20, 20);
	glutCreateWindow("kar122 Raytracing");

	glutDisplayFunc(display);
	initialize();

	glutMainLoop();
	return 0;
}