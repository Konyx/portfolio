/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The FLOOR Class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/

#ifndef H_FLOOR
#define H_FLOOR

#include "Vector.h"
#include "Object.h"
#include "Color.h"
#include "Plane.h"

class Floor : public Plane
{
private:
	Color color1, color2;
	int tileCount;

public:
	Floor(void);

	Floor(Vector pa, Vector pb, Vector pc, Vector pd, Color col, Color col2, int Tiles)
		: color1(col), color2(col2), tileCount(Tiles)
	{
		a = pa;
		b = pb;
		c = pc;
		d = pd;

	};

	Color getColor(Vector point);



};

#endif //!H_FLOOR
