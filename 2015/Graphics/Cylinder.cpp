/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The CYLINDER class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/




#include "Cylinder.h"
#include <math.h>


float Cylinder::intersect(Vector pos, Vector dir)
{
	float x = pos.x - base.x;
	float z = pos.z - base.z;
	float a = dir.x*dir.x + dir.z*dir.z;
	float b = 2 * (dir.x*x + dir.z*z);
	float c = x*x + z*z - radius*radius;

	float discriminant = b*b - 4 * a*c;
	if (fabs(discriminant) < 0.001) return -1.0f;

	if (discriminant <= 0.0) return -1.0f;

	float t1 = (-b + sqrt(discriminant)) / (2 * a);
	float t2 = (-b - sqrt(discriminant)) / (2 * a);
	
   
    if(fabs(t1) < 0.001 )
    {
        if (t2 > 0) return t2;
        else t1 = -1.0;
    }

	if (fabs(t2) < 0.001) {

		t2 = -1.0;
	}
	float t_main;
	float t_second_value;

	if (t1 < t2) {
		t_main = t1;
		t_second_value = t2;

	}

	else {
		t_main = t2;
		t_second_value = t1;

	}
	
	float height_intersect = pos.y + (dir.y*t_main);
	float height_intersect1 = pos.y + (dir.y*t_second_value);

	float insideCoeff = (height_intersect - base.y);
	float insideCoeff1 = (height_intersect1 - base.y);



	if (insideCoeff >= base.y && insideCoeff <= height)
	{
		return t_main;
	}

	else if (insideCoeff1 >= base.y && insideCoeff1 <= height) {
		return t_second_value;

	}


	return -1;

}



Vector Cylinder::normal(Vector p)
{
	Vector n = Vector(p.x - base.x, 0, p.z - base.z);
	n.normalise();
	return n;
}
