/*----------------------------------------------------------
* COSC363  Ray Tracer
*
*  The FLOOR Class
*  This is a subclass of Object, and hence implements the
*  methods intersect() and normal().



*AUTHOR KAAN ARIK:- 84254373
-------------------------------------------------------------*/

#include "Floor.h"
#include "Color.h"
#include "Vector.h"
#include <stdlib.h>
#include <math.h>


Color Floor::getColor(Vector point)
{


	//ALGORITHM TO GET THE CHEQERED FLOOR PATTERN 
	int X = tileCount*(point.x - a.x) / (b.x - a.x);
	int Y = tileCount*(point.z - a.z) / (d.z - a.z);



	if ((X + Y) % 2 == 0) {
		return color1;
	}

	else {
		return color2;

	}

}